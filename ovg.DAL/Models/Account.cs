﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Account
    {
        public int AccountId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ServiceAddress { get; set; }
        public string ZipCode { get; set; }
        public string Ovgnumber { get; set; }
        public bool? FromErp { get; set; }
    }
}
