﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeInspectionType
    {
        public ListPipeInspectionType()
        {
            PipeAndCoatingInspection = new HashSet<PipeAndCoatingInspection>();
        }

        public string PipeInspectionType { get; set; }
        public int? Order { get; set; }
        public int PipeInspectionTypeId { get; set; }

        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspection { get; set; }
    }
}
