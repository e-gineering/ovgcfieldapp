﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFittingType
    {
        public ListFittingType()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string FittingType { get; set; }
        public int? Order { get; set; }
        public int FittingTypeId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
