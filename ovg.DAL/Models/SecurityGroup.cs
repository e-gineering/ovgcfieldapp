﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class SecurityGroup
    {
        public SecurityGroup()
        {
            User = new HashSet<User>();
        }

        public string SecurityGroup1 { get; set; }
        public int? Order { get; set; }
        public int SecurityGroupId { get; set; }

        public ICollection<User> User { get; set; }
    }
}
