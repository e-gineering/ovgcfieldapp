﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ServerError
    {
        public int ServerErrorId { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
