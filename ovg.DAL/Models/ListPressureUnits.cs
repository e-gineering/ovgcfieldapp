﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPressureUnits
    {
        public ListPressureUnits()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string PressureUnits { get; set; }
        public int? Order { get; set; }
        public int PressureUnitsId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
