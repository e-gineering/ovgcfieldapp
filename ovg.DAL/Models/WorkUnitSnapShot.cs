﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class WorkUnitSnapShot
    {
        public int WorkUnitSnapShotId { get; set; }
        public DateTime SnapShotCreatedOn { get; set; }
        public int? WorkUnitId { get; set; }
        public string DistrictId { get; set; }
        public string WorkUnitAutoId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public int? AssignedToUser { get; set; }
        public DateTime? AssignedToUserOnDate { get; set; }
        public int? CheckedOutBy { get; set; }
        public DateTime? CheckedOutOnDate { get; set; }
        public int? ClosedBy { get; set; }
        public DateTime? ClosedByDate { get; set; }
        public int? ApprovedClosedBy { get; set; }
        public DateTime? ApprovedClosedByDate { get; set; }
        public DateTime? IsInitSoftPullbackDate { get; set; }
        public DateTime? IsSoftPullbackCompletedDate { get; set; }
        public bool? IsEmergency { get; set; }
        public string Jsonpackage { get; set; }
        public int? WorkUnitTypeId { get; set; }
    }
}
