﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class WorkOrderSpecificationReport
    {
        public int WorkOrderSpecificationReportId { get; set; }
        public int WorkOrderId { get; set; }
        public string Note { get; set; }
        public bool IsReport { get; set; }
        public int SpecificationReportId { get; set; }

        public SpecificationReport SpecificationReport { get; set; }
        public WorkOrder WorkOrder { get; set; }
    }
}
