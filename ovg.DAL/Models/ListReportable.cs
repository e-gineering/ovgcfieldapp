﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListReportable
    {
        public ListReportable()
        {
            LeakFinalReview = new HashSet<LeakFinalReview>();
        }

        public string Reportable { get; set; }
        public int? Order { get; set; }
        public int ReportableId { get; set; }

        public ICollection<LeakFinalReview> LeakFinalReview { get; set; }
    }
}
