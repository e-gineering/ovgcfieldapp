﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListComponent
    {
        public ListComponent()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string Component { get; set; }
        public int? Order { get; set; }
        public int ComponentId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
