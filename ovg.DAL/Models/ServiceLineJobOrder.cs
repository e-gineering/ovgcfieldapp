﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ServiceLineJobOrder
    {
        public int ServiceLineJobOrderId { get; set; }
        public int WorkUnitId { get; set; }
        public string JobOrderNum { get; set; }
        public string CustomerName { get; set; }
        public string ServiceAddress { get; set; }
        public string SlworkToBeDone { get; set; }
        public string ReasonForWork { get; set; }
        public string DistrictManagerSignature1 { get; set; }
        public bool? LeakFrm106Submitted { get; set; }
        public bool? PipeInspFrm100Submitted { get; set; }
        public string DistrictManagerSignature2 { get; set; }
        public string AccountingDeptSignature { get; set; }
        public DateTime? AccountingDeptDate { get; set; }
        public string BillingDeptSignature { get; set; }
        public DateTime? BillingDeptDate { get; set; }
        public string EngineeringDeptSignature { get; set; }
        public DateTime? EngineeringDeptDate { get; set; }
        public string AccountNo { get; set; }
        public string Town { get; set; }
        public string Mrtno { get; set; }
        public string HeatingApplicationNo { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
    }
}
