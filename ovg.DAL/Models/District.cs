﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class District
    {
        public District()
        {
            CityStateZip = new HashSet<CityStateZip>();
            User2District = new HashSet<User2District>();
            WorkUnit = new HashSet<WorkUnit>();
        }

        public string DistrictDesc { get; set; }
        public string DistrictIuupsidlist { get; set; }
        public int? Order { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }

        public ICollection<CityStateZip> CityStateZip { get; set; }
        public ICollection<User2District> User2District { get; set; }
        public ICollection<WorkUnit> WorkUnit { get; set; }
    }
}
