﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMechanicalFittingLeakLocation2
    {
        public ListMechanicalFittingLeakLocation2()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string MechanicalFittingLeakLocation2 { get; set; }
        public int? Order { get; set; }
        public int MechanicalFittingLeakLocation2Id { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
