﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class User
    {
        public User()
        {
            User2District = new HashSet<User2District>();
            WorkUnitApprovedClosedByNavigation = new HashSet<WorkUnit>();
            WorkUnitAssignedToUserNavigation = new HashSet<WorkUnit>();
            WorkUnitCheckedOutByNavigation = new HashSet<WorkUnit>();
            WorkUnitClosedByNavigation = new HashSet<WorkUnit>();
            WorkUnitCreatedByNavigation = new HashSet<WorkUnit>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid? Token { get; set; }
        public DateTime? LastCheckin { get; set; }
        public int SecurityGroupId { get; set; }

        public SecurityGroup SecurityGroup { get; set; }
        public ICollection<User2District> User2District { get; set; }
        public ICollection<WorkUnit> WorkUnitApprovedClosedByNavigation { get; set; }
        public ICollection<WorkUnit> WorkUnitAssignedToUserNavigation { get; set; }
        public ICollection<WorkUnit> WorkUnitCheckedOutByNavigation { get; set; }
        public ICollection<WorkUnit> WorkUnitClosedByNavigation { get; set; }
        public ICollection<WorkUnit> WorkUnitCreatedByNavigation { get; set; }
    }
}
