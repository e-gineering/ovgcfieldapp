﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListCoatingCondition
    {
        public ListCoatingCondition()
        {
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
            PipeAndCoatingInspectionCoatingConditionMain = new HashSet<PipeAndCoatingInspection>();
            PipeAndCoatingInspectionCoatingConditionService = new HashSet<PipeAndCoatingInspection>();
        }

        public string CoatingCondition { get; set; }
        public int? Order { get; set; }
        public int CoatingConditionId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionCoatingConditionMain { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionCoatingConditionService { get; set; }
    }
}
