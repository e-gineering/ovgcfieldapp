﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeConditionExternal
    {
        public ListPipeConditionExternal()
        {
            LeakRepair2PipeConditionExternal = new HashSet<LeakRepair2PipeConditionExternal>();
            LineExcavation2PipeConditionExternal = new HashSet<LineExcavation2PipeConditionExternal>();
            PipeAndCoatingInspectionPconditionExtMain = new HashSet<PipeAndCoatingInspection>();
            PipeAndCoatingInspectionPconditionExtService = new HashSet<PipeAndCoatingInspection>();
        }

        public string PipeConditionExternal { get; set; }
        public int? Order { get; set; }
        public int PipeConditionExternalId { get; set; }

        public ICollection<LeakRepair2PipeConditionExternal> LeakRepair2PipeConditionExternal { get; set; }
        public ICollection<LineExcavation2PipeConditionExternal> LineExcavation2PipeConditionExternal { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPconditionExtMain { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPconditionExtService { get; set; }
    }
}
