﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakRepair
    {
        public LeakRepair()
        {
            LeakRepair2PipeConditionExternal = new HashSet<LeakRepair2PipeConditionExternal>();
        }

        public int LeakRepairId { get; set; }
        public int WorkUnitId { get; set; }
        public string DetailOfRepair { get; set; }
        public string DetailOfComponentFailure { get; set; }
        public string Mrtnum { get; set; }
        public string PipeSize { get; set; }
        public int? Depth { get; set; }
        public int? PressureAmount { get; set; }
        public bool? CathodicProtection { get; set; }
        public double? PipeToSoil { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public string Pressure { get; set; }
        public bool? OdorantSmellTest { get; set; }
        public DateTime? RepairDate { get; set; }
        public int? ComponentId { get; set; }
        public int? DecadeInstalledId { get; set; }
        public int? EnvironmentId { get; set; }
        public int? LeakCauseId { get; set; }
        public int? LeakConfirmationId { get; set; }
        public int? LocationId { get; set; }
        public int? PipeDataId { get; set; }
        public int? PressureUnitsId { get; set; }
        public int? CoatingConditionId { get; set; }
        public int? OdorantSmellTestResultId { get; set; }
        public int? PipeToSoilCurrentTypeId { get; set; }
        public int? PipeToSoilPolarityId { get; set; }
        public int? PipeTypeId { get; set; }
        public int? SoilNearPipeId { get; set; }
        public int? TypeCoatingId { get; set; }

        public ListCoatingCondition CoatingCondition { get; set; }
        public ListComponent Component { get; set; }
        public ListDecadeInstalled DecadeInstalled { get; set; }
        public ListEnvironment Environment { get; set; }
        public ListLeakCause LeakCause { get; set; }
        public ListLeakConfirmation LeakConfirmation { get; set; }
        public ListLocation Location { get; set; }
        public ListOdorantSmellTestResult OdorantSmellTestResult { get; set; }
        public ListPipeData PipeData { get; set; }
        public ListPipeToSoilCurrentType PipeToSoilCurrentType { get; set; }
        public ListPipeToSoilPolarity PipeToSoilPolarity { get; set; }
        public ListPipeType PipeType { get; set; }
        public ListPressureUnits PressureUnits { get; set; }
        public ListSoilNearPipe SoilNearPipe { get; set; }
        public ListTypeCoating TypeCoating { get; set; }
        public WorkUnit WorkUnit { get; set; }
        public ICollection<LeakRepair2PipeConditionExternal> LeakRepair2PipeConditionExternal { get; set; }
    }
}
