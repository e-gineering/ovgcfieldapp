﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListResponsibleParty
    {
        public ListResponsibleParty()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string ResponsibleParty { get; set; }
        public int? Order { get; set; }
        public int ResponsiblePartyId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
