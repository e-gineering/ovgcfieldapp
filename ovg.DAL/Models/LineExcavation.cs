﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LineExcavation
    {
        public int LineExcavationId { get; set; }
        public int WorkUnitId { get; set; }
        public bool? IsDistribution { get; set; }
        public bool? IsTransmission { get; set; }
        public string MapSection { get; set; }
        public bool? ExcavatorRequestLocate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Excavator { get; set; }
        public string LocationOrReferencePoint { get; set; }
        public string DistanceToMainLine { get; set; }
        public string ExcavationType { get; set; }
        public string ExcavationPurpose { get; set; }
        public bool? GasLinesLocatedCorrectly { get; set; }
        public bool? GasLinesAccurateOnBookMap { get; set; }
        public bool? WasLineExposed { get; set; }
        public string PipeSize { get; set; }
        public string PipeDepth { get; set; }
        public bool? IsPipeUnderPavement { get; set; }
        public double? PipeToSoil { get; set; }
        public bool? RepairsNeeded { get; set; }
        public bool? DamageCausedByExcavation { get; set; }
        public string RepairsMadeNeeded { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public int? CoatingConditionId { get; set; }
        public int? PipeToSoilCurrentTypeId { get; set; }
        public int? PipeToSoilPolarityId { get; set; }
        public int? PipeTypeId { get; set; }
        public int? PressureId { get; set; }
        public int? SoilNearPipeId { get; set; }
        public int? TypeCoatingId { get; set; }

        public ListCoatingCondition CoatingCondition { get; set; }
        public LineExcavation LineExcavationNavigation { get; set; }
        public ListPipeToSoilCurrentType PipeToSoilCurrentType { get; set; }
        public ListPipeToSoilPolarity PipeToSoilPolarity { get; set; }
        public ListPipeType PipeType { get; set; }
        public ListPressure Pressure { get; set; }
        public ListSoilNearPipe SoilNearPipe { get; set; }
        public ListTypeCoating TypeCoating { get; set; }
        public WorkUnit WorkUnit { get; set; }
        public LineExcavation InverseLineExcavationNavigation { get; set; }
    }
}
