﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakData
    {
        public int LeakDataId { get; set; }
        public int WorkUnitId { get; set; }
        public int? VentedCgipercentGas { get; set; }
        public int? VentedCgipercentLel { get; set; }
        public long? VentedCgippm { get; set; }
        public string Town { get; set; }
        public string Address { get; set; }
        public string MapSectionNo { get; set; }
        public string Remarks { get; set; }
        public int? EstNoOfLeaks { get; set; }
        public string MeterSetOvgcno { get; set; }
        public bool? PublicReportedViaPhone { get; set; }
        public bool? PublicReportedViaCounter { get; set; }
        public bool? PublicReportedGasOdorStrong { get; set; }
        public bool? PublicReportedGasOdorFaint { get; set; }
        public string PublicReportedRemarks { get; set; }
        public string PublicReportedRecievedFrom { get; set; }
        public string PublicReportedPhone { get; set; }
        public bool? PublicReportedLocationInside { get; set; }
        public bool? PublicReportedLocationOutside { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public DateTime? RecievedOn { get; set; }
        public DateTime? ArrivedSceneOn { get; set; }
        public DateTime? DispatchedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public bool? FederalProperty { get; set; }
        public bool? OdorantSmellTest { get; set; }
        public bool? Monitor { get; set; }
        public DateTime? LeakEvaluationDate { get; set; }
        public DateTime? LeakDetectionDate { get; set; }
        public int? CityStateZipId { get; set; }
        public int? EstimatedSourceId { get; set; }
        public int? LeakClassId { get; set; }
        public int? LeakSurveyId { get; set; }
        public int? MeterSetLocationId { get; set; }
        public int? ResponsiblePartyId { get; set; }
        public int? StateId { get; set; }
        public int? SurfaceCoverId { get; set; }
        public int? OdorantSmellTestResultId { get; set; }
        public int? PipeSizeId { get; set; }
        public int? PipeTypeId { get; set; }
        public int? PressureId { get; set; }

        public CityStateZip CityStateZip { get; set; }
        public ListEstimatedSource EstimatedSource { get; set; }
        public ListLeakClass LeakClass { get; set; }
        public LeakData LeakDataNavigation { get; set; }
        public ListLeakSurvey LeakSurvey { get; set; }
        public ListMeterSetLocation MeterSetLocation { get; set; }
        public ListOdorantSmellTestResult OdorantSmellTestResult { get; set; }
        public ListPipeSize PipeSize { get; set; }
        public ListPipeType PipeType { get; set; }
        public ListPressure Pressure { get; set; }
        public ListResponsibleParty ResponsibleParty { get; set; }
        public ListState State { get; set; }
        public ListSurfaceCover SurfaceCover { get; set; }
        public WorkUnit WorkUnit { get; set; }
        public LeakData InverseLeakDataNavigation { get; set; }
    }
}
