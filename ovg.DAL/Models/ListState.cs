﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListState
    {
        public ListState()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string State { get; set; }
        public int? Order { get; set; }
        public int StateId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
