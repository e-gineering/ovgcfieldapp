﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListLeakClass
    {
        public ListLeakClass()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string LeakClass { get; set; }
        public int? Order { get; set; }
        public int LeakClassId { get; set; }
        public string LeakClassDesc { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
