﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakData2DetectedIn
    {
        public int LeakDataId { get; set; }
        public int WorkUnitId { get; set; }
        public int DetectedInId { get; set; }

        public ListDetectedIn DetectedIn { get; set; }
    }
}
