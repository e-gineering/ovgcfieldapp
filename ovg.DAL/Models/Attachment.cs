﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Attachment
    {
        public int AttachmentId { get; set; }
        public int WorkUnitId { get; set; }
        public string FileName { get; set; }
        public string ServerPath { get; set; }

        public WorkUnit WorkUnit { get; set; }
    }
}
