﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class PipeAndCoatingInspection
    {
        public int PipeAndCoatingInspectionId { get; set; }
        public int WorkUnitId { get; set; }
        public string MapSec { get; set; }
        public string Town { get; set; }
        public bool? IsAnodeOnExposedPipe { get; set; }
        public string AnodeOnExposedPipeExplain { get; set; }
        public string IsService { get; set; }
        public bool? IsServicePressure10psigOrMore { get; set; }
        public bool? IsFlowLimitiorInstalled { get; set; }
        public string SltestPressure1 { get; set; }
        public string SltestDuration1 { get; set; }
        public string SltotalPipeFtg1 { get; set; }
        public string SltestPressure2 { get; set; }
        public string SltestDuration2 { get; set; }
        public string SltotalPipeFtg2 { get; set; }
        public string PdWonumMain { get; set; }
        public string PdWonumService { get; set; }
        public string PdYearInstalledMain { get; set; }
        public string PdYearInstalledService { get; set; }
        public string PdSizeMain { get; set; }
        public string PdSizeService { get; set; }
        public string PdDepthMain { get; set; }
        public string PdDepthService { get; set; }
        public string PdPressureMain { get; set; }
        public string PdpressureService { get; set; }
        public bool? PdisCathoodicProtectedMain { get; set; }
        public bool? PdisCathoodicProtectedService { get; set; }
        public string PdcorrosionMeaterReadingMain { get; set; }
        public string PdcorrosionMeaterReadingService { get; set; }
        public bool? IsEnteredOnMap { get; set; }
        public string EnteredOnMapSigned { get; set; }
        public DateTime? EnteredOnMapSignedDate { get; set; }
        public string Signed { get; set; }
        public DateTime? SignedDate { get; set; }
        public string LocationOfExposedPipe { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public int? PipeInspectionTypeId { get; set; }
        public int? CoatingConditionServiceId { get; set; }
        public int? CoatingConditionMainId { get; set; }
        public int? PcoatingMaterialServiceId { get; set; }
        public int? PcoatingMaterialMainId { get; set; }
        public int? PconditionExtServiceId { get; set; }
        public int? PconditionExtMainId { get; set; }
        public int? PconditionIntServiceId { get; set; }
        public int? PconditionIntMainId { get; set; }
        public int? SoilNearPipeMainId { get; set; }
        public int? SoilNearPipeServiceId { get; set; }

        public ListCoatingCondition CoatingConditionMain { get; set; }
        public ListCoatingCondition CoatingConditionService { get; set; }
        public ListPipeCoatingMaterial PcoatingMaterialMain { get; set; }
        public ListPipeCoatingMaterial PcoatingMaterialService { get; set; }
        public ListPipeConditionExternal PconditionExtMain { get; set; }
        public ListPipeConditionExternal PconditionExtService { get; set; }
        public ListPipeConditionInternal PconditionIntMain { get; set; }
        public ListPipeConditionInternal PconditionIntService { get; set; }
        public ListPipeInspectionType PipeInspectionType { get; set; }
        public ListSoilNearPipe SoilNearPipeMain { get; set; }
        public ListSoilNearPipe SoilNearPipeService { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
