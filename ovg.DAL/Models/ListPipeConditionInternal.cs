﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeConditionInternal
    {
        public ListPipeConditionInternal()
        {
            LeakRepair2PipeConditionInternal = new HashSet<LeakRepair2PipeConditionInternal>();
            PipeAndCoatingInspectionPconditionIntMain = new HashSet<PipeAndCoatingInspection>();
            PipeAndCoatingInspectionPconditionIntService = new HashSet<PipeAndCoatingInspection>();
        }

        public string PipeConditionInternal { get; set; }
        public int? Order { get; set; }
        public int PipeConditionInternalId { get; set; }

        public ICollection<LeakRepair2PipeConditionInternal> LeakRepair2PipeConditionInternal { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPconditionIntMain { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPconditionIntService { get; set; }
    }
}
