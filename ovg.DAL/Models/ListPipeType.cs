﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeType
    {
        public ListPipeType()
        {
            LeakData = new HashSet<LeakData>();
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
        }

        public string PipeType { get; set; }
        public int? Order { get; set; }
        public int PipeTypeId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
    }
}
