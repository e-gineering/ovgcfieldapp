﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Mrtorder
    {
        public Mrtorder()
        {
            Mrtdetail = new HashSet<Mrtdetail>();
        }

        public int MrtorderId { get; set; }
        public int WorkUnitId { get; set; }
        public string Description { get; set; }
        public string SignedBy { get; set; }
        public DateTime? SignedByDate { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedByDate { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
        public ICollection<Mrtdetail> Mrtdetail { get; set; }
    }
}
