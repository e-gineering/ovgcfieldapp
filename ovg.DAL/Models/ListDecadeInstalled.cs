﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListDecadeInstalled
    {
        public ListDecadeInstalled()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string DecadeInstalled { get; set; }
        public int? Order { get; set; }
        public int DecadeInstalledId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
