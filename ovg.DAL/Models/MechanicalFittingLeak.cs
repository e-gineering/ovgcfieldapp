﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class MechanicalFittingLeak
    {
        public int MechanicalFittingLeakId { get; set; }
        public int WorkUnitId { get; set; }
        public string YearInstalled { get; set; }
        public string YearManufactured { get; set; }
        public string DecadeInstalled { get; set; }
        public string Manufacturer { get; set; }
        public string PartModelNumber { get; set; }
        public string LotNumber { get; set; }
        public string OtherAttributes { get; set; }
        public string SecondPipeSize { get; set; }
        public string SecondPipeUnit { get; set; }
        public string State { get; set; }
        public DateTime? DateOfFailure { get; set; }
        public bool? ThermalExpansionOrContraction { get; set; }
        public bool? ExcavationDamageAtTimeOfLeakDiscovery { get; set; }
        public bool? ExcavationDamagePreviousOfLeakDiscovery { get; set; }
        public bool? LeakDueToMaterialDefect { get; set; }
        public bool? LeakDueToDesignDefect { get; set; }
        public string Remarks { get; set; }
        public bool IsSpecification { get; set; }
        public string SpecificationNote { get; set; }
        public string SpecificationName { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public int? FittingId { get; set; }
        public int? FittingLeakLocationId { get; set; }
        public int? FittingMaterialId { get; set; }
        public int? FittingTypeId { get; set; }
        public int? MechanicalFittingLeakCauseId { get; set; }
        public int? MechanicalFittingLeakLocation1Id { get; set; }
        public int? MechanicalFittingLeakLocation2Id { get; set; }
        public int? MechanicalFittingLeakLocation3Id { get; set; }
        public int? MechanicalFittingLeakOccurredId { get; set; }
        public int? FirstPipeUnitId { get; set; }
        public int? FirstPipeMaterialId { get; set; }
        public int? SecondPipeMaterialId { get; set; }
        public int? FirstPipeSizeId { get; set; }

        public ListMaterial FirstPipeMaterial { get; set; }
        public ListPipeSize FirstPipeSize { get; set; }
        public ListPipeUnit FirstPipeUnit { get; set; }
        public ListFitting Fitting { get; set; }
        public ListFittingLeakLocation FittingLeakLocation { get; set; }
        public ListFittingMaterial FittingMaterial { get; set; }
        public ListFittingType FittingType { get; set; }
        public ListMechanicalFittingLeakCause MechanicalFittingLeakCause { get; set; }
        public ListMechanicalFittingLeakLocation1 MechanicalFittingLeakLocation1 { get; set; }
        public ListMechanicalFittingLeakLocation2 MechanicalFittingLeakLocation2 { get; set; }
        public ListMechanicalFittingLeakLocation3 MechanicalFittingLeakLocation3 { get; set; }
        public ListMechanicalFittingLeakOccurred MechanicalFittingLeakOccurred { get; set; }
        public ListMaterial SecondPipeMaterial { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
