﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class InvalidNonce
    {
        public string Nonce { get; set; }
    }
}
