﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeToSoilPolarity
    {
        public ListPipeToSoilPolarity()
        {
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
        }

        public string PipeToSoilPolarity { get; set; }
        public int? Order { get; set; }
        public int PipeToSoilPolarityId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
    }
}
