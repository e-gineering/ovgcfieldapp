﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListEnvironment
    {
        public ListEnvironment()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string Environment { get; set; }
        public int? Order { get; set; }
        public int EnvironmentId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
