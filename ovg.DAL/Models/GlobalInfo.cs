﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class GlobalInfo
    {
        public int GlobalInfoId { get; set; }
        public DateTime? AccountsChangedOn { get; set; }
        public DateTime? LastLocatesCronOn { get; set; }
    }
}
