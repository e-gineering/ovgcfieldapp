﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class StationCatalog
    {
        public StationCatalog()
        {
            RegulatorCatalog = new HashSet<RegulatorCatalog>();
            ReliefValveCatalog = new HashSet<ReliefValveCatalog>();
        }

        public int RegulatorStationId { get; set; }

        public ICollection<RegulatorCatalog> RegulatorCatalog { get; set; }
        public ICollection<ReliefValveCatalog> ReliefValveCatalog { get; set; }
    }
}
