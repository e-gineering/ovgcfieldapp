﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class SpecificationReport
    {
        public SpecificationReport()
        {
            WorkOrderSpecificationReport = new HashSet<WorkOrderSpecificationReport>();
        }

        public string SpecificationReportName { get; set; }
        public string FullyQualifiedTypeName { get; set; }
        public int SpecificationReportId { get; set; }

        public ICollection<WorkOrderSpecificationReport> WorkOrderSpecificationReport { get; set; }
    }
}
