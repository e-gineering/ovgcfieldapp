﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListOdorantSmellTestResult
    {
        public ListOdorantSmellTestResult()
        {
            LeakData = new HashSet<LeakData>();
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string OdorantSmellTestResult { get; set; }
        public int? Order { get; set; }
        public int OdorantSmellTestResultId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
