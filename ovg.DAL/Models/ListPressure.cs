﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPressure
    {
        public ListPressure()
        {
            LeakData = new HashSet<LeakData>();
            LineExcavation = new HashSet<LineExcavation>();
        }

        public string Pressure { get; set; }
        public int? Order { get; set; }
        public int PressureId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
    }
}
