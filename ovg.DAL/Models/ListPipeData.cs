﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeData
    {
        public ListPipeData()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string PipeData { get; set; }
        public int? Order { get; set; }
        public int PipeDataId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
