﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ovg.DAL.Models
{
    public partial class ovgContext : DbContext
    {
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Attachment> Attachment { get; set; }
        public virtual DbSet<CityStateZip> CityStateZip { get; set; }
        public virtual DbSet<CustomerAccount> CustomerAccount { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<GlobalInfo> GlobalInfo { get; set; }
        public virtual DbSet<InvalidNonce> InvalidNonce { get; set; }
        public virtual DbSet<LeakData> LeakData { get; set; }
        public virtual DbSet<LeakData2DetectedBy> LeakData2DetectedBy { get; set; }
        public virtual DbSet<LeakData2DetectedIn> LeakData2DetectedIn { get; set; }
        public virtual DbSet<LeakFinalReview> LeakFinalReview { get; set; }
        public virtual DbSet<LeakRepair> LeakRepair { get; set; }
        public virtual DbSet<LeakRepair2PipeConditionExternal> LeakRepair2PipeConditionExternal { get; set; }
        public virtual DbSet<LeakRepair2PipeConditionInternal> LeakRepair2PipeConditionInternal { get; set; }
        public virtual DbSet<LeakRepairVerification> LeakRepairVerification { get; set; }
        public virtual DbSet<LeakRepairVerification2DetectedBy> LeakRepairVerification2DetectedBy { get; set; }
        public virtual DbSet<LineExcavation> LineExcavation { get; set; }
        public virtual DbSet<LineExcavation2PipeConditionExternal> LineExcavation2PipeConditionExternal { get; set; }
        public virtual DbSet<ListCoatingCondition> ListCoatingCondition { get; set; }
        public virtual DbSet<ListComponent> ListComponent { get; set; }
        public virtual DbSet<ListDecadeInstalled> ListDecadeInstalled { get; set; }
        public virtual DbSet<ListDesignator> ListDesignator { get; set; }
        public virtual DbSet<ListDetectedBy> ListDetectedBy { get; set; }
        public virtual DbSet<ListDetectedIn> ListDetectedIn { get; set; }
        public virtual DbSet<ListEnvironment> ListEnvironment { get; set; }
        public virtual DbSet<ListEstimatedSource> ListEstimatedSource { get; set; }
        public virtual DbSet<ListFacilitiesMarkedType> ListFacilitiesMarkedType { get; set; }
        public virtual DbSet<ListFitting> ListFitting { get; set; }
        public virtual DbSet<ListFittingFailureCause> ListFittingFailureCause { get; set; }
        public virtual DbSet<ListFittingLeakLocation> ListFittingLeakLocation { get; set; }
        public virtual DbSet<ListFittingMaterial> ListFittingMaterial { get; set; }
        public virtual DbSet<ListFittingType> ListFittingType { get; set; }
        public virtual DbSet<ListLeakCause> ListLeakCause { get; set; }
        public virtual DbSet<ListLeakClass> ListLeakClass { get; set; }
        public virtual DbSet<ListLeakConfirmation> ListLeakConfirmation { get; set; }
        public virtual DbSet<ListLeakSurvey> ListLeakSurvey { get; set; }
        public virtual DbSet<ListLocation> ListLocation { get; set; }
        public virtual DbSet<ListMaterial> ListMaterial { get; set; }
        public virtual DbSet<ListMechanicalFittingLeakCause> ListMechanicalFittingLeakCause { get; set; }
        public virtual DbSet<ListMechanicalFittingLeakLocation1> ListMechanicalFittingLeakLocation1 { get; set; }
        public virtual DbSet<ListMechanicalFittingLeakLocation2> ListMechanicalFittingLeakLocation2 { get; set; }
        public virtual DbSet<ListMechanicalFittingLeakLocation3> ListMechanicalFittingLeakLocation3 { get; set; }
        public virtual DbSet<ListMechanicalFittingLeakOccurred> ListMechanicalFittingLeakOccurred { get; set; }
        public virtual DbSet<ListMeterSetLocation> ListMeterSetLocation { get; set; }
        public virtual DbSet<ListOdorantSmellTestResult> ListOdorantSmellTestResult { get; set; }
        public virtual DbSet<ListPipeCoatingMaterial> ListPipeCoatingMaterial { get; set; }
        public virtual DbSet<ListPipeConditionExternal> ListPipeConditionExternal { get; set; }
        public virtual DbSet<ListPipeConditionInternal> ListPipeConditionInternal { get; set; }
        public virtual DbSet<ListPipeData> ListPipeData { get; set; }
        public virtual DbSet<ListPipeInspectionType> ListPipeInspectionType { get; set; }
        public virtual DbSet<ListPipeSize> ListPipeSize { get; set; }
        public virtual DbSet<ListPipeToSoilCurrentType> ListPipeToSoilCurrentType { get; set; }
        public virtual DbSet<ListPipeToSoilPolarity> ListPipeToSoilPolarity { get; set; }
        public virtual DbSet<ListPipeType> ListPipeType { get; set; }
        public virtual DbSet<ListPipeUnit> ListPipeUnit { get; set; }
        public virtual DbSet<ListPressure> ListPressure { get; set; }
        public virtual DbSet<ListPressureUnits> ListPressureUnits { get; set; }
        public virtual DbSet<ListReportable> ListReportable { get; set; }
        public virtual DbSet<ListResponsibleParty> ListResponsibleParty { get; set; }
        public virtual DbSet<ListSoilNearPipe> ListSoilNearPipe { get; set; }
        public virtual DbSet<ListState> ListState { get; set; }
        public virtual DbSet<ListSurfaceCover> ListSurfaceCover { get; set; }
        public virtual DbSet<ListTypeCoating> ListTypeCoating { get; set; }
        public virtual DbSet<Locate> Locate { get; set; }
        public virtual DbSet<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
        public virtual DbSet<MeteringPressureFieldTest> MeteringPressureFieldTest { get; set; }
        public virtual DbSet<MiscAttribute> MiscAttribute { get; set; }
        public virtual DbSet<Miscellaneous> Miscellaneous { get; set; }
        public virtual DbSet<Mrtdetail> Mrtdetail { get; set; }
        public virtual DbSet<Mrtorder> Mrtorder { get; set; }
        public virtual DbSet<PipeAndCoatingInspection> PipeAndCoatingInspection { get; set; }
        public virtual DbSet<PipelinePressureTest> PipelinePressureTest { get; set; }
        public virtual DbSet<Po> Po { get; set; }
        public virtual DbSet<Podetail> Podetail { get; set; }
        public virtual DbSet<RegulatorCatalog> RegulatorCatalog { get; set; }
        public virtual DbSet<ReliefValveCatalog> ReliefValveCatalog { get; set; }
        public virtual DbSet<SecurityGroup> SecurityGroup { get; set; }
        public virtual DbSet<ServerError> ServerError { get; set; }
        public virtual DbSet<ServiceLineJobOrder> ServiceLineJobOrder { get; set; }
        public virtual DbSet<SpecificationReport> SpecificationReport { get; set; }
        public virtual DbSet<StationCatalog> StationCatalog { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<User2District> User2District { get; set; }
        public virtual DbSet<UserMessage> UserMessage { get; set; }
        public virtual DbSet<UserMessageThread> UserMessageThread { get; set; }
        public virtual DbSet<WorkOrder> WorkOrder { get; set; }
        public virtual DbSet<WorkOrderSpecificationReport> WorkOrderSpecificationReport { get; set; }
        public virtual DbSet<WorkUnit> WorkUnit { get; set; }
        public virtual DbSet<WorkUnitSnapShot> WorkUnitSnapShot { get; set; }
        public virtual DbSet<WorkUnitType> WorkUnitType { get; set; }

        // Unable to generate entity type for table 'dbo.GlobalSettings'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.InventoryTransactions'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.InventoryTransactionTable'. Please see the warning messages.

        public ovgContext(DbContextOptions<ovgContext> options) : base(options)
        {
        }

   /*      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
               {
                   if (!optionsBuilder.IsConfigured)
                   {
       #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                       optionsBuilder.UseSqlServer(@"connectionInfoHere");
                   }
               }*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FromErp)
                    .HasColumnName("FromERP")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Ovgnumber)
                    .HasColumnName("OVGNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.Property(e => e.AttachmentId).HasColumnName("AttachmentID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServerPath)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.Attachment)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Attachment_WorkUnit");
            });

            modelBuilder.Entity<CityStateZip>(entity =>
            {
                entity.Property(e => e.CityStateZipId).HasColumnName("CityStateZipID");

                entity.Property(e => e.CityStateZipName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.CityStateZip)
                    .HasForeignKey(d => d.DistrictId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CityStateZip_District");
            });

            modelBuilder.Entity<CustomerAccount>(entity =>
            {
                entity.Property(e => e.CustomerAccountId).HasColumnName("CustomerAccountID");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.Property(e => e.DistrictId)
                    .HasColumnName("DistrictID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DistrictDesc)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictIuupsidlist)
                    .HasColumnName("DistrictIUUPSIDList")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GlobalInfo>(entity =>
            {
                entity.Property(e => e.GlobalInfoId).HasColumnName("GlobalInfoID");

                entity.Property(e => e.AccountsChangedOn).HasColumnType("datetime");

                entity.Property(e => e.LastLocatesCronOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<InvalidNonce>(entity =>
            {
                entity.HasKey(e => e.Nonce);

                entity.Property(e => e.Nonce)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<LeakData>(entity =>
            {
                entity.Property(e => e.LeakDataId)
                    .HasColumnName("LeakDataID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ArrivedSceneOn).HasColumnType("datetime");

                entity.Property(e => e.CityStateZipId).HasColumnName("CityStateZipID");

                entity.Property(e => e.CompletedOn).HasColumnType("datetime");

                entity.Property(e => e.DispatchedOn).HasColumnType("datetime");

                entity.Property(e => e.EstimatedSourceId).HasColumnName("EstimatedSourceID");

                entity.Property(e => e.LeakClassId).HasColumnName("LeakClassID");

                entity.Property(e => e.LeakDetectionDate).HasColumnType("datetime");

                entity.Property(e => e.LeakEvaluationDate).HasColumnType("datetime");

                entity.Property(e => e.LeakSurveyId).HasColumnName("LeakSurveyID");

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MapSectionNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeterSetLocationId).HasColumnName("MeterSetLocationID");

                entity.Property(e => e.MeterSetOvgcno)
                    .HasColumnName("MeterSetOVGCNo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OdorantSmellTestResultId).HasColumnName("OdorantSmellTestResultID");

                entity.Property(e => e.PipeSizeId).HasColumnName("PipeSizeID");

                entity.Property(e => e.PipeTypeId).HasColumnName("PipeTypeID");

                entity.Property(e => e.PressureId).HasColumnName("PressureID");

                entity.Property(e => e.PublicReportedPhone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PublicReportedRecievedFrom)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PublicReportedRemarks)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RecievedOn).HasColumnType("datetime");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsiblePartyId).HasColumnName("ResponsiblePartyID");

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.SurfaceCoverId).HasColumnName("SurfaceCoverID");

                entity.Property(e => e.Town)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VentedCgipercentGas).HasColumnName("VentedCGIPercentGas");

                entity.Property(e => e.VentedCgipercentLel).HasColumnName("VentedCGIPercentLEL");

                entity.Property(e => e.VentedCgippm).HasColumnName("VentedCGIPPM");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.CityStateZip)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.CityStateZipId)
                    .HasConstraintName("FK_LeakData_CityStateZip");

                entity.HasOne(d => d.EstimatedSource)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.EstimatedSourceId)
                    .HasConstraintName("FK_Leak_EstimatedSource");

                entity.HasOne(d => d.LeakClass)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.LeakClassId)
                    .HasConstraintName("FK_LeakData_ListLeakClass");

                entity.HasOne(d => d.LeakDataNavigation)
                    .WithOne(p => p.InverseLeakDataNavigation)
                    .HasForeignKey<LeakData>(d => d.LeakDataId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakData_LeakData1");

                entity.HasOne(d => d.LeakSurvey)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.LeakSurveyId)
                    .HasConstraintName("FK_LeakData_ListLeakSurvey");

                entity.HasOne(d => d.MeterSetLocation)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.MeterSetLocationId)
                    .HasConstraintName("FK_Leak_MeterSetLocation");

                entity.HasOne(d => d.OdorantSmellTestResult)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.OdorantSmellTestResultId)
                    .HasConstraintName("FK_LeakData_ListOdorantSmellTestResult");

                entity.HasOne(d => d.PipeSize)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.PipeSizeId)
                    .HasConstraintName("FK_Leak_PipeSize");

                entity.HasOne(d => d.PipeType)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.PipeTypeId)
                    .HasConstraintName("FK_Leak_PipeType");

                entity.HasOne(d => d.Pressure)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.PressureId)
                    .HasConstraintName("FK_Leak_Pressure");

                entity.HasOne(d => d.ResponsibleParty)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.ResponsiblePartyId)
                    .HasConstraintName("FK_LeakData_ListResponsibleParty");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_Leak_State");

                entity.HasOne(d => d.SurfaceCover)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.SurfaceCoverId)
                    .HasConstraintName("FK_Leak_SurfaceCover");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LeakData)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leak_WorkUnit");
            });

            modelBuilder.Entity<LeakData2DetectedBy>(entity =>
            {
                entity.HasKey(e => new { e.LeakDataId, e.DetectedById });

                entity.Property(e => e.LeakDataId).HasColumnName("LeakDataID");

                entity.Property(e => e.DetectedById).HasColumnName("DetectedByID");

                entity.Property(e => e.VentedCgitestValue).HasColumnName("VentedCGITestValue");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.DetectedBy)
                    .WithMany(p => p.LeakData2DetectedBy)
                    .HasForeignKey(d => d.DetectedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leak2DetectedBy_DetectedBy");
            });

            modelBuilder.Entity<LeakData2DetectedIn>(entity =>
            {
                entity.HasKey(e => new { e.LeakDataId, e.DetectedInId });

                entity.Property(e => e.LeakDataId).HasColumnName("LeakDataID");

                entity.Property(e => e.DetectedInId).HasColumnName("DetectedInID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.DetectedIn)
                    .WithMany(p => p.LeakData2DetectedIn)
                    .HasForeignKey(d => d.DetectedInId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leak2DetectedIn_DetectedIn");
            });

            modelBuilder.Entity<LeakFinalReview>(entity =>
            {
                entity.Property(e => e.LeakFinalReviewId)
                    .HasColumnName("LeakFinalReviewID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DesignatorId).HasColumnName("DesignatorID");

                entity.Property(e => e.Dimprelated).HasColumnName("DIMPRelated");

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ReportableId).HasColumnName("ReportableID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.Designator)
                    .WithMany(p => p.LeakFinalReview)
                    .HasForeignKey(d => d.DesignatorId)
                    .HasConstraintName("FK_LeakFinalReview_LeakFinalReview");

                entity.HasOne(d => d.LeakFinalReviewNavigation)
                    .WithOne(p => p.InverseLeakFinalReviewNavigation)
                    .HasForeignKey<LeakFinalReview>(d => d.LeakFinalReviewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakFinalReview_LeakFinalReview1");

                entity.HasOne(d => d.Reportable)
                    .WithMany(p => p.LeakFinalReview)
                    .HasForeignKey(d => d.ReportableId)
                    .HasConstraintName("FK_LeakFinalReview_ListReportable");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LeakFinalReview)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakFinalReview_WorkUnit");
            });

            modelBuilder.Entity<LeakRepair>(entity =>
            {
                entity.Property(e => e.LeakRepairId).HasColumnName("LeakRepairID");

                entity.Property(e => e.CoatingConditionId).HasColumnName("CoatingConditionID");

                entity.Property(e => e.ComponentId).HasColumnName("ComponentID");

                entity.Property(e => e.DecadeInstalledId).HasColumnName("DecadeInstalledID");

                entity.Property(e => e.DetailOfComponentFailure)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DetailOfRepair)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EnvironmentId).HasColumnName("EnvironmentID");

                entity.Property(e => e.LeakCauseId).HasColumnName("LeakCauseID");

                entity.Property(e => e.LeakConfirmationId).HasColumnName("LeakConfirmationID");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mrtnum)
                    .HasColumnName("MRTNum")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OdorantSmellTestResultId).HasColumnName("OdorantSmellTestResultID");

                entity.Property(e => e.PipeDataId).HasColumnName("PipeDataID");

                entity.Property(e => e.PipeSize)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PipeToSoilCurrentTypeId).HasColumnName("PipeToSoilCurrentTypeID");

                entity.Property(e => e.PipeToSoilPolarityId).HasColumnName("PipeToSoilPolarityID");

                entity.Property(e => e.PipeTypeId).HasColumnName("PipeTypeID");

                entity.Property(e => e.Pressure)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureUnitsId).HasColumnName("PressureUnitsID");

                entity.Property(e => e.RepairDate).HasColumnType("datetime");

                entity.Property(e => e.SoilNearPipeId).HasColumnName("SoilNearPipeID");

                entity.Property(e => e.TypeCoatingId).HasColumnName("TypeCoatingID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.CoatingCondition)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.CoatingConditionId)
                    .HasConstraintName("FK_LeakRepair_ListCoatingCondition");

                entity.HasOne(d => d.Component)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.ComponentId)
                    .HasConstraintName("FK_LeakRepair_ListComponent");

                entity.HasOne(d => d.DecadeInstalled)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.DecadeInstalledId)
                    .HasConstraintName("FK_LeakRepair_ListDecadeInstalled");

                entity.HasOne(d => d.Environment)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.EnvironmentId)
                    .HasConstraintName("FK_LeakRepair_ListEnvironment");

                entity.HasOne(d => d.LeakCause)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.LeakCauseId)
                    .HasConstraintName("FK_LeakRepair_ListLeakCause");

                entity.HasOne(d => d.LeakConfirmation)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.LeakConfirmationId)
                    .HasConstraintName("FK_LeakRepair_ListPositiveNegative1");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_LeakRepair_ListLocation");

                entity.HasOne(d => d.OdorantSmellTestResult)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.OdorantSmellTestResultId)
                    .HasConstraintName("FK_LeakRepair_ListOdorantSmellTestResult");

                entity.HasOne(d => d.PipeData)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.PipeDataId)
                    .HasConstraintName("FK_LeakRepair_ListPipeData");

                entity.HasOne(d => d.PipeToSoilCurrentType)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.PipeToSoilCurrentTypeId)
                    .HasConstraintName("FK_LeakRepair_ListPipeToSoilCurrentType");

                entity.HasOne(d => d.PipeToSoilPolarity)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.PipeToSoilPolarityId)
                    .HasConstraintName("FK_LeakRepair_ListPipeToSoilPolarity");

                entity.HasOne(d => d.PipeType)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.PipeTypeId)
                    .HasConstraintName("FK_LeakRepair_ListPipeType");

                entity.HasOne(d => d.PressureUnits)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.PressureUnitsId)
                    .HasConstraintName("FK_LeakRepair_ListPressureUnits");

                entity.HasOne(d => d.SoilNearPipe)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.SoilNearPipeId)
                    .HasConstraintName("FK_LeakRepair_ListSoilNearPipe");

                entity.HasOne(d => d.TypeCoating)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.TypeCoatingId)
                    .HasConstraintName("FK_LeakRepair_ListTypeCoating");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LeakRepair)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakRepair_WorkUnit");
            });

            modelBuilder.Entity<LeakRepair2PipeConditionExternal>(entity =>
            {
                entity.HasKey(e => new { e.LeakRepairId, e.PipeConditionExternalId });

                entity.Property(e => e.LeakRepairId).HasColumnName("LeakRepairID");

                entity.Property(e => e.PipeConditionExternalId).HasColumnName("PipeConditionExternalID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.LeakRepair)
                    .WithMany(p => p.LeakRepair2PipeConditionExternal)
                    .HasForeignKey(d => d.LeakRepairId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakRepair2PipeConditionExternal_LeakRepair");

                entity.HasOne(d => d.PipeConditionExternal)
                    .WithMany(p => p.LeakRepair2PipeConditionExternal)
                    .HasForeignKey(d => d.PipeConditionExternalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leak2PipeConditionExternal_PipeConditionExternal");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LeakRepair2PipeConditionExternal)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakRepair2PipeConditionExternal_WorkUnit");
            });

            modelBuilder.Entity<LeakRepair2PipeConditionInternal>(entity =>
            {
                entity.HasKey(e => new { e.PipeConditionInternalId, e.LeakRepairId });

                entity.Property(e => e.PipeConditionInternalId).HasColumnName("PipeConditionInternalID");

                entity.Property(e => e.LeakRepairId).HasColumnName("LeakRepairID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.PipeConditionInternal)
                    .WithMany(p => p.LeakRepair2PipeConditionInternal)
                    .HasForeignKey(d => d.PipeConditionInternalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leak2PipeConditionInternal_PipeConditionInternal");
            });

            modelBuilder.Entity<LeakRepairVerification>(entity =>
            {
                entity.Property(e => e.LeakRepairVerificationId).HasColumnName("LeakRepairVerificationID");

                entity.Property(e => e.LeakConfirmation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.RepairVerificationDate).HasColumnType("datetime");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LeakRepairVerification)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakRepairVerification_WorkUnit");
            });

            modelBuilder.Entity<LeakRepairVerification2DetectedBy>(entity =>
            {
                entity.HasKey(e => new { e.DetectedById, e.LeakRepairVerificationId });

                entity.Property(e => e.DetectedById).HasColumnName("DetectedByID");

                entity.Property(e => e.LeakRepairVerificationId).HasColumnName("LeakRepairVerificationID");

                entity.Property(e => e.VentedCgitestValue).HasColumnName("VentedCGITestValue");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.DetectedBy)
                    .WithMany(p => p.LeakRepairVerification2DetectedBy)
                    .HasForeignKey(d => d.DetectedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeakRepairVerification2DetectedBy_ListDetectedBy");
            });

            modelBuilder.Entity<LineExcavation>(entity =>
            {
                entity.Property(e => e.LineExcavationId)
                    .HasColumnName("LineExcavationID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CoatingConditionId).HasColumnName("CoatingConditionID");

                entity.Property(e => e.DistanceToMainLine)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ExcavationPurpose)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExcavationType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Excavator)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LocationOrReferencePoint)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MapSection)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PipeDepth)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PipeSize)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PipeToSoilCurrentTypeId).HasColumnName("PipeToSoilCurrentTypeID");

                entity.Property(e => e.PipeToSoilPolarityId).HasColumnName("PipeToSoilPolarityID");

                entity.Property(e => e.PipeTypeId).HasColumnName("PipeTypeID");

                entity.Property(e => e.PressureId).HasColumnName("PressureID");

                entity.Property(e => e.RepairsMadeNeeded)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SoilNearPipeId).HasColumnName("SoilNearPipeID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.TypeCoatingId).HasColumnName("TypeCoatingID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.CoatingCondition)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.CoatingConditionId)
                    .HasConstraintName("FK_LineExcavation_ListCoatingCondition");

                entity.HasOne(d => d.LineExcavationNavigation)
                    .WithOne(p => p.InverseLineExcavationNavigation)
                    .HasForeignKey<LineExcavation>(d => d.LineExcavationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LineExcavation_LineExcavation");

                entity.HasOne(d => d.PipeToSoilCurrentType)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.PipeToSoilCurrentTypeId)
                    .HasConstraintName("FK_LineExcavation_ListPipeToSoilCurrentType");

                entity.HasOne(d => d.PipeToSoilPolarity)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.PipeToSoilPolarityId)
                    .HasConstraintName("FK_LineExcavation_ListPipeToSoilPolarity");

                entity.HasOne(d => d.PipeType)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.PipeTypeId)
                    .HasConstraintName("FK_LineExcavation_ListPipeType");

                entity.HasOne(d => d.Pressure)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.PressureId)
                    .HasConstraintName("FK_DistributionLineExcavation_Pressure");

                entity.HasOne(d => d.SoilNearPipe)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.SoilNearPipeId)
                    .HasConstraintName("FK_LineExcavation_ListSoilNearPipe");

                entity.HasOne(d => d.TypeCoating)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.TypeCoatingId)
                    .HasConstraintName("FK_LineExcavation_ListTypeCoating");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LineExcavation)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DistributionLineExcavation_WorkUnit");
            });

            modelBuilder.Entity<LineExcavation2PipeConditionExternal>(entity =>
            {
                entity.HasKey(e => new { e.PipeConditionExternalId, e.LineExcavationId });

                entity.Property(e => e.PipeConditionExternalId).HasColumnName("PipeConditionExternalID");

                entity.Property(e => e.LineExcavationId).HasColumnName("LineExcavationID");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.PipeConditionExternal)
                    .WithMany(p => p.LineExcavation2PipeConditionExternal)
                    .HasForeignKey(d => d.PipeConditionExternalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LineExcavation2PipeConditionExternal_ListPipeConditionExternal");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.LineExcavation2PipeConditionExternal)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LineExcavation2PipeConditionExternal_WorkUnit");
            });

            modelBuilder.Entity<ListCoatingCondition>(entity =>
            {
                entity.HasKey(e => e.CoatingConditionId);

                entity.Property(e => e.CoatingConditionId).HasColumnName("CoatingConditionID");

                entity.Property(e => e.CoatingCondition)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListComponent>(entity =>
            {
                entity.HasKey(e => e.ComponentId);

                entity.Property(e => e.ComponentId).HasColumnName("ComponentID");

                entity.Property(e => e.Component)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListDecadeInstalled>(entity =>
            {
                entity.HasKey(e => e.DecadeInstalledId);

                entity.Property(e => e.DecadeInstalledId).HasColumnName("DecadeInstalledID");

                entity.Property(e => e.DecadeInstalled)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListDesignator>(entity =>
            {
                entity.HasKey(e => e.DesignatorId);

                entity.Property(e => e.DesignatorId).HasColumnName("DesignatorID");

                entity.Property(e => e.Designator)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListDetectedBy>(entity =>
            {
                entity.HasKey(e => e.DetectedById);

                entity.Property(e => e.DetectedById).HasColumnName("DetectedByID");

                entity.Property(e => e.DetectedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListDetectedIn>(entity =>
            {
                entity.HasKey(e => e.DetectedInId);

                entity.Property(e => e.DetectedInId).HasColumnName("DetectedInID");

                entity.Property(e => e.DetectedIn)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListEnvironment>(entity =>
            {
                entity.HasKey(e => e.EnvironmentId);

                entity.Property(e => e.EnvironmentId).HasColumnName("EnvironmentID");

                entity.Property(e => e.Environment)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListEstimatedSource>(entity =>
            {
                entity.HasKey(e => e.EstimatedSourceId);

                entity.Property(e => e.EstimatedSourceId).HasColumnName("EstimatedSourceID");

                entity.Property(e => e.EstimatedSource)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListFacilitiesMarkedType>(entity =>
            {
                entity.HasKey(e => e.FacilitiesMarkedTypeId);

                entity.Property(e => e.FacilitiesMarkedTypeId).HasColumnName("FacilitiesMarkedTypeID");

                entity.Property(e => e.FacilitiesMarkedType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListFitting>(entity =>
            {
                entity.HasKey(e => e.FittingId);

                entity.Property(e => e.FittingId).HasColumnName("FittingID");

                entity.Property(e => e.Fitting)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListFittingFailureCause>(entity =>
            {
                entity.HasKey(e => e.FittingFailureCause);

                entity.Property(e => e.FittingFailureCause)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<ListFittingLeakLocation>(entity =>
            {
                entity.HasKey(e => e.FittingLeakLocationId);

                entity.Property(e => e.FittingLeakLocationId).HasColumnName("FittingLeakLocationID");

                entity.Property(e => e.FittingLeakLocation)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListFittingMaterial>(entity =>
            {
                entity.HasKey(e => e.FittingMaterialId);

                entity.Property(e => e.FittingMaterialId).HasColumnName("FittingMaterialID");

                entity.Property(e => e.FittingMaterial)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListFittingType>(entity =>
            {
                entity.HasKey(e => e.FittingTypeId);

                entity.Property(e => e.FittingTypeId).HasColumnName("FittingTypeID");

                entity.Property(e => e.FittingType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListLeakCause>(entity =>
            {
                entity.HasKey(e => e.LeakCauseId);

                entity.Property(e => e.LeakCauseId).HasColumnName("LeakCauseID");

                entity.Property(e => e.LeakCause)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListLeakClass>(entity =>
            {
                entity.HasKey(e => e.LeakClassId);

                entity.Property(e => e.LeakClassId)
                    .HasColumnName("LeakClassID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LeakClass)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LeakClassDesc)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListLeakConfirmation>(entity =>
            {
                entity.HasKey(e => e.LeakConfirmationId);

                entity.Property(e => e.LeakConfirmationId).HasColumnName("LeakConfirmationID");

                entity.Property(e => e.LeakConfirmation)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListLeakSurvey>(entity =>
            {
                entity.HasKey(e => e.LeakSurveyId);

                entity.Property(e => e.LeakSurveyId).HasColumnName("LeakSurveyID");

                entity.Property(e => e.LeakSurvey)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListLocation>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMaterial>(entity =>
            {
                entity.HasKey(e => e.MaterialId);

                entity.Property(e => e.MaterialId).HasColumnName("MaterialID");

                entity.Property(e => e.Material)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMechanicalFittingLeakCause>(entity =>
            {
                entity.HasKey(e => e.MechanicalFittingLeakCauseId);

                entity.Property(e => e.MechanicalFittingLeakCauseId).HasColumnName("MechanicalFittingLeakCauseID");

                entity.Property(e => e.MechanicalFittingLeakCause)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMechanicalFittingLeakLocation1>(entity =>
            {
                entity.HasKey(e => e.MechanicalFittingLeakLocation1Id);

                entity.Property(e => e.MechanicalFittingLeakLocation1Id).HasColumnName("MechanicalFittingLeakLocation1ID");

                entity.Property(e => e.MechanicalFittingLeakLocation1)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMechanicalFittingLeakLocation2>(entity =>
            {
                entity.HasKey(e => e.MechanicalFittingLeakLocation2Id);

                entity.Property(e => e.MechanicalFittingLeakLocation2Id).HasColumnName("MechanicalFittingLeakLocation2ID");

                entity.Property(e => e.MechanicalFittingLeakLocation2)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMechanicalFittingLeakLocation3>(entity =>
            {
                entity.HasKey(e => e.MechanicalFittingLeakLocation3Id);

                entity.Property(e => e.MechanicalFittingLeakLocation3Id).HasColumnName("MechanicalFittingLeakLocation3ID");

                entity.Property(e => e.MechanicalFittingLeakLocation3)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMechanicalFittingLeakOccurred>(entity =>
            {
                entity.HasKey(e => e.MechanicalFittingLeakOccurredId);

                entity.Property(e => e.MechanicalFittingLeakOccurredId).HasColumnName("MechanicalFittingLeakOccurredID");

                entity.Property(e => e.MechanicalFittingLeakOccurred)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListMeterSetLocation>(entity =>
            {
                entity.HasKey(e => e.MeterSetLocationId);

                entity.Property(e => e.MeterSetLocationId).HasColumnName("MeterSetLocationID");

                entity.Property(e => e.MeterSetLocation)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListOdorantSmellTestResult>(entity =>
            {
                entity.HasKey(e => e.OdorantSmellTestResultId);

                entity.Property(e => e.OdorantSmellTestResultId).HasColumnName("OdorantSmellTestResultID");

                entity.Property(e => e.OdorantSmellTestResult)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeCoatingMaterial>(entity =>
            {
                entity.HasKey(e => e.PipeCoatingMaterialId);

                entity.Property(e => e.PipeCoatingMaterialId).HasColumnName("PipeCoatingMaterialID");

                entity.Property(e => e.PipeCoatingMaterial)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeConditionExternal>(entity =>
            {
                entity.HasKey(e => e.PipeConditionExternalId);

                entity.Property(e => e.PipeConditionExternalId).HasColumnName("PipeConditionExternalID");

                entity.Property(e => e.PipeConditionExternal)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeConditionInternal>(entity =>
            {
                entity.HasKey(e => e.PipeConditionInternalId);

                entity.Property(e => e.PipeConditionInternalId).HasColumnName("PipeConditionInternalID");

                entity.Property(e => e.PipeConditionInternal)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeData>(entity =>
            {
                entity.HasKey(e => e.PipeDataId);

                entity.Property(e => e.PipeDataId).HasColumnName("PipeDataID");

                entity.Property(e => e.PipeData)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeInspectionType>(entity =>
            {
                entity.HasKey(e => e.PipeInspectionTypeId);

                entity.Property(e => e.PipeInspectionTypeId).HasColumnName("PipeInspectionTypeID");

                entity.Property(e => e.PipeInspectionType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeSize>(entity =>
            {
                entity.HasKey(e => e.PipeSizeId);

                entity.Property(e => e.PipeSizeId).HasColumnName("PipeSizeID");

                entity.Property(e => e.PipeSize)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeToSoilCurrentType>(entity =>
            {
                entity.HasKey(e => e.PipeToSoilCurrentTypeId);

                entity.Property(e => e.PipeToSoilCurrentTypeId).HasColumnName("PipeToSoilCurrentTypeID");

                entity.Property(e => e.PipeToSoilCurrentType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeToSoilPolarity>(entity =>
            {
                entity.HasKey(e => e.PipeToSoilPolarityId);

                entity.Property(e => e.PipeToSoilPolarityId).HasColumnName("PipeToSoilPolarityID");

                entity.Property(e => e.PipeToSoilPolarity)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeType>(entity =>
            {
                entity.HasKey(e => e.PipeTypeId);

                entity.Property(e => e.PipeTypeId).HasColumnName("PipeTypeID");

                entity.Property(e => e.PipeType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPipeUnit>(entity =>
            {
                entity.HasKey(e => e.PipeUnitId);

                entity.Property(e => e.PipeUnitId).HasColumnName("PipeUnitID");

                entity.Property(e => e.PipeUnit)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPressure>(entity =>
            {
                entity.HasKey(e => e.PressureId);

                entity.Property(e => e.PressureId).HasColumnName("PressureID");

                entity.Property(e => e.Pressure)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListPressureUnits>(entity =>
            {
                entity.HasKey(e => e.PressureUnitsId);

                entity.Property(e => e.PressureUnitsId).HasColumnName("PressureUnitsID");

                entity.Property(e => e.PressureUnits)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListReportable>(entity =>
            {
                entity.HasKey(e => e.ReportableId);

                entity.Property(e => e.ReportableId).HasColumnName("ReportableID");

                entity.Property(e => e.Reportable)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListResponsibleParty>(entity =>
            {
                entity.HasKey(e => e.ResponsiblePartyId);

                entity.Property(e => e.ResponsiblePartyId).HasColumnName("ResponsiblePartyID");

                entity.Property(e => e.ResponsibleParty)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListSoilNearPipe>(entity =>
            {
                entity.HasKey(e => e.SoilNearPipeId);

                entity.Property(e => e.SoilNearPipeId).HasColumnName("SoilNearPipeID");

                entity.Property(e => e.SoilNearPipe)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListState>(entity =>
            {
                entity.HasKey(e => e.StateId);

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListSurfaceCover>(entity =>
            {
                entity.HasKey(e => e.SurfaceCoverId);

                entity.Property(e => e.SurfaceCoverId).HasColumnName("SurfaceCoverID");

                entity.Property(e => e.SurfaceCover)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListTypeCoating>(entity =>
            {
                entity.HasKey(e => e.TypeCoatingId);

                entity.Property(e => e.TypeCoatingId).HasColumnName("TypeCoatingID");

                entity.Property(e => e.TypeCoating)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Locate>(entity =>
            {
                entity.Property(e => e.LocateId).HasColumnName("LocateID");

                entity.Property(e => e.ContactedRequestorDate).HasColumnType("datetime");

                entity.Property(e => e.EmailBody).IsUnicode(false);

                entity.Property(e => e.EmailMessageId)
                    .HasColumnName("EmailMessageID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailUid)
                    .HasColumnName("EmailUID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FacilitiesMarkedTypeId).HasColumnName("FacilitiesMarkedTypeID");

                entity.Property(e => e.FacilitiesMarkedTypeOther)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FollowUpRequestorRepDate).HasColumnType("datetime");

                entity.Property(e => e.FollowUpRequestorRepRemarks)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IuppsticketDate)
                    .HasColumnName("IUPPSTicketDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IuppsticketNo)
                    .HasColumnName("IUPPSTicketNo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteAddress)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteCity)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteContact)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteCounty)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteDate).HasColumnType("datetime");

                entity.Property(e => e.JobSiteLocation)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.JobSitePhoneNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteTownship)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobSiteTypeOfWork)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MetRequestorDate).HasColumnType("datetime");

                entity.Property(e => e.OvgfacilitiesAffected).HasColumnName("OVGFacilitiesAffected");

                entity.Property(e => e.OvgfacilitiesMarked).HasColumnName("OVGFacilitiesMarked");

                entity.Property(e => e.OvgfacilitiesMarkedDate)
                    .HasColumnName("OVGFacilitiesMarkedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RequestorRepCompany)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestorRepName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WhatWasLocated)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.FacilitiesMarkedType)
                    .WithMany(p => p.Locate)
                    .HasForeignKey(d => d.FacilitiesMarkedTypeId)
                    .HasConstraintName("FK_Locate_FacilitiesMarkedType");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.Locate)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locate_Ticket1");
            });

            modelBuilder.Entity<MechanicalFittingLeak>(entity =>
            {
                entity.Property(e => e.MechanicalFittingLeakId).HasColumnName("MechanicalFittingLeakID");

                entity.Property(e => e.DateOfFailure).HasColumnType("datetime");

                entity.Property(e => e.DecadeInstalled)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstPipeMaterialId).HasColumnName("FirstPipeMaterialID");

                entity.Property(e => e.FirstPipeSizeId).HasColumnName("FirstPipeSizeID");

                entity.Property(e => e.FirstPipeUnitId).HasColumnName("FirstPipeUnitID");

                entity.Property(e => e.FittingId).HasColumnName("FittingID");

                entity.Property(e => e.FittingLeakLocationId).HasColumnName("FittingLeakLocationID");

                entity.Property(e => e.FittingMaterialId).HasColumnName("FittingMaterialID");

                entity.Property(e => e.FittingTypeId).HasColumnName("FittingTypeID");

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LotNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MechanicalFittingLeakCauseId).HasColumnName("MechanicalFittingLeakCauseID");

                entity.Property(e => e.MechanicalFittingLeakLocation1Id).HasColumnName("MechanicalFittingLeakLocation1ID");

                entity.Property(e => e.MechanicalFittingLeakLocation2Id).HasColumnName("MechanicalFittingLeakLocation2ID");

                entity.Property(e => e.MechanicalFittingLeakLocation3Id).HasColumnName("MechanicalFittingLeakLocation3ID");

                entity.Property(e => e.MechanicalFittingLeakOccurredId).HasColumnName("MechanicalFittingLeakOccurredID");

                entity.Property(e => e.OtherAttributes)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PartModelNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SecondPipeMaterialId).HasColumnName("SecondPipeMaterialID");

                entity.Property(e => e.SecondPipeSize)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SecondPipeUnit)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecificationName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecificationNote)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.Property(e => e.YearInstalled)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.YearManufactured)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.FirstPipeMaterial)
                    .WithMany(p => p.MechanicalFittingLeakFirstPipeMaterial)
                    .HasForeignKey(d => d.FirstPipeMaterialId)
                    .HasConstraintName("FK_MechanicalFittingLeak_Material");

                entity.HasOne(d => d.FirstPipeSize)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FirstPipeSizeId)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListPipeSize");

                entity.HasOne(d => d.FirstPipeUnit)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FirstPipeUnitId)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListPipeUnit");

                entity.HasOne(d => d.Fitting)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FittingId)
                    .HasConstraintName("FK_MechanicalFittingLeak_Fitting");

                entity.HasOne(d => d.FittingLeakLocation)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FittingLeakLocationId)
                    .HasConstraintName("FK_MechanicalFittingLeak_FittingLeakLocation");

                entity.HasOne(d => d.FittingMaterial)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FittingMaterialId)
                    .HasConstraintName("FK_MechanicalFittingLeak_FittingMaterial");

                entity.HasOne(d => d.FittingType)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.FittingTypeId)
                    .HasConstraintName("FK_MechanicalFittingLeak_FittingType");

                entity.HasOne(d => d.MechanicalFittingLeakCause)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.MechanicalFittingLeakCauseId)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListMechanicalFittingLeakCause");

                entity.HasOne(d => d.MechanicalFittingLeakLocation1)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.MechanicalFittingLeakLocation1Id)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListMechanicalFittingLeakLocation1");

                entity.HasOne(d => d.MechanicalFittingLeakLocation2)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.MechanicalFittingLeakLocation2Id)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListMechanicalFittingLeakLocation2");

                entity.HasOne(d => d.MechanicalFittingLeakLocation3)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.MechanicalFittingLeakLocation3Id)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListMechanicalFittingLeakLocation3");

                entity.HasOne(d => d.MechanicalFittingLeakOccurred)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.MechanicalFittingLeakOccurredId)
                    .HasConstraintName("FK_MechanicalFittingLeak_ListMechanicalFittingLeakOccurred");

                entity.HasOne(d => d.SecondPipeMaterial)
                    .WithMany(p => p.MechanicalFittingLeakSecondPipeMaterial)
                    .HasForeignKey(d => d.SecondPipeMaterialId)
                    .HasConstraintName("FK_MechanicalFittingLeak_Material1");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.MechanicalFittingLeak)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MechanicalFittingLeak_WorkUnit");
            });

            modelBuilder.Entity<MeteringPressureFieldTest>(entity =>
            {
                entity.Property(e => e.MeteringPressureFieldTestId).HasColumnName("MeteringPressureFieldTestID");

                entity.Property(e => e.AsFoundGaugeIb)
                    .HasColumnName("AsFoundGaugeIB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsFoundGaugeIboz).HasColumnName("AsFoundGaugeIBOz");

                entity.Property(e => e.AsFoundGaugeIbwc).HasColumnName("AsFoundGaugeIBWC");

                entity.Property(e => e.AsFoundGaugeIiipsig)
                    .HasColumnName("AsFoundGaugeIIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsFoundGaugeIipsig)
                    .HasColumnName("AsFoundGaugeIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsFoundInstrumentIiipsig)
                    .HasColumnName("AsFoundInstrumentIIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsFoundInstrumentIipsig)
                    .HasColumnName("AsFoundInstrumentIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsFoundInstrumentVoltage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftGaugeIb)
                    .HasColumnName("AsLeftGaugeIB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftGaugeIboz).HasColumnName("AsLeftGaugeIBOz");

                entity.Property(e => e.AsLeftGaugeIbwc).HasColumnName("AsLeftGaugeIBWC");

                entity.Property(e => e.AsLeftGaugeIiipsig)
                    .HasColumnName("AsLeftGaugeIIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftGaugeIipsig)
                    .HasColumnName("AsLeftGaugeIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftInstrumentIiipsig)
                    .HasColumnName("AsLeftInstrumentIIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftInstrumentIipsig)
                    .HasColumnName("AsLeftInstrumentIIPSIG")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsLeftInstrumentVoltage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EstimatedConsumption)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GasTempAtMeter)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InCorrectedReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InInstrumentSerial)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InMechanicalReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InUncorrectedReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LengthOfBypass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeterFlowAfterBypass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeterFlowBeforeBypass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutCorrectedReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutInstrumentSerial)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutMechanicalReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutUncorrectedReading)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureChangeFrom)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureChangeFromInWc).HasColumnName("PressureChangeFromInWC");

                entity.Property(e => e.PressureChangeFromPsig).HasColumnName("PressureChangeFromPSIG");

                entity.Property(e => e.PressureChangeTo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureChangeToInWc).HasColumnName("PressureChangeToInWC");

                entity.Property(e => e.PressureChangeToPsig).HasColumnName("PressureChangeToPSIG");

                entity.Property(e => e.RegularIndexIn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RegularIndexOut)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RotaryMeterIndexIn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RotaryMeterIndexOut)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Signature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SignatureDate).HasColumnType("datetime");

                entity.Property(e => e.SpecificationName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecificationNote)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.MeteringPressureFieldTest)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MeteringPressureFieldTest_WorkUnit");
            });

            modelBuilder.Entity<MiscAttribute>(entity =>
            {
                entity.Property(e => e.MiscAttributeId).HasColumnName("MiscAttributeID");

                entity.Property(e => e.MiscAttributeName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Miscellaneous>(entity =>
            {
                entity.HasKey(e => e.MiscId);

                entity.Property(e => e.MiscId)
                    .HasColumnName("MiscID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MiscAttributeId).HasColumnName("MiscAttributeID");

                entity.Property(e => e.Value).HasColumnType("money");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.MiscAttribute)
                    .WithMany(p => p.Miscellaneous)
                    .HasForeignKey(d => d.MiscAttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Miscellaneous_MiscAttribute");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.Miscellaneous)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Miscellaneous_WorkUnit");
            });

            modelBuilder.Entity<Mrtdetail>(entity =>
            {
                entity.ToTable("MRTDetail");

                entity.Property(e => e.MrtdetailId).HasColumnName("MRTDetailID");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.MrtorderId).HasColumnName("MRTOrderID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.StockId).HasColumnName("StockID");

                entity.Property(e => e.StockNum)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.UnitOfMeasure)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.Mrtorder)
                    .WithMany(p => p.Mrtdetail)
                    .HasForeignKey(d => d.MrtorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MRTDetail_MRTOrder");

                entity.HasOne(d => d.Stock)
                    .WithMany(p => p.Mrtdetail)
                    .HasForeignKey(d => d.StockId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MRTDetail_Stock");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.Mrtdetail)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MRTDetail_WorkUnit");
            });

            modelBuilder.Entity<Mrtorder>(entity =>
            {
                entity.ToTable("MRTOrder");

                entity.Property(e => e.MrtorderId).HasColumnName("MRTOrderID");

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedByDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SignedBy)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SignedByDate).HasColumnType("datetime");

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.Mrtorder)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MRTOrder_WorkUnit");
            });

            modelBuilder.Entity<PipeAndCoatingInspection>(entity =>
            {
                entity.Property(e => e.PipeAndCoatingInspectionId).HasColumnName("PipeAndCoatingInspectionID");

                entity.Property(e => e.AnodeOnExposedPipeExplain)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CoatingConditionMainId).HasColumnName("CoatingConditionMainID");

                entity.Property(e => e.CoatingConditionServiceId).HasColumnName("CoatingConditionServiceID");

                entity.Property(e => e.EnteredOnMapSigned)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EnteredOnMapSignedDate).HasColumnType("datetime");

                entity.Property(e => e.IsAnodeOnExposedPipe).HasColumnName("isAnodeOnExposedPipe");

                entity.Property(e => e.IsEnteredOnMap).HasColumnName("isEnteredOnMap");

                entity.Property(e => e.IsFlowLimitiorInstalled).HasColumnName("isFlowLimitiorInstalled");

                entity.Property(e => e.IsService)
                    .HasColumnName("isService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.IsServicePressure10psigOrMore).HasColumnName("isServicePressure10psigOrMore");

                entity.Property(e => e.LocationOfExposedPipe)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MapSec)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PcoatingMaterialMainId).HasColumnName("PCoatingMaterialMainID");

                entity.Property(e => e.PcoatingMaterialServiceId).HasColumnName("PCoatingMaterialServiceID");

                entity.Property(e => e.PconditionExtMainId).HasColumnName("PConditionExtMainID");

                entity.Property(e => e.PconditionExtServiceId).HasColumnName("PConditionExtServiceID");

                entity.Property(e => e.PconditionIntMainId).HasColumnName("PConditionIntMainID");

                entity.Property(e => e.PconditionIntServiceId).HasColumnName("PConditionIntServiceID");

                entity.Property(e => e.PdDepthMain)
                    .HasColumnName("PD_DepthMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdDepthService)
                    .HasColumnName("PD_DepthService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdPressureMain)
                    .HasColumnName("PD_PressureMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdSizeMain)
                    .HasColumnName("PD_SizeMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdSizeService)
                    .HasColumnName("PD_SizeService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdWonumMain)
                    .HasColumnName("PD_WONumMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdWonumService)
                    .HasColumnName("PD_WONumService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdYearInstalledMain)
                    .HasColumnName("PD_YearInstalledMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdYearInstalledService)
                    .HasColumnName("PD_YearInstalledService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdcorrosionMeaterReadingMain)
                    .HasColumnName("PDCorrosionMeaterReadingMain")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdcorrosionMeaterReadingService)
                    .HasColumnName("PDCorrosionMeaterReadingService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PdisCathoodicProtectedMain).HasColumnName("PDisCathoodicProtectedMain");

                entity.Property(e => e.PdisCathoodicProtectedService).HasColumnName("PDisCathoodicProtectedService");

                entity.Property(e => e.PdpressureService)
                    .HasColumnName("PDPressureService")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PipeInspectionTypeId).HasColumnName("PipeInspectionTypeID");

                entity.Property(e => e.Signed)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SignedDate).HasColumnType("datetime");

                entity.Property(e => e.SltestDuration1)
                    .HasColumnName("SLTestDuration1")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SltestDuration2)
                    .HasColumnName("SLTestDuration2")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SltestPressure1)
                    .HasColumnName("SLTestPressure1")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SltestPressure2)
                    .HasColumnName("SLTestPressure2")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SltotalPipeFtg1)
                    .HasColumnName("SLTotalPipeFtg1")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SltotalPipeFtg2)
                    .HasColumnName("SLTotalPipeFtg2")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SoilNearPipeMainId).HasColumnName("SoilNearPipeMainID");

                entity.Property(e => e.SoilNearPipeServiceId).HasColumnName("SoilNearPipeServiceID");

                entity.Property(e => e.Town)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.CoatingConditionMain)
                    .WithMany(p => p.PipeAndCoatingInspectionCoatingConditionMain)
                    .HasForeignKey(d => d.CoatingConditionMainId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_CoatingCondition");

                entity.HasOne(d => d.CoatingConditionService)
                    .WithMany(p => p.PipeAndCoatingInspectionCoatingConditionService)
                    .HasForeignKey(d => d.CoatingConditionServiceId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_CoatingCondition1");

                entity.HasOne(d => d.PcoatingMaterialMain)
                    .WithMany(p => p.PipeAndCoatingInspectionPcoatingMaterialMain)
                    .HasForeignKey(d => d.PcoatingMaterialMainId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeCoatingMaterial1");

                entity.HasOne(d => d.PcoatingMaterialService)
                    .WithMany(p => p.PipeAndCoatingInspectionPcoatingMaterialService)
                    .HasForeignKey(d => d.PcoatingMaterialServiceId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeCoatingMaterial");

                entity.HasOne(d => d.PconditionExtMain)
                    .WithMany(p => p.PipeAndCoatingInspectionPconditionExtMain)
                    .HasForeignKey(d => d.PconditionExtMainId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeConditionExternal");

                entity.HasOne(d => d.PconditionExtService)
                    .WithMany(p => p.PipeAndCoatingInspectionPconditionExtService)
                    .HasForeignKey(d => d.PconditionExtServiceId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeConditionExternal1");

                entity.HasOne(d => d.PconditionIntMain)
                    .WithMany(p => p.PipeAndCoatingInspectionPconditionIntMain)
                    .HasForeignKey(d => d.PconditionIntMainId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeConditionInternal");

                entity.HasOne(d => d.PconditionIntService)
                    .WithMany(p => p.PipeAndCoatingInspectionPconditionIntService)
                    .HasForeignKey(d => d.PconditionIntServiceId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeConditionInternal1");

                entity.HasOne(d => d.PipeInspectionType)
                    .WithMany(p => p.PipeAndCoatingInspection)
                    .HasForeignKey(d => d.PipeInspectionTypeId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_PipeInspectionType");

                entity.HasOne(d => d.SoilNearPipeMain)
                    .WithMany(p => p.PipeAndCoatingInspectionSoilNearPipeMain)
                    .HasForeignKey(d => d.SoilNearPipeMainId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_SoilNearPipe");

                entity.HasOne(d => d.SoilNearPipeService)
                    .WithMany(p => p.PipeAndCoatingInspectionSoilNearPipeService)
                    .HasForeignKey(d => d.SoilNearPipeServiceId)
                    .HasConstraintName("FK_PipeAndCoatingInspection_SoilNearPipe1");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.PipeAndCoatingInspection)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_frm100_PipeInspect_WorkUnit");
            });

            modelBuilder.Entity<PipelinePressureTest>(entity =>
            {
                entity.Property(e => e.PipelinePressureTestId).HasColumnName("PipelinePressureTestID");

                entity.Property(e => e.ApprovedSignature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedSignatureDate).HasColumnType("datetime");

                entity.Property(e => e.ClassLocation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Duration)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EngineeringDrawingNum)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Grade)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LeakAndFailureComments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LongitudinalSeamType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MapNum)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaxOperatingPressure)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaxSafeTestPressure)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MinTestPressure)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MiscOrderNum)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe1Grade)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe1Length)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe1Size)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe1Type)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe1WallThickness)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe2Grade)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe2Length)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe2Size)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe2Type)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe2WallThickness)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe3Grade)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe3Length)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe3Size)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe3Type)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pipe3WallThickness)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureAtSmys)
                    .HasColumnName("PressureAtSMYS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureDropDueTo)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PressureEnd)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureStart)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PressureTestNum)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Route)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Signature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SignatureDate).HasColumnType("datetime");

                entity.Property(e => e.Town)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WallThickness)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.PipelinePressureTest)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PipelinePressureTest_WorkUnit");
            });

            modelBuilder.Entity<Po>(entity =>
            {
                entity.HasKey(e => e.Ponum);

                entity.ToTable("PO");

                entity.Property(e => e.Ponum)
                    .HasColumnName("PONum")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Podate)
                    .HasColumnName("PODate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");
            });

            modelBuilder.Entity<Podetail>(entity =>
            {
                entity.ToTable("PODetail");

                entity.Property(e => e.PodetailId).HasColumnName("PODetailID");

                entity.Property(e => e.CostPerUnit).HasColumnType("money");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Ponum)
                    .IsRequired()
                    .HasColumnName("PONum")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StockId).HasColumnName("StockID");

                entity.Property(e => e.StockNum)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.UnitOfMeasure)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PonumNavigation)
                    .WithMany(p => p.Podetail)
                    .HasForeignKey(d => d.Ponum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PODetail_PO");

                entity.HasOne(d => d.Stock)
                    .WithMany(p => p.Podetail)
                    .HasForeignKey(d => d.StockId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PODetail_Stock");
            });

            modelBuilder.Entity<RegulatorCatalog>(entity =>
            {
                entity.HasKey(e => e.RegulatorId);

                entity.Property(e => e.RegulatorId)
                    .HasColumnName("RegulatorID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RegulatorStationId).HasColumnName("RegulatorStationID");

                entity.HasOne(d => d.RegulatorStation)
                    .WithMany(p => p.RegulatorCatalog)
                    .HasForeignKey(d => d.RegulatorStationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Regulator_RegulatorStation");
            });

            modelBuilder.Entity<ReliefValveCatalog>(entity =>
            {
                entity.HasKey(e => e.ReliefValveId);

                entity.Property(e => e.ReliefValveId)
                    .HasColumnName("ReliefValveID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RegulatorStationId).HasColumnName("RegulatorStationID");

                entity.HasOne(d => d.RegulatorStation)
                    .WithMany(p => p.ReliefValveCatalog)
                    .HasForeignKey(d => d.RegulatorStationId)
                    .HasConstraintName("FK_ReliefValve_RegulatorStation");
            });

            modelBuilder.Entity<SecurityGroup>(entity =>
            {
                entity.Property(e => e.SecurityGroupId).HasColumnName("SecurityGroupID");

                entity.Property(e => e.SecurityGroup1)
                    .IsRequired()
                    .HasColumnName("SecurityGroup")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ServerError>(entity =>
            {
                entity.Property(e => e.ServerErrorId).HasColumnName("ServerErrorID");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Source).IsUnicode(false);

                entity.Property(e => e.StackTrace).IsUnicode(false);
            });

            modelBuilder.Entity<ServiceLineJobOrder>(entity =>
            {
                entity.Property(e => e.ServiceLineJobOrderId).HasColumnName("ServiceLineJobOrderID");

                entity.Property(e => e.AccountNo)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.AccountingDeptDate).HasColumnType("datetime");

                entity.Property(e => e.AccountingDeptSignature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.BillingDeptDate).HasColumnType("datetime");

                entity.Property(e => e.BillingDeptSignature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictManagerSignature1)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictManagerSignature2)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EngineeringDeptDate).HasColumnType("datetime");

                entity.Property(e => e.EngineeringDeptSignature)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.HeatingApplicationNo)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.JobOrderNum)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.LeakFrm106Submitted).HasColumnName("LeakFrm106_Submitted");

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mrtno)
                    .HasColumnName("MRTNo")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PipeInspFrm100Submitted).HasColumnName("PipeInspFrm100_Submitted");

                entity.Property(e => e.ReasonForWork)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceAddress)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SlworkToBeDone)
                    .HasColumnName("SLWorkToBeDone")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Town)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.ServiceLineJobOrder)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_frm133_SLJO_WorkUnit");
            });

            modelBuilder.Entity<SpecificationReport>(entity =>
            {
                entity.Property(e => e.SpecificationReportId).HasColumnName("SpecificationReportID");

                entity.Property(e => e.FullyQualifiedTypeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecificationReportName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StationCatalog>(entity =>
            {
                entity.HasKey(e => e.RegulatorStationId);

                entity.Property(e => e.RegulatorStationId)
                    .HasColumnName("RegulatorStationID")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.Property(e => e.StockId).HasColumnName("StockID");

                entity.Property(e => e.CurrentInventory).HasDefaultValueSql("((0))");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.StockNum)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitOfMeasure)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.UserName)
                    .HasName("UserLoginEmailUnique")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.LastCheckin).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityGroupId).HasColumnName("SecurityGroupID");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.SecurityGroup)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.SecurityGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_SecurityGroup");
            });

            modelBuilder.Entity<User2District>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.DistrictId });

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.User2District)
                    .HasForeignKey(d => d.DistrictId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User2District_District");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.User2District)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User2District_User");
            });

            modelBuilder.Entity<UserMessage>(entity =>
            {
                entity.Property(e => e.UserMessageId).HasColumnName("UserMessageID");

                entity.Property(e => e.FromUserId).HasColumnName("FromUserID");

                entity.Property(e => e.MessageContent)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.MessageCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.UserMessageThreadId).HasColumnName("UserMessageThreadID");
            });

            modelBuilder.Entity<UserMessageThread>(entity =>
            {
                entity.HasKey(e => new { e.UserMessageThreadId, e.UserId });

                entity.Property(e => e.UserMessageThreadId).HasColumnName("UserMessageThreadID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ThreadCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");
            });

            modelBuilder.Entity<WorkOrder>(entity =>
            {
                entity.Property(e => e.WorkOrderId).HasColumnName("WorkOrderID");

                entity.Property(e => e.CustomerNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CwodatePlacedInService)
                    .HasColumnName("CWODatePlacedInService")
                    .HasColumnType("datetime");

                entity.Property(e => e.CwodistrictManager)
                    .HasColumnName("CWODistrictManager")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Cwoengineering)
                    .HasColumnName("CWOEngineering")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CwoisInstalled).HasColumnName("CWOIsInstalled");

                entity.Property(e => e.CwoisRetired).HasColumnName("CWOIsRetired");

                entity.Property(e => e.CwoisUpgraded).HasColumnName("CWOIsUpgraded");

                entity.Property(e => e.Cwomrtnumbers)
                    .HasColumnName("CWOMRTNumbers")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CworegulatorStationLocation)
                    .HasColumnName("CWORegulatorStationLocation")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CworegulatorStationNo)
                    .HasColumnName("CWORegulatorStationNo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CworeportsAttached).HasColumnName("CWOReportsAttached");

                entity.Property(e => e.CwoworkCompleted)
                    .HasColumnName("CWOWorkCompleted")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionNotes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EstimatedCost)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LockedOnDate).HasColumnType("datetime");

                entity.Property(e => e.LockedSig)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MapSectionNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mjocomments)
                    .HasColumnName("MJOComments")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mjocompleted)
                    .HasColumnName("MJOCompleted")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MjodistrictManager)
                    .HasColumnName("MJODistrictManager")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MjoreportsAttached).HasColumnName("MJOReportsAttached");

                entity.Property(e => e.OrderLocation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OvgcbookNo)
                    .HasColumnName("OVGCBookNo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectCoordinator)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Purpose)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ToServe)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.HasOne(d => d.WorkUnit)
                    .WithMany(p => p.WorkOrder)
                    .HasForeignKey(d => d.WorkUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MaintenanceJobOrder_WorkUnit");
            });

            modelBuilder.Entity<WorkOrderSpecificationReport>(entity =>
            {
                entity.Property(e => e.WorkOrderSpecificationReportId).HasColumnName("WorkOrderSpecificationReportID");

                entity.Property(e => e.Note)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecificationReportId).HasColumnName("SpecificationReportID");

                entity.Property(e => e.WorkOrderId).HasColumnName("WorkOrderID");

                entity.HasOne(d => d.SpecificationReport)
                    .WithMany(p => p.WorkOrderSpecificationReport)
                    .HasForeignKey(d => d.SpecificationReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkOrderSpecification_SpecificationReport");

                entity.HasOne(d => d.WorkOrder)
                    .WithMany(p => p.WorkOrderSpecificationReport)
                    .HasForeignKey(d => d.WorkOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkOrderSpecification_WorkOrder");
            });

            modelBuilder.Entity<WorkUnit>(entity =>
            {
                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.Property(e => e.ApprovedClosedByDate).HasColumnType("datetime");

                entity.Property(e => e.AssignedToUserOnDate).HasColumnType("datetime");

                entity.Property(e => e.CheckedOutId).HasColumnName("CheckedOutID");

                entity.Property(e => e.CheckedOutOnDate).HasColumnType("datetime");

                entity.Property(e => e.ClosedByDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOnDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.LastUpdatedOnDate).HasColumnType("datetime");

                entity.Property(e => e.MachineName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ParentWorkUnitId).HasColumnName("ParentWorkUnitID");

                entity.Property(e => e.WorkUnitAutoId)
                    .HasColumnName("WorkUnitAutoID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitTypeId).HasColumnName("WorkUnitTypeID");

                entity.HasOne(d => d.ApprovedClosedByNavigation)
                    .WithMany(p => p.WorkUnitApprovedClosedByNavigation)
                    .HasForeignKey(d => d.ApprovedClosedBy)
                    .HasConstraintName("FK_Ticket_User3");

                entity.HasOne(d => d.AssignedToUserNavigation)
                    .WithMany(p => p.WorkUnitAssignedToUserNavigation)
                    .HasForeignKey(d => d.AssignedToUser)
                    .HasConstraintName("FK_Ticket_User1");

                entity.HasOne(d => d.CheckedOutByNavigation)
                    .WithMany(p => p.WorkUnitCheckedOutByNavigation)
                    .HasForeignKey(d => d.CheckedOutBy)
                    .HasConstraintName("FK_WorkUnit_User");

                entity.HasOne(d => d.ClosedByNavigation)
                    .WithMany(p => p.WorkUnitClosedByNavigation)
                    .HasForeignKey(d => d.ClosedBy)
                    .HasConstraintName("FK_Ticket_User2");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.WorkUnitCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkUnit_User1");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.WorkUnit)
                    .HasForeignKey(d => d.DistrictId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkUnit_District");

                entity.HasOne(d => d.ParentWorkUnit)
                    .WithMany(p => p.InverseParentWorkUnit)
                    .HasForeignKey(d => d.ParentWorkUnitId)
                    .HasConstraintName("FK_WorkUnit_WorkUnit");

                entity.HasOne(d => d.WorkUnitType)
                    .WithMany(p => p.WorkUnit)
                    .HasForeignKey(d => d.WorkUnitTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkUnit_WorkUnitType");
            });

            modelBuilder.Entity<WorkUnitSnapShot>(entity =>
            {
                entity.Property(e => e.WorkUnitSnapShotId).HasColumnName("WorkUnitSnapShotID");

                entity.Property(e => e.ApprovedClosedByDate).HasColumnType("datetime");

                entity.Property(e => e.AssignedToUserOnDate).HasColumnType("datetime");

                entity.Property(e => e.CheckedOutOnDate).HasColumnType("datetime");

                entity.Property(e => e.ClosedByDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOnDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.DistrictId)
                    .HasColumnName("DistrictID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsInitSoftPullbackDate).HasColumnType("datetime");

                entity.Property(e => e.IsSoftPullbackCompletedDate).HasColumnType("datetime");

                entity.Property(e => e.Jsonpackage)
                    .HasColumnName("JSONPackage")
                    .IsUnicode(false);

                entity.Property(e => e.SnapShotCreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.WorkUnitAutoId)
                    .HasColumnName("WorkUnitAutoID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitId).HasColumnName("WorkUnitID");

                entity.Property(e => e.WorkUnitTypeId).HasColumnName("WorkUnitTypeID");
            });

            modelBuilder.Entity<WorkUnitType>(entity =>
            {
                entity.Property(e => e.WorkUnitTypeId).HasColumnName("WorkUnitTypeID");

                entity.Property(e => e.WorkUnitType1)
                    .IsRequired()
                    .HasColumnName("WorkUnitType")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
