﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class UserMessage
    {
        public int UserMessageId { get; set; }
        public Guid UserMessageThreadId { get; set; }
        public string MessageContent { get; set; }
        public DateTime MessageCreated { get; set; }
        public int FromUserId { get; set; }
    }
}
