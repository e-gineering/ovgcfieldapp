﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakRepairVerification2DetectedBy
    {
        public int LeakRepairVerificationId { get; set; }
        public int WorkUnitId { get; set; }
        public int? VentedCgitestValue { get; set; }
        public int DetectedById { get; set; }

        public ListDetectedBy DetectedBy { get; set; }
    }
}
