﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMeterSetLocation
    {
        public ListMeterSetLocation()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string MeterSetLocation { get; set; }
        public int? Order { get; set; }
        public int MeterSetLocationId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
