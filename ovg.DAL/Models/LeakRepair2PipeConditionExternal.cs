﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakRepair2PipeConditionExternal
    {
        public int LeakRepairId { get; set; }
        public int WorkUnitId { get; set; }
        public int PipeConditionExternalId { get; set; }

        public LeakRepair LeakRepair { get; set; }
        public ListPipeConditionExternal PipeConditionExternal { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
