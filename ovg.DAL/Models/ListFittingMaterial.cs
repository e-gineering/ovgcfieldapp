﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFittingMaterial
    {
        public ListFittingMaterial()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string FittingMaterial { get; set; }
        public int? Order { get; set; }
        public int FittingMaterialId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
