﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFittingLeakLocation
    {
        public ListFittingLeakLocation()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string FittingLeakLocation { get; set; }
        public int? Order { get; set; }
        public int FittingLeakLocationId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
