﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFacilitiesMarkedType
    {
        public ListFacilitiesMarkedType()
        {
            Locate = new HashSet<Locate>();
        }

        public string FacilitiesMarkedType { get; set; }
        public int? Order { get; set; }
        public int FacilitiesMarkedTypeId { get; set; }

        public ICollection<Locate> Locate { get; set; }
    }
}
