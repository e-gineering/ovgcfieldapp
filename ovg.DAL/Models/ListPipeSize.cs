﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeSize
    {
        public ListPipeSize()
        {
            LeakData = new HashSet<LeakData>();
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string PipeSize { get; set; }
        public int? Order { get; set; }
        public int PipeSizeId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
