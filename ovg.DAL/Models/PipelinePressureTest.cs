﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class PipelinePressureTest
    {
        public int PipelinePressureTestId { get; set; }
        public int WorkUnitId { get; set; }
        public string Town { get; set; }
        public string MapNum { get; set; }
        public string Route { get; set; }
        public string PressureTestNum { get; set; }
        public string MiscOrderNum { get; set; }
        public string EngineeringDrawingNum { get; set; }
        public bool? IsLowPressure { get; set; }
        public bool? IsIntermediatePressure { get; set; }
        public bool? IsHighPressure { get; set; }
        public string MaxOperatingPressure { get; set; }
        public string MinTestPressure { get; set; }
        public string MaxSafeTestPressure { get; set; }
        public string WallThickness { get; set; }
        public string Grade { get; set; }
        public string PressureAtSmys { get; set; }
        public string ClassLocation { get; set; }
        public string LongitudinalSeamType { get; set; }
        public string Pipe1Size { get; set; }
        public string Pipe1Type { get; set; }
        public string Pipe1Length { get; set; }
        public string Pipe1WallThickness { get; set; }
        public string Pipe1Grade { get; set; }
        public string Pipe2Size { get; set; }
        public string Pipe2Type { get; set; }
        public string Pipe2Length { get; set; }
        public string Pipe2WallThickness { get; set; }
        public string Pipe2Grade { get; set; }
        public string Pipe3Size { get; set; }
        public string Pipe3Type { get; set; }
        public string Pipe3Length { get; set; }
        public string Pipe3WallThickness { get; set; }
        public string Pipe3Grade { get; set; }
        public string PressureStart { get; set; }
        public string PressureEnd { get; set; }
        public string Duration { get; set; }
        public bool? IsMediaWater { get; set; }
        public bool? IsMediaAir { get; set; }
        public bool? IsMediaNitrogen { get; set; }
        public bool? IsMediaNaturalGas { get; set; }
        public bool? IsInstrumentGauge { get; set; }
        public bool? IsInstrumentRecorder { get; set; }
        public bool? SatisfactoryPressureDropNegligible { get; set; }
        public bool? SatisfactoryPressureDrop { get; set; }
        public string PressureDropDueTo { get; set; }
        public string Signature { get; set; }
        public DateTime? SignatureDate { get; set; }
        public string ApprovedSignature { get; set; }
        public DateTime? ApprovedSignatureDate { get; set; }
        public string LeakAndFailureComments { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
    }
}
