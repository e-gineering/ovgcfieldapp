﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListSurfaceCover
    {
        public ListSurfaceCover()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string SurfaceCover { get; set; }
        public int? Order { get; set; }
        public int SurfaceCoverId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
