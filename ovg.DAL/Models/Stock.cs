﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Stock
    {
        public Stock()
        {
            Mrtdetail = new HashSet<Mrtdetail>();
            Podetail = new HashSet<Podetail>();
        }

        public int StockId { get; set; }
        public string StockNum { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public int CurrentInventory { get; set; }

        public ICollection<Mrtdetail> Mrtdetail { get; set; }
        public ICollection<Podetail> Podetail { get; set; }
    }
}
