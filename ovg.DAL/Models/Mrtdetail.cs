﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Mrtdetail
    {
        public int MrtdetailId { get; set; }
        public int WorkUnitId { get; set; }
        public int MrtorderId { get; set; }
        public int StockId { get; set; }
        public string StockNum { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string UnitOfMeasure { get; set; }
        public int QuantityUsed { get; set; }
        public int QuantityReturned { get; set; }
        public DateTime Date { get; set; }
        public bool IsInventoryProcessed { get; set; }

        public Mrtorder Mrtorder { get; set; }
        public Stock Stock { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
