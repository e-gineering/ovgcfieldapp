﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Podetail
    {
        public int PodetailId { get; set; }
        public string Ponum { get; set; }
        public int StockId { get; set; }
        public int Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
        public string StockNum { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public decimal CostPerUnit { get; set; }
        public DateTime CreateDate { get; set; }

        public Po PonumNavigation { get; set; }
        public Stock Stock { get; set; }
    }
}
