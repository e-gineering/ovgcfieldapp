﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListSoilNearPipe
    {
        public ListSoilNearPipe()
        {
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
            PipeAndCoatingInspectionSoilNearPipeMain = new HashSet<PipeAndCoatingInspection>();
            PipeAndCoatingInspectionSoilNearPipeService = new HashSet<PipeAndCoatingInspection>();
        }

        public string SoilNearPipe { get; set; }
        public int? Order { get; set; }
        public int SoilNearPipeId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionSoilNearPipeMain { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionSoilNearPipeService { get; set; }
    }
}
