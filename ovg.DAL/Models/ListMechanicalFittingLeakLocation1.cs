﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMechanicalFittingLeakLocation1
    {
        public ListMechanicalFittingLeakLocation1()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string MechanicalFittingLeakLocation1 { get; set; }
        public int? Order { get; set; }
        public int MechanicalFittingLeakLocation1Id { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
