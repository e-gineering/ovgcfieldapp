﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class UserMessageThread
    {
        public Guid UserMessageThreadId { get; set; }
        public int UserId { get; set; }
        public DateTime ThreadCreated { get; set; }
    }
}
