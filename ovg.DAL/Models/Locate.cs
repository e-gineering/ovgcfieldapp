﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Locate
    {
        public int LocateId { get; set; }
        public int WorkUnitId { get; set; }
        public string IuppsticketNo { get; set; }
        public DateTime? IuppsticketDate { get; set; }
        public bool? FromRequestCanUnderstandProject { get; set; }
        public bool? OvgfacilitiesAffected { get; set; }
        public DateTime? JobSiteDate { get; set; }
        public string JobSiteCounty { get; set; }
        public string JobSiteTownship { get; set; }
        public string JobSiteCity { get; set; }
        public string JobSiteAddress { get; set; }
        public string JobSiteLocation { get; set; }
        public string JobSiteTypeOfWork { get; set; }
        public string JobSiteContact { get; set; }
        public string JobSitePhoneNo { get; set; }
        public bool? ContactedRequestor { get; set; }
        public DateTime? ContactedRequestorDate { get; set; }
        public bool? MetRequestorRep { get; set; }
        public DateTime? MetRequestorDate { get; set; }
        public string RequestorRepName { get; set; }
        public string RequestorRepCompany { get; set; }
        public bool? OvgfacilitiesMarked { get; set; }
        public DateTime? OvgfacilitiesMarkedDate { get; set; }
        public string WhatWasLocated { get; set; }
        public bool? IsSystemHighPressure { get; set; }
        public string FacilitiesMarkedTypeOther { get; set; }
        public bool? FollowUpRequestorRep { get; set; }
        public DateTime? FollowUpRequestorRepDate { get; set; }
        public string FollowUpRequestorRepRemarks { get; set; }
        public string EmailBody { get; set; }
        public string EmailUid { get; set; }
        public string EmailMessageId { get; set; }
        public string EmailSubject { get; set; }
        public bool? IsEmergencyTicket { get; set; }
        public int? MainCount { get; set; }
        public int? ServiceCount { get; set; }
        public int? FuelLineCount { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public int? FacilitiesMarkedTypeId { get; set; }

        public ListFacilitiesMarkedType FacilitiesMarkedType { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
