﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class MiscAttribute
    {
        public MiscAttribute()
        {
            Miscellaneous = new HashSet<Miscellaneous>();
        }

        public string MiscAttributeName { get; set; }
        public int MiscAttributeId { get; set; }

        public ICollection<Miscellaneous> Miscellaneous { get; set; }
    }
}
