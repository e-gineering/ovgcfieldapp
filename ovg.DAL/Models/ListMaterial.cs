﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMaterial
    {
        public ListMaterial()
        {
            MechanicalFittingLeakFirstPipeMaterial = new HashSet<MechanicalFittingLeak>();
            MechanicalFittingLeakSecondPipeMaterial = new HashSet<MechanicalFittingLeak>();
        }

        public string Material { get; set; }
        public int? Order { get; set; }
        public int MaterialId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeakFirstPipeMaterial { get; set; }
        public ICollection<MechanicalFittingLeak> MechanicalFittingLeakSecondPipeMaterial { get; set; }
    }
}
