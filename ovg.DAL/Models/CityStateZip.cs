﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class CityStateZip
    {
        public CityStateZip()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string CityStateZipName { get; set; }
        public int CityStateZipId { get; set; }
        public int DistrictId { get; set; }

        public District District { get; set; }
        public ICollection<LeakData> LeakData { get; set; }
    }
}
