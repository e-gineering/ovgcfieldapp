﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListEstimatedSource
    {
        public ListEstimatedSource()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string EstimatedSource { get; set; }
        public int? Order { get; set; }
        public int EstimatedSourceId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
