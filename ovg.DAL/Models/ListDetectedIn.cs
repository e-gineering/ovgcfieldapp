﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListDetectedIn
    {
        public ListDetectedIn()
        {
            LeakData2DetectedIn = new HashSet<LeakData2DetectedIn>();
        }

        public string DetectedIn { get; set; }
        public int? Order { get; set; }
        public int DetectedInId { get; set; }

        public ICollection<LeakData2DetectedIn> LeakData2DetectedIn { get; set; }
    }
}
