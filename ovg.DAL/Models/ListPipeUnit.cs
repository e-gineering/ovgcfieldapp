﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeUnit
    {
        public ListPipeUnit()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string PipeUnit { get; set; }
        public int? Order { get; set; }
        public int PipeUnitId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
