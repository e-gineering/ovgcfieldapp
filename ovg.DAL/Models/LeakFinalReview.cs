﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakFinalReview
    {
        public int LeakFinalReviewId { get; set; }
        public int WorkUnitId { get; set; }
        public string Notes { get; set; }
        public bool? Dimprelated { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public int? DesignatorId { get; set; }
        public int? ReportableId { get; set; }

        public ListDesignator Designator { get; set; }
        public LeakFinalReview LeakFinalReviewNavigation { get; set; }
        public ListReportable Reportable { get; set; }
        public WorkUnit WorkUnit { get; set; }
        public LeakFinalReview InverseLeakFinalReviewNavigation { get; set; }
    }
}
