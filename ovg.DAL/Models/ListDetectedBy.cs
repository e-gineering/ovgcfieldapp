﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListDetectedBy
    {
        public ListDetectedBy()
        {
            LeakData2DetectedBy = new HashSet<LeakData2DetectedBy>();
            LeakRepairVerification2DetectedBy = new HashSet<LeakRepairVerification2DetectedBy>();
        }

        public string DetectedBy { get; set; }
        public int? Order { get; set; }
        public int DetectedById { get; set; }

        public ICollection<LeakData2DetectedBy> LeakData2DetectedBy { get; set; }
        public ICollection<LeakRepairVerification2DetectedBy> LeakRepairVerification2DetectedBy { get; set; }
    }
}
