﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class WorkUnitType
    {
        public WorkUnitType()
        {
            WorkUnit = new HashSet<WorkUnit>();
        }

        public string WorkUnitType1 { get; set; }
        public int? Order { get; set; }
        public int WorkUnitTypeId { get; set; }

        public ICollection<WorkUnit> WorkUnit { get; set; }
    }
}
