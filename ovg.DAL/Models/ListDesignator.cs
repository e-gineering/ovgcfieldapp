﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListDesignator
    {
        public ListDesignator()
        {
            LeakFinalReview = new HashSet<LeakFinalReview>();
        }

        public string Designator { get; set; }
        public int? Order { get; set; }
        public int DesignatorId { get; set; }

        public ICollection<LeakFinalReview> LeakFinalReview { get; set; }
    }
}
