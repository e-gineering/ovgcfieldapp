﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Miscellaneous
    {
        public int MiscId { get; set; }
        public int WorkUnitId { get; set; }
        public string Description { get; set; }
        public decimal? Value { get; set; }
        public int MiscAttributeId { get; set; }

        public MiscAttribute MiscAttribute { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
