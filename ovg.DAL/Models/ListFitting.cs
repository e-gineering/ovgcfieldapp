﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFitting
    {
        public ListFitting()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string Fitting { get; set; }
        public int? Order { get; set; }
        public int FittingId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
