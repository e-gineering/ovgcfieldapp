﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LineExcavation2PipeConditionExternal
    {
        public int LineExcavationId { get; set; }
        public int WorkUnitId { get; set; }
        public int PipeConditionExternalId { get; set; }

        public ListPipeConditionExternal PipeConditionExternal { get; set; }
        public WorkUnit WorkUnit { get; set; }
    }
}
