﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class WorkOrder
    {
        public WorkOrder()
        {
            WorkOrderSpecificationReport = new HashSet<WorkOrderSpecificationReport>();
        }

        public int WorkOrderId { get; set; }
        public int WorkUnitId { get; set; }
        public string OrderLocation { get; set; }
        public string Purpose { get; set; }
        public string ToServe { get; set; }
        public string CustomerNo { get; set; }
        public string DescriptionNotes { get; set; }
        public string OvgcbookNo { get; set; }
        public string MapSectionNo { get; set; }
        public string ProjectCoordinator { get; set; }
        public string EstimatedCost { get; set; }
        public string Cwomrtnumbers { get; set; }
        public string CworegulatorStationNo { get; set; }
        public string CworegulatorStationLocation { get; set; }
        public bool? CwoisInstalled { get; set; }
        public bool? CwoisUpgraded { get; set; }
        public bool? CwoisRetired { get; set; }
        public bool? CworeportsAttached { get; set; }
        public DateTime? CwodatePlacedInService { get; set; }
        public string CwoworkCompleted { get; set; }
        public string Cwoengineering { get; set; }
        public string CwodistrictManager { get; set; }
        public string Mjocompleted { get; set; }
        public string Mjocomments { get; set; }
        public string MjodistrictManager { get; set; }
        public bool? MjoreportsAttached { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
        public ICollection<WorkOrderSpecificationReport> WorkOrderSpecificationReport { get; set; }
    }
}
