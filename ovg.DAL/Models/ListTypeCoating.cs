﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListTypeCoating
    {
        public ListTypeCoating()
        {
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
        }

        public string TypeCoating { get; set; }
        public int? Order { get; set; }
        public int TypeCoatingId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
    }
}
