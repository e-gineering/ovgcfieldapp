﻿ using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class WorkUnit
    {
        public WorkUnit()
        {
            Attachment = new HashSet<Attachment>();
            InverseParentWorkUnit = new HashSet<WorkUnit>();
            LeakData = new HashSet<LeakData>();
            LeakFinalReview = new HashSet<LeakFinalReview>();
            LeakRepair = new HashSet<LeakRepair>();
            LeakRepair2PipeConditionExternal = new HashSet<LeakRepair2PipeConditionExternal>();
            LeakRepairVerification = new HashSet<LeakRepairVerification>();
            LineExcavation = new HashSet<LineExcavation>();
            LineExcavation2PipeConditionExternal = new HashSet<LineExcavation2PipeConditionExternal>();
            Locate = new HashSet<Locate>();
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
            MeteringPressureFieldTest = new HashSet<MeteringPressureFieldTest>();
            Miscellaneous = new HashSet<Miscellaneous>();
            Mrtdetail = new HashSet<Mrtdetail>();
            Mrtorder = new HashSet<Mrtorder>();
            PipeAndCoatingInspection = new HashSet<PipeAndCoatingInspection>();
            PipelinePressureTest = new HashSet<PipelinePressureTest>();
            ServiceLineJobOrder = new HashSet<ServiceLineJobOrder>();
            WorkOrder = new HashSet<WorkOrder>();
        }

        public int WorkUnitId { get; set; }
        public int? ParentWorkUnitId { get; set; }
        public string WorkUnitAutoId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public int? AssignedToUser { get; set; }
        public DateTime? AssignedToUserOnDate { get; set; }
        public int? CheckedOutBy { get; set; }
        public DateTime? CheckedOutOnDate { get; set; }
        public Guid? CheckedOutId { get; set; }
        public int? ClosedBy { get; set; }
        public DateTime? ClosedByDate { get; set; }
        public int? ApprovedClosedBy { get; set; }
        public DateTime? ApprovedClosedByDate { get; set; }
        public string Notes { get; set; }
        public DateTime? LastUpdatedOnDate { get; set; }
        public string MachineName { get; set; }
        public int WorkUnitTypeId { get; set; }
        public int DistrictId { get; set; }

        public User ApprovedClosedByNavigation { get; set; }
        public User AssignedToUserNavigation { get; set; }
        public User CheckedOutByNavigation { get; set; }
        public User ClosedByNavigation { get; set; }
        public User CreatedByNavigation { get; set; }
        public District District { get; set; }
        public WorkUnit ParentWorkUnit { get; set; }
        public WorkUnitType WorkUnitType { get; set; }
        public ICollection<Attachment> Attachment { get; set; }
        public ICollection<WorkUnit> InverseParentWorkUnit { get; set; }
        public ICollection<LeakData> LeakData { get; set; }
        public ICollection<LeakFinalReview> LeakFinalReview { get; set; }
        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LeakRepair2PipeConditionExternal> LeakRepair2PipeConditionExternal { get; set; }
        public ICollection<LeakRepairVerification> LeakRepairVerification { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
        public ICollection<LineExcavation2PipeConditionExternal> LineExcavation2PipeConditionExternal { get; set; }
        public ICollection<Locate> Locate { get; set; }
        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
        public ICollection<MeteringPressureFieldTest> MeteringPressureFieldTest { get; set; }
        public ICollection<Miscellaneous> Miscellaneous { get; set; }
        public ICollection<Mrtdetail> Mrtdetail { get; set; }
        public ICollection<Mrtorder> Mrtorder { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspection { get; set; }
        public ICollection<PipelinePressureTest> PipelinePressureTest { get; set; }
        public ICollection<ServiceLineJobOrder> ServiceLineJobOrder { get; set; }
        public ICollection<WorkOrder> WorkOrder { get; set; }
    }
}
