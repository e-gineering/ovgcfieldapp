﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class Po
    {
        public Po()
        {
            Podetail = new HashSet<Podetail>();
        }

        public string Ponum { get; set; }
        public string Notes { get; set; }
        public DateTime Podate { get; set; }

        public ICollection<Podetail> Podetail { get; set; }
    }
}
