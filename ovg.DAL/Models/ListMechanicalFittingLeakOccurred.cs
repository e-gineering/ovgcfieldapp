﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMechanicalFittingLeakOccurred
    {
        public ListMechanicalFittingLeakOccurred()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string MechanicalFittingLeakOccurred { get; set; }
        public int? Order { get; set; }
        public int MechanicalFittingLeakOccurredId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
