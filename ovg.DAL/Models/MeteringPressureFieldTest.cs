﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class MeteringPressureFieldTest
    {
        public int MeteringPressureFieldTestId { get; set; }
        public int WorkUnitId { get; set; }
        public bool? IsNewMeteringSet { get; set; }
        public bool? IsCustomerChange { get; set; }
        public bool? IsCustomerDisconnect { get; set; }
        public bool? IsCustomerReconnect { get; set; }
        public bool? IsInstrumentChange { get; set; }
        public bool? IsPressureChange { get; set; }
        public string PressureChangeFrom { get; set; }
        public bool? PressureChangeFromPsig { get; set; }
        public bool? PressureChangeFromInWc { get; set; }
        public string PressureChangeTo { get; set; }
        public bool? PressureChangeToPsig { get; set; }
        public bool? PressureChangeToInWc { get; set; }
        public bool? IsRoutineTest { get; set; }
        public bool? IsQuarterlyEngineeringDept { get; set; }
        public bool? IsDistrictPersonnel { get; set; }
        public bool? IsMeterChange { get; set; }
        public bool? IsIndexChange { get; set; }
        public bool? IsInstallElectricInstrument { get; set; }
        public bool? IsAccountNoChange { get; set; }
        public bool? IsInstrumentCalibration { get; set; }
        public string AsFoundGaugeIb { get; set; }
        public bool? AsFoundGaugeIboz { get; set; }
        public bool? AsFoundGaugeIbwc { get; set; }
        public string AsFoundGaugeIipsig { get; set; }
        public string AsFoundGaugeIiipsig { get; set; }
        public string AsFoundInstrumentIipsig { get; set; }
        public string AsFoundInstrumentIiipsig { get; set; }
        public string AsLeftGaugeIb { get; set; }
        public bool? AsLeftGaugeIboz { get; set; }
        public bool? AsLeftGaugeIbwc { get; set; }
        public string AsLeftGaugeIipsig { get; set; }
        public string AsLeftGaugeIiipsig { get; set; }
        public string AsLeftInstrumentIipsig { get; set; }
        public string AsLeftInstrumentIiipsig { get; set; }
        public string RegularIndexIn { get; set; }
        public string RegularIndexOut { get; set; }
        public string RotaryMeterIndexIn { get; set; }
        public string RotaryMeterIndexOut { get; set; }
        public bool? CompensatingIndex2 { get; set; }
        public bool? CompensatingIndex10 { get; set; }
        public bool? MeterOperatingAfterChecks { get; set; }
        public bool? MeterBypassed { get; set; }
        public string LengthOfBypass { get; set; }
        public bool? LengthOfBypassMins { get; set; }
        public bool? LengthOfBypassHours { get; set; }
        public string MeterFlowBeforeBypass { get; set; }
        public string MeterFlowAfterBypass { get; set; }
        public string GasTempAtMeter { get; set; }
        public string EstimatedConsumption { get; set; }
        public bool? IsMechanicalInstrument { get; set; }
        public bool? IsElectronicInstrument { get; set; }
        public string InCorrectedReading { get; set; }
        public string InUncorrectedReading { get; set; }
        public string OutCorrectedReading { get; set; }
        public string OutUncorrectedReading { get; set; }
        public string InMechanicalReading { get; set; }
        public string OutMechanicalReading { get; set; }
        public string OutInstrumentSerial { get; set; }
        public string InInstrumentSerial { get; set; }
        public string AsFoundInstrumentVoltage { get; set; }
        public string AsLeftInstrumentVoltage { get; set; }
        public bool? InstrumentVoltageChanged { get; set; }
        public string Comments { get; set; }
        public string Signature { get; set; }
        public DateTime? SignatureDate { get; set; }
        public bool IsSpecification { get; set; }
        public string SpecificationNote { get; set; }
        public string SpecificationName { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
    }
}
