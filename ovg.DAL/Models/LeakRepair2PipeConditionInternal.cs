﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakRepair2PipeConditionInternal
    {
        public int LeakRepairId { get; set; }
        public int WorkUnitId { get; set; }
        public int PipeConditionInternalId { get; set; }

        public ListPipeConditionInternal PipeConditionInternal { get; set; }
    }
}
