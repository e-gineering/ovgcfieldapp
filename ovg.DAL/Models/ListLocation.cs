﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListLocation
    {
        public ListLocation()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string Location { get; set; }
        public int? Order { get; set; }
        public int LocationId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
