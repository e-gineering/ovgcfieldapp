﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeCoatingMaterial
    {
        public ListPipeCoatingMaterial()
        {
            PipeAndCoatingInspectionPcoatingMaterialMain = new HashSet<PipeAndCoatingInspection>();
            PipeAndCoatingInspectionPcoatingMaterialService = new HashSet<PipeAndCoatingInspection>();
        }

        public string PipeCoatingMaterial { get; set; }
        public int? Order { get; set; }
        public int PipeCoatingMaterialId { get; set; }

        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPcoatingMaterialMain { get; set; }
        public ICollection<PipeAndCoatingInspection> PipeAndCoatingInspectionPcoatingMaterialService { get; set; }
    }
}
