﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMechanicalFittingLeakCause
    {
        public ListMechanicalFittingLeakCause()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string MechanicalFittingLeakCause { get; set; }
        public int? Order { get; set; }
        public int MechanicalFittingLeakCauseId { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
