﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListPipeToSoilCurrentType
    {
        public ListPipeToSoilCurrentType()
        {
            LeakRepair = new HashSet<LeakRepair>();
            LineExcavation = new HashSet<LineExcavation>();
        }

        public string PipeToSoilCurrentType { get; set; }
        public int? Order { get; set; }
        public int PipeToSoilCurrentTypeId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
        public ICollection<LineExcavation> LineExcavation { get; set; }
    }
}
