﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListFittingFailureCause
    {
        public string FittingFailureCause { get; set; }
        public int? Order { get; set; }
    }
}
