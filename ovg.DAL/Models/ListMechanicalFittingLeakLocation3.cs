﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListMechanicalFittingLeakLocation3
    {
        public ListMechanicalFittingLeakLocation3()
        {
            MechanicalFittingLeak = new HashSet<MechanicalFittingLeak>();
        }

        public string MechanicalFittingLeakLocation3 { get; set; }
        public int? Order { get; set; }
        public int MechanicalFittingLeakLocation3Id { get; set; }

        public ICollection<MechanicalFittingLeak> MechanicalFittingLeak { get; set; }
    }
}
