﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ReliefValveCatalog
    {
        public int ReliefValveId { get; set; }
        public int? RegulatorStationId { get; set; }

        public StationCatalog RegulatorStation { get; set; }
    }
}
