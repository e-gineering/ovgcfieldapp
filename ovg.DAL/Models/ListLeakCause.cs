﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListLeakCause
    {
        public ListLeakCause()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string LeakCause { get; set; }
        public int? Order { get; set; }
        public int LeakCauseId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
