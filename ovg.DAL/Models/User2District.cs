﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class User2District
    {
        public int UserId { get; set; }
        public int DistrictId { get; set; }

        public District District { get; set; }
        public User User { get; set; }
    }
}
