﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class LeakRepairVerification
    {
        public int LeakRepairVerificationId { get; set; }
        public int WorkUnitId { get; set; }
        public string Notes { get; set; }
        public string LeakConfirmation { get; set; }
        public DateTime? RepairVerificationDate { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }

        public WorkUnit WorkUnit { get; set; }
    }
}
