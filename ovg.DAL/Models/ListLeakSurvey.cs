﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListLeakSurvey
    {
        public ListLeakSurvey()
        {
            LeakData = new HashSet<LeakData>();
        }

        public string LeakSurvey { get; set; }
        public int? Order { get; set; }
        public int LeakSurveyId { get; set; }

        public ICollection<LeakData> LeakData { get; set; }
    }
}
