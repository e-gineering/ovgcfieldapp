﻿using System;
using System.Collections.Generic;

namespace ovg.DAL.Models
{
    public partial class ListLeakConfirmation
    {
        public ListLeakConfirmation()
        {
            LeakRepair = new HashSet<LeakRepair>();
        }

        public string LeakConfirmation { get; set; }
        public int? Order { get; set; }
        public int LeakConfirmationId { get; set; }

        public ICollection<LeakRepair> LeakRepair { get; set; }
    }
}
