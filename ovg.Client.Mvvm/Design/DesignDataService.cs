﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using RestSharp;

namespace ovg.Client.Mvvm.Design
{
    public class DesignDataService : IDataService
    {
        public async Task<string> GetListJsonAsync(OVGObject itemtype, string urlpath, string jwt, CancellationToken ct)
        {
            string url = string.Empty;

            switch (itemtype)
            {
                case OVGObject.WorkUnit:
                    url = @"C:\Source\Repos\OVGFieldTool\ovg.Client.Mvvm\Design\WorkUnitsSample.json";
                    break;

                case OVGObject.User:
                    url = @"C:\Source\Repos\OVGFieldTool\ovg.Client.Mvvm\Design\UsersSample.json";
                    break;

                default:
                    break;
            }

            string data = string.Empty;
            using (var reader = File.OpenText("Words.txt"))
            {
                data = await reader.ReadToEndAsync();
            }

            return data;
        }
        public Task<string> Validate(OVGObject itemtype, string itemid, string jwt, CancellationToken ct)
        {

            return Task.FromResult(string.Empty);
        }
        public Task<string> GetItem(OVGObject itemtype, int itemid, string urlpath, string jwt, CancellationToken ct)
        {
            string url = string.Empty;

            switch (itemtype)
            {
                case OVGObject.WorkUnit:
                    url = @"C:\Source\Repos\OVGFieldTool\ovg.Client.Mvvm\Design\workunit-package-79419.json";
                    break;

                case OVGObject.User:
                    url = @"C:\Source\Repos\OVGFieldTool\ovg.Client.Mvvm\Design\UsersSample.json";
                    break;

                default:
                    break;
            }
            string data =  File.ReadAllText(url);

            return Task.FromResult(data);
        }
    }
}