﻿using GalaSoft.MvvmLight;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.Models
{
    public class DataItem : ObservableObject
    {
        public string mystring;

        public string Title
        {
            get;
            private set;
        }

        public DataItem(string title)
        {
            Title = title;
        }
    }

}