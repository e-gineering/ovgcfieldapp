﻿using ovg.DL.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.Models
{
    public enum OVGObject
    {
        WorkUnit,
        District,
        User
    }
    public interface IDataService
    {
        Task<string> GetListJsonAsync(OVGObject itemtype, string urlpath, string jwt, CancellationToken ct);
        Task<string> GetItem(OVGObject itemtype, int itemid, string urlpath, string jwt, CancellationToken ct);
        Task<string> Validate(OVGObject itemtype, string itemid, string jwt, CancellationToken ct);
    }
}
