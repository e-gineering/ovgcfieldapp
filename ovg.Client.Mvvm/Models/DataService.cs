﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ovg.DL.Models;
using RestSharp;

namespace ovg.Client.Mvvm.Models
{
    public class DataService : IDataService
    {
        private string _urlpath;
       public DataService()
        {
            _urlpath = Properties.Settings.Default.ApiUrl;

        }
        public async Task<string> GetListJsonAsync(OVGObject itemtype, string urlpath, string jwt, CancellationToken ct)
        {
            string url = string.Empty;

            switch (itemtype)
            {
                case OVGObject.WorkUnit:
                    url= urlpath + "api/workunit";
                    break;

                case OVGObject.User:
                    url = urlpath + "api/user";
                    break;

                default:
                    break;
            }

            // Declare an HttpClient object.
            HttpClient client = new HttpClient();

            if (!string.IsNullOrEmpty(jwt))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwt);
                client.DefaultRequestHeaders.Add("Accept", "application/json; charset=utf-8");
            }

            // GetAsync returns a Task<HttpResponseMessage>. 
            // Argument ct carries the message if the Cancel button is chosen. 
        //    HttpResponseMessage response = await client.GetAsync(url);
            HttpResponseMessage response = await client.GetAsync(url, ct);

            // Retrieve the website contents from the HttpResponseMessage.
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> Validate(OVGObject itemtype, string emailaddress, string jwt, CancellationToken ct)
        {
            string url = string.Empty;

            switch (itemtype)
            {
                case OVGObject.User:
                    url = _urlpath + "api/user/validate/" + emailaddress;
                    break;

                default:
                    break;
            }

            // Declare an HttpClient object.
            HttpClient client = new HttpClient();

            if (!string.IsNullOrEmpty(jwt))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwt);
                client.DefaultRequestHeaders.Add("Accept", "application/json; charset=utf-8");
            }

            // GetAsync returns a Task<HttpResponseMessage>. 
            // Argument ct carries the message if the Cancel button is chosen. 
            //    HttpResponseMessage response = await client.GetAsync(url);
            try
            {
                HttpResponseMessage response = await client.GetAsync(url, ct);

                // Retrieve the website contents from the HttpResponseMessage.
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public async Task<string> GetItem(OVGObject itemtype, int itemid, string urlpath, string jwt, CancellationToken ct)
        {
            string url = string.Empty;

            switch (itemtype)
            {
                case OVGObject.WorkUnit:
                    url = "api/workunit/" + itemid;
                    break;

                case OVGObject.User:
                    url = "api/user/" + itemid;
                    break;

                default:
                    break;
            }

            // Declare an HttpClient object.
            HttpClient client = new HttpClient();

            if (!string.IsNullOrEmpty(jwt))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwt);
                client.DefaultRequestHeaders.Add("Accept", "application/json; charset=utf-8");
            }

            // GetAsync returns a Task<HttpResponseMessage>. 
            // Argument ct carries the message if the Cancel button is chosen. 
//            HttpResponseMessage response = await client.GetAsync(url);
            HttpResponseMessage response = await client.GetAsync(url, ct);

            // Retrieve the website contents from the HttpResponseMessage.
            return await response.Content.ReadAsStringAsync();
        }
    }
}