﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Auxiliary.Helpers;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.ViewModels
{
    public class ManageUserViewModel : TabViewModel, IAsyncInitialization
    {
        private readonly IDataService _dataService;
        CancellationTokenSource cts;
        public string Jwt { get; set; }

        /// <summary>
        /// Initializes a new instance of the WorkUnitListViewModel class.
        /// </summary>
        public Task Initialization { get; private set; }
        public ManageUserViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Jwt = SimpleIoc.Default.GetInstance<MainViewModel>().Jwt;
            Title = "Manage Users";
            IsVisible = true;
            IsEnabled = false;
            string urlpath = Properties.Settings.Default.ApiUrl;

            // if app is running in design mode, retrieve static data from the "design" data service to populate the list of users
            if (IsInDesignMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                var task = _dataService.GetListJsonAsync(OVGObject.User, urlpath,string.Empty, cts.Token);
                task.Wait();
                List<UserInfo> items = JsonConvert.DeserializeObject<List<UserInfo>>(task.Result);
                UsersCollection = new ObservableCollection<UserInfo>(items);
            }

            Initialization = InitializeAsync();

        }
        private async Task InitializeAsync()
        {
            // Asynchronously initialize this instance.
            string result = await LoadDataAsync();
        }
        public async Task<string> LoadDataAsync()
        {
            //get start time
            long startTime = DateTime.UtcNow.Ticks;
            string result = string.Empty;

            cts = new CancellationTokenSource();
            if (UsersCollection is null)
                UsersCollection = new ObservableCollection<UserInfo>();
            else if (UsersCollection.Count > 0)
                UsersCollection.Clear();

            string urlpath = Properties.Settings.Default.ApiUrl;

            try
            {
                result = await _dataService.GetListJsonAsync(OVGObject.User, urlpath, Jwt, cts.Token);
                List<UserInfo> list = JsonConvert.DeserializeObject<List<UserInfo>>(result);
                foreach (UserInfo w in list)
                    UsersCollection.Add(w);
                long serverLatency = (DateTime.UtcNow.Ticks - startTime) / TimeSpan.TicksPerMillisecond;


                // Send message to main window to update server latency
                ConnectionStatus cs = new ConnectionStatus();
                cs.StatusType = "ServerResponse";
                cs.Status = "success";
                cs.result = serverLatency;
                Messenger.Default.Send<ConnectionStatus>(cs);

                // Blank out Alert Message, since all is well at this point
                cs.StatusType = "AlertMessage";
                cs.Status = string.Empty;
                cs.result = 0;
                Messenger.Default.Send<ConnectionStatus>(cs);

            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        // Since this is an ObservableCollection, use Set (includes PropertyChanged())
        private ObservableCollection<UserInfo> _users;
        public ObservableCollection<UserInfo> UsersCollection
        {
            get { return _users; }
            set { Set("UsersCollection", ref _users, value, true); }
        }

        private UserInfo selecteduser;
        public UserInfo SelectedUser
        {        
            get { return selecteduser; }
            set { Set("SelectedUser", ref selecteduser, value, true); }
        }
    }
}
