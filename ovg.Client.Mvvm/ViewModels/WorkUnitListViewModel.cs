﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Auxiliary.Helpers;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.ViewModels
{
    public class WorkUnitListViewModel : TabViewModel, IAsyncInitialization
    {
        // Declare dataservice, cancellation token source, private variables.
        private readonly IDataService _dataService;
        public string Jwt { get; set; }
        CancellationTokenSource cts;
        private int _unitsdisplayed;
        private WorkUnitInfo selectedworkunit;
        private bool _showactive;
        private bool _showcompleted;
        private bool _showmy;
        private string _searchtext;
        private bool _showleaks;
        private bool _showlocates;
        public int ByteCount { get; private set; }

        // Declare public properties for databinding.
        public int UnitsDisplayed
        {
            get { return _unitsdisplayed; }
            set { Set("UnitsDisplayed", ref _unitsdisplayed, value, true); }
        }
        public Task Initialization { get; private set; }
        // Since this MVVM light, use Set (includes PropertyChanged())
        private ObservableCollection<WorkUnitInfo> _workunits;
        private List<WorkUnitInfo> _allworkunits;
        public ObservableCollection<WorkUnitInfo> WorkUnitsCollection
        {
            get { return _workunits; }
            set { Set("WorkUnitsCollection", ref _workunits, value, true); }
        }
        public WorkUnitInfo SelectedWorkUnit
        {
            get { return selectedworkunit; }
            set { Set("SelectedWorkUnit", ref selectedworkunit, value, true); }
        }
        public bool ShowLeaks
        {
            get { return _showleaks; }
            set { Set("ShowLeaks", ref _showleaks, value, true); }
        }
        public bool ShowLocates
        {
            get { return _showlocates; }
            set { Set("ShowLocates", ref _showlocates, value, true); }
        }

        public bool ShowCompleted
        {
            get { return _showcompleted; }
            set {
                Set("ShowCompleted", ref _showcompleted, value, true);
            }
        }
        public bool ShowActive
        {
            get { return _showactive; }
            set {
                Set("ShowActive", ref _showactive, value, true);
            }
        }
        public bool ShowMyAssigned
        {
            get { return _showmy; }
            set {
                Set("ShowMyAssigned", ref _showmy, value, true);
            }
        }
        public string SearchText
        {
            get { return _searchtext; }
            set
            {
                Set("SearchText", ref _searchtext, value, true);
            }
        }
        public IAsyncCommand RefreshCommand { get; private set; }
        public RelayCommand SearchCommand { get; private set; }
        public RelayCommand ShowLeaksCommand { get; private set; }
        public RelayCommand ShowLocatesCommand { get; private set; }
        public RelayCommand ShowActiveCommand { get; private set; }
        public RelayCommand ShowMyAssignedCommand { get; private set; }
        public RelayCommand ShowCompletedCommand { get; private set; }

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the WorkUnitListViewModel class.
        /// </summary>
        public WorkUnitListViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Jwt = SimpleIoc.Default.GetInstance<MainViewModel>().Jwt;
            ShowLeaks = true;
            ShowActive = true;
            Title = "Work Units";
            IsVisible = true;
            IsEnabled = true;
            string urlpath = Properties.Settings.Default.ApiUrl;

            // if app is running in design mode, retrieve static data from the "design" data service to populate the work unit list
            if (IsInDesignMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                var task = _dataService.GetListJsonAsync(OVGObject.WorkUnit, urlpath, Jwt, cts.Token);
                task.Wait();
                List<WorkUnitInfo> items = JsonConvert.DeserializeObject<List<WorkUnitInfo>>(task.Result);
                _allworkunits = new List<WorkUnitInfo>(items);
                WorkUnitsCollection = new ObservableCollection<WorkUnitInfo>(_allworkunits);
            }

            Initialization = InitializeAsync();

            // Set up relay commands
            SearchCommand = new RelayCommand(FilterList, true);
            ShowLeaksCommand = new RelayCommand(FilterList, true);
            ShowLocatesCommand = new RelayCommand(FilterList, true);
            ShowActiveCommand = new RelayCommand(FilterList, true);
            ShowMyAssignedCommand = new RelayCommand(FilterList, true);
            ShowCompletedCommand = new RelayCommand(FilterList, true);
        //    RefreshCommand = new AsyncCommand<int>(() => Refresh);
            RefreshCommand = new AsyncCommand(async () =>
            {
                ByteCount = await Refresh();
            });
            return;
        }
        #endregion

        private async Task InitializeAsync()
        {
            // Asynchronously initialize this instance.
            int result = await LoadDataAsync();
            FilterList();
        }
        private async Task<int> Refresh()
        {
            // Asynchronously initialize this instance.
            int result = await LoadDataAsync();
            FilterList();
            return result;
        }

        public async Task<int> LoadDataAsync()
        {
            //get start time
            long startTime = DateTime.UtcNow.Ticks;

            cts = new CancellationTokenSource();
            if (WorkUnitsCollection is null)
                WorkUnitsCollection = new ObservableCollection<WorkUnitInfo>();
            else if (WorkUnitsCollection.Count > 0)
                WorkUnitsCollection.Clear();
            if (_allworkunits is null)
                _allworkunits = new List<WorkUnitInfo>();
            else if (_allworkunits.Count > 0)
                _allworkunits.Clear();

            string urlpath = Properties.Settings.Default.ApiUrl;

            // Prepare to send message to main window to update server latency
            ConnectionStatus cs = new ConnectionStatus();

            try
            {
                string result = await _dataService.GetListJsonAsync(OVGObject.WorkUnit, urlpath, Jwt, cts.Token);
                long serverLatency = (DateTime.UtcNow.Ticks - startTime) / TimeSpan.TicksPerMillisecond;

                cs.StatusType = "ServerResponse";
                cs.Status = "success";
                cs.result = serverLatency;
                Messenger.Default.Send<ConnectionStatus>(cs);

                try
                {
                    List<WorkUnitInfo> list = JsonConvert.DeserializeObject<List<WorkUnitInfo>>(result);
                    result = serverLatency.ToString();
                    foreach (WorkUnitInfo w in list)
                        _allworkunits.Add(w);

                    // Blank out Alert Message, since all is well at this point
                    cs.StatusType = "AlertMessage";
                    cs.Status = string.Empty;
                    cs.result = 0;
                    Messenger.Default.Send<ConnectionStatus>(cs);
                }
                catch (Exception ex)
                {
                    // Update Alert Message to notifiy user about error
                    cs.StatusType = "AlertMessage";
                    cs.Status = ex.Message;
                    cs.result = 0;
                    Messenger.Default.Send<ConnectionStatus>(cs);
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception ex)
            {
                // Update Alert Message to notifiy user about error
                cs.StatusType = "AlertMessage";
                cs.Status = ex.Message;
                cs.result = 0;
                Messenger.Default.Send<ConnectionStatus>(cs);
            }
            return 0;
        }
        public void FilterList()
        {
            string srch = string.Empty;
            if (SearchText != null)
                srch = SearchText.ToLower();
            if (WorkUnitsCollection == null || _allworkunits == null)
                return;

            WorkUnitsCollection.Clear();
            List<WorkUnitInfo> filteredlist = new List<WorkUnitInfo>();
            foreach (WorkUnitInfo w in _allworkunits)
            {
                if ((w.Address != null && w.Address.ToLower().Contains(srch)) ||
                    (w.CityStateZip != null && w.CityStateZip.ToLower().Contains(srch)) ||
                    (w.District != null && w.District.ToLower().Contains(srch)) ||
                    (w.CreatedByUser != null && w.CreatedByUser.ToLower().Contains(srch)) ||
                    (w.CheckedOutByUser != null && w.CheckedOutByUser.ToLower().Contains(srch)) ||
                    (w.ApprovedClosedByUser != null && w.ApprovedClosedByUser.ToLower().Contains(srch)))
                {
                    if (w.WorkUnitType == "leak" && ShowLeaks == false)
                        continue;
                    if (w.WorkUnitType == "locate" && ShowLocates == false)
                        continue;
                    if (ShowActive == false && w.ApprovedClosedBy != null && w.ApprovedClosedBy > 0)
                               continue;
                    if (ShowCompleted == false && (w.ApprovedClosedBy == null || w.ApprovedClosedBy == 0))
                               continue;
                    if (ShowMyAssigned == true && (w.AssignedTo == null || w.AssignedTo != 118))
                               continue;
                    filteredlist.Add(w);
                }
            }
            WorkUnitsCollection = new ObservableCollection<WorkUnitInfo>(filteredlist);
            UnitsDisplayed = WorkUnitsCollection.Count;
        }
    }
}