﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Auxiliary.Helpers;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace ovg.Client.Mvvm.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase, IAsyncInitialization
    {
        // User authentication properties
        private WindowsIdentity _winIdentity;
        private WindowsPrincipal _winPrincipal;
        private string _refresh = "1/_MF8TUQbCdgdEO_JbFWNoVTxwn3yey8BJ1Hy5K_Y9Io";
        private OAuth2AccessTokenReponse _tokenresponse;
        private string _currentUserEmail;

        public Task Initialization { get; private set; }
        private CancellationTokenSource _cts;
        private bool _showlogin;
        public Boolean ShowLogin { get { return _showlogin; } set { Set(ref _showlogin, value); } }

        // Declare service proxy
        private readonly IDataService _dataService;
        private ObservableCollection<TabViewModel> _tabitemviewmodels;

        // client configuration - OVGC Field Tool app
        const string _clientID = "868301095766-8mkjnjfj9c98tokt7t4r9tkbbrcp0mmv.apps.googleusercontent.com";
        const string _clientSecret = "SbgOkf5EXZSootR2Ui7ONNh4";
        const string _authorizationEndpoint = "https://accounts.google.com/o/oauth2/auth";
        const string _auth_uri = "https://accounts.google.com/o/oauth2/auth";
        const string _tokenEndpoint = "https://accounts.google.com/o/oauth2/token";
        const string _tokenRequestEndpoint = "https://www.googleapis.com/oauth2/v4/token";
        const string _userInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";
        const string _auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs";
        string[] redirecturis = new[] { "urn:ietf:wg:oauth:2.0:oob", "http://localhost" };
        public RelayCommand AppDataCommand { get; private set; }
        public string AccessToken { get; set; }
        public string Jwt { get; set; }
        public string Title { get; set; }
        public string TextBoxOutput { get; set; }
        public ObservableCollection<TabViewModel> TabItemViewModels
        {
            get { return _tabitemviewmodels ?? (_tabitemviewmodels = new ObservableCollection<TabViewModel>()); }
            set { Set(ref _tabitemviewmodels, value); }
        }

        private ViewModelBase _selectedtab;
        private UserEnvironment _userenv;
        public UserEnvironment UserEnvironmentInfo
        {
            get { return _userenv; }
            set { Set(ref _userenv, value); }
        }
        public ViewModelBase SelectedTab
        {
            get { return _selectedtab; }
            set { Set(ref _selectedtab, value); }
        }
        private RelayCommand _loginCommand;

        public RelayCommand LoginCommand
        {
            get
            {
                return _loginCommand
                  ?? (_loginCommand = new RelayCommand(
                    async () =>
                    {
                        await Login();
                    }));
            }
        }

        public MainViewModel(IDataService dataService)
        {
            if (string.IsNullOrEmpty(Jwt))
                ShowLogin = true;
            else
                ShowLogin = false;

            _dataService = dataService;
            UserEnvironmentInfo = new UserEnvironment();
            _winPrincipal = GetCurrentUser();

            Initialization = InitializeAsync();

            // Set up relay commands
            AppDataCommand = new RelayCommand(AppData, true);

            return;
        }
        private WindowsPrincipal GetCurrentUser()
        {
            _winIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
            string username = _winIdentity.Name;
            UserEnvironmentInfo.MachineName = System.Environment.MachineName;
            UserEnvironmentInfo.UserName = _winIdentity.Name;

            return new System.Security.Principal.WindowsPrincipal(_winIdentity);
        }

        private async Task InitializeAsync()
        {
            // Asynchronously initialize this instance.
            UserInfo result = await ValidateUser(_currentUserEmail);
        }

    public async Task<UserInfo> ValidateUser(string email)
        {
            UserInfo uinfo = new UserInfo();
            if (string.IsNullOrEmpty(email))
                return uinfo;

            ShowStatus("Making API call to retrieve OVGC-specific user information...");
            _cts = new CancellationTokenSource();

            try
            {
                string result = await _dataService.Validate(OVGObject.User, email, Jwt, _cts.Token);
                uinfo = JsonConvert.DeserializeObject<UserInfo>(result);
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception)
            {

            }
            return uinfo;

        }
        private void CreateTabs()
        {
            ShowStatus("Retrieving work unit list . . .");

            // Instantiate the view model collection, then add the two view models (as tabs) that are alway included (Manage Users and Work Units)
            TabItemViewModels = new ObservableCollection<TabViewModel>();
            TabItemViewModels.Add(SimpleIoc.Default.GetInstance<WorkUnitListViewModel>());
            TabItemViewModels.Add(new ManageUserViewModel(ServiceLocator.Current.GetInstance<IDataService>()));

            // Register methods with the Messenger class to receive notifications from other places in the field tool.
            Messenger.Default.Register<NavigationMessage>(this, (action) => ReceiveMessage(action));
            Messenger.Default.Register<ConnectionStatus>(this, (action) => ReceiveConnectionMessage(action));
        }
        private void ReceiveMessage(NavigationMessage action)
        {
            switch (action.TabName)
            {
                case "WorkUnitContainer":
                    if (action.WorkUnitId > 0)
                    {
                        _tabitemviewmodels.Add(new WorkUnitViewModel(_dataService));
                    }
                    break;
                default:
                    break;
            }
        }
        private void ReceiveConnectionMessage(ConnectionStatus action)
        {
            switch (action.StatusType)
            {
                case "ServerResponse":
                    if (action.Status == "success")
                    {
                        UserEnvironmentInfo.ServerConnection =  "Connected";

                        // Update PingTime label
                        if (action.result > 0)
                        {
                            UserEnvironmentInfo.ServerLatency = action.result;
                        }
                    }
                    else
                    {
                        UserEnvironmentInfo.ServerConnection = action.Status;
                    }
                    break;
                case "AlertMessage":
                    UserEnvironmentInfo.AlertMessage = action.Status;
                    break;
                default:
                    UserEnvironmentInfo.ServerConnection = "Disconnected";
                    break;
            }
        }
        #region Properties
        /// <summary>
        /// Used to bind any incoming status messages, to the MainWindow status bar.
        /// </summary>
        public string StatusBarMessage
        {
            get { return _statusBarMessage; }
            set
            {
                if (value == _statusBarMessage) return;
                _statusBarMessage = value;
                RaisePropertyChanged("StatusBarMessage");
            }
        }
        private string _statusBarMessage;
        #endregion

        // ref http://stackoverflow.com/a/3978040
        public static int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }
        public void AppData()
        {
            if (!Directory.Exists(UserEnvironmentInfo.AppDataFolder))
                Directory.CreateDirectory(UserEnvironmentInfo.AppDataFolder);

            Process.Start(new ProcessStartInfo(UserEnvironmentInfo.AppDataFolder));
        }
        private async Task Login()
        {
            // if we have a refresh token, use it to retrieve an access token
            if (!string.IsNullOrEmpty(_refresh))
            {
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    byte[] response = client.UploadValues(_tokenRequestEndpoint, new System.Collections.Specialized.NameValueCollection(){
                {"client_id", _clientID},
                {"client_secret", _clientSecret},
                {"refresh_token", _refresh},
                {"grant_type", "refresh_token"}
            });
                    string sresponse = System.Text.Encoding.Default.GetString(response);
                    // converts to dictionary
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(sresponse);
                    _tokenresponse = (OAuth2AccessTokenReponse)Newtonsoft.Json.JsonConvert.DeserializeObject(sresponse, typeof(OAuth2AccessTokenReponse));
                    if (tokenEndpointDecoded["access_token"] != null && tokenEndpointDecoded["id_token"] != null)
                    {
                        AccessToken = tokenEndpointDecoded["access_token"];
                        Jwt = tokenEndpointDecoded["id_token"];
                    }
                }
            }

            // If we procured a jwt and access token, proceed
            if (!string.IsNullOrEmpty(AccessToken) && !string.IsNullOrEmpty(Jwt))
            {
                userinfoCall(AccessToken, Jwt);
            }
            else
                await Login2();
        }
        private async Task Login2()
        {
           
            // Generates state and PKCE values.
            string state = randomDataBase64url(32);
            string code_verifier = randomDataBase64url(32);
            string code_challenge = base64urlencodeNoPadding(sha256(code_verifier));
            const string code_challenge_method = "S256";

            // Creates a redirect URI using an available port on the loopback address.
            string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback, GetRandomUnusedPort());
            ShowStatus("redirect URI: " + redirectURI);

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectURI);
            ShowStatus("Listening..");
            http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&scope=openid%20email%20profile&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                _authorizationEndpoint,
                System.Uri.EscapeDataString(redirectURI),
                _clientID,
                state,
                code_challenge,
                code_challenge_method);

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Brings this app back to the foreground.
            //         this.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                ShowStatus(String.Format("OAuth authorization error: {0}.", context.Request.QueryString.Get("error")));
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                ShowStatus("Malformed authorization response. " + context.Request.QueryString);
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state)
            {
                ShowStatus(String.Format("Received request with invalid state ({0})", incoming_state));
                return;
            }
            output("Authorization code: " + code);

            // Starts the code exchange at the Token Endpoint.
            performCodeExchange(code, code_verifier, redirectURI);
        }

        async void performCodeExchange(string code, string code_verifier, string redirectURI)
        {
            ShowStatus("Exchanging authorization code for tokens...");

            // builds the  request
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                System.Uri.EscapeDataString(redirectURI),
                _clientID,
                code_verifier,
                _clientSecret
                );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(_tokenRequestEndpoint);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    output(responseText);

                    // converts to dictionary
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string access_token = tokenEndpointDecoded["access_token"];
                    string refresh_token = tokenEndpointDecoded["refresh_token"];
                    string id_token = tokenEndpointDecoded["id_token"];
                    userinfoCall(access_token, id_token);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        output("HTTP: " + response.StatusCode);
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                            output(responseText);
                        }
                    }

                }
            }
        }
        async void userinfoCall(string access_token, string jwt)
        {
            ShowStatus("Making API Call to Userinfo...");

            // builds the  request
            string userinfoRequestURI = "https://www.googleapis.com/oauth2/v3/userinfo";

            // sends the request
            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(userinfoRequestURI);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
            userinfoRequest.ContentType = "application/x-www-form-urlencoded";
            userinfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            // gets the response
            AuthInfo authinfo = new AuthInfo();
            string userinfoResponseText = string.Empty;
            WebResponse userinfoResponse = await userinfoRequest.GetResponseAsync();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream()))
            {
                // reads response body
                userinfoResponseText = await userinfoResponseReader.ReadToEndAsync();
                authinfo = JsonConvert.DeserializeObject<AuthInfo>(userinfoResponseText);
            }
            if (authinfo != null && !string.IsNullOrEmpty(authinfo.sub))
            {
                AccessToken = access_token;
                Jwt = jwt;
                UserEnvironmentInfo.UserName = authinfo.email;
                UserEnvironmentInfo.WelcomeLabel = string.Format("Welcome, {0} {1}!", authinfo.given_name, authinfo.family_name);
                UserEnvironmentInfo.UserEmail = authinfo.email;
                ShowStatus(UserEnvironmentInfo.WelcomeLabel);
                UserEnvironmentInfo.AppDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Properties.Settings.Default.LocalStorage);
                UserEnvironmentInfo.AppDataText = "Items in outbox waiting to send";
                ShowLogin = false;
                int districtcount = 0;
                StringBuilder sb = new StringBuilder();
                UserInfo uinfo = await ValidateUser(authinfo.email);
                if (uinfo != null && uinfo.UserDistricts != null)
                {
                    foreach (DistrictDTO d in uinfo.UserDistricts)
                    {
                        if (districtcount == 0)
                            sb.AppendFormat("My districts: {0} {1}", d.DistrictId, d.DistrictName);
                        else
                            sb.AppendFormat(", {0} {1}", d.DistrictId, d.DistrictName);
                        districtcount++;
                    }
                }
                UserEnvironmentInfo.DistrictList = sb.ToString();
                CreateTabs();
            }
            else
            {
                ShowStatus("Please log in via Google.");
            }
        }

        /// <summary>
        /// Appends the given string to the on-screen log, and the debug console.
        /// </summary>
        /// <param name="output">string to be appended</param>
        public void output(string output)
        {
            TextBoxOutput = TextBoxOutput + output + Environment.NewLine;
            Console.WriteLine(output);
        }

        public void ShowStatus(string output)
        {
            UserEnvironmentInfo.AlertMessage = output;
        }

        /// <summary>
        /// Returns URI-safe data with a given input length.
        /// </summary>
        /// <param name="length">Input length (nb. output will be longer)</param>
        /// <returns></returns>
        public static string randomDataBase64url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return base64urlencodeNoPadding(bytes);
        }

        /// <summary>
        /// Returns the SHA256 hash of the input string.
        /// </summary>
        /// <param name="inputStirng"></param>
        /// <returns></returns>
        public static byte[] sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// Base64url no-padding encodes the given input buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string base64urlencodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }


    }
    public class OAuth2AccessTokenReponse
    {
        public string access_token;
        public int expires_in;
        public string token_type;
    }
 
}