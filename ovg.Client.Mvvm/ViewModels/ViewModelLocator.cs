﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:ovg.Client.Mvvm.ViewModels"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using ovg.Client.Mvvm.Design;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;

namespace ovg.Client.Mvvm.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        private string _jwt;
        public string Jwt
        {
            get { return _jwt; }
            set { _jwt = value; }
        }
        private UserInfo _currentuser;
        public UserInfo CurrentUser
        {
            get { return _currentuser; }
            set { _currentuser = value; }
        }
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<WorkUnitListViewModel>();
            SimpleIoc.Default.Register<ManageUserViewModel>();
        }

        /// <summary>
        /// Gets the Main property.
        /// This is a place to organize your views / view models
        /// Also to manage dependency injection if necessary
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }
        public WorkUnitListViewModel WorkUnitInfo_VM
        {
            get { return ServiceLocator.Current.GetInstance<WorkUnitListViewModel>(); }
        }
        public ManageUserViewModel ManageUser_VM
        {
            get { return ServiceLocator.Current.GetInstance<ManageUserViewModel>(); }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }
    }
}