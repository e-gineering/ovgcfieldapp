﻿using ovg.Client.Mvvm.Models;
using ovg.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.ViewModels
{
    class LeakDetectionViewModel : LeakTabViewModel
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// Initializes a new instance of the WorkUnitListViewModel class.
        /// </summary>
        public LeakDetectionViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Title = "Leak Detection";
            IsVisible = true;
            IsEnabled = false;

            // if app is running in design mode, retrieve static data from the "design" data service to populate the list of users
            if (IsInDesignMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
            }
        }


        private LeakData leakdetectiondata;
        public LeakData LeakDetectionData
        {
            get { return leakdetectiondata; }
            set { Set("LeakDetectionData", ref leakdetectiondata, value, true); }
        }
    }
}
