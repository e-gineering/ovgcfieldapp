﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Auxiliary.Helpers;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.ViewModels
{
    public class WorkUnitViewModel : TabViewModel
    {
        // Declare dataservice and cancellation token source.
        private readonly IDataService _dataService;
        private int WorkUnitId;
        private int WorkUnitIdType;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the WorkUnitViewModel class.
        /// </summary>
        public WorkUnitViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Title = "Work Units";
            IsVisible = true;
            IsEnabled = true;
            string urlpath = Properties.Settings.Default.ApiUrl;

            // if app is running in design mode, retrieve static data from the "design" data service to populate the work unit list
            if (IsInDesignMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                var task = _dataService.GetListJsonAsync(OVGObject.WorkUnit, urlpath, string.Empty, cts.Token);
                task.Wait();
                List<WorkUnitInfo> items = JsonConvert.DeserializeObject<List<WorkUnitInfo>>(task.Result);
            }

            return;
        }
        #endregion

        private WorkUnitInfo selectedworkunit;
        public WorkUnitInfo SelectedWorkUnit
        {
            get { return selectedworkunit;  }
            set {
                Set("SelectedWorkUnit", ref selectedworkunit, value, true);
                WorkUnit_SelectionChanged(selectedworkunit);
            }
            
        }
        public void WorkUnit_SelectionChanged(WorkUnitInfo selectedworkunit)
        {

            WorkUnitInfo sel = selectedworkunit;
        }
    }
}