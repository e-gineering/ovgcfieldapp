﻿
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using ovg.Client.Mvvm.Auxiliary.Helpers;
using ovg.Client.Mvvm.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using static ovg.DL.Models.WorkUnitInfo;

namespace ovg.Client.Mvvm.Views
{
    /// <summary>
    /// Interaction logic for WorkUnitListView.xaml
    /// Note: For any ItemsControl, if the items added to its Items collection (either directly or via ItemsSource) are not instance of that control's item container, 
    /// then each item is wrapped in an instance of the item container. So here, there is a grid in the xaml (not a TabItem), so the user control will be wrapped in 
    /// a TabItem when rendered
    /// </summary>
    public partial class WorkUnitListView : UserControl
    {
        public WorkUnitListView()
        {
            InitializeComponent();
        }

        private void Dtgd_WorkUnits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg != null && dg.SelectedIndex >= 0)
            {
                WorkUnitInfo wk = (WorkUnitInfo)dg.Items[dg.SelectedIndex];
                NavigationMessage nm = new NavigationMessage();
                nm.WorkUnitId = wk.WorkUnitId;
                nm.TabName = "WorkUnitContainer";
                Messenger.Default.Send<NavigationMessage>(nm);
                return;
           }
        }

    }



 }
