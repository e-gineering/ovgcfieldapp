﻿using System;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.Auxiliary.Helpers
{
    // From https://msdn.microsoft.com/en-us/magazine/dn630647.aspx
    // Just using a minimalist version of Steven Cleary's async command, because we don't need the fancier features for now.
    public class AsyncCommand : AsyncCommandBase
    {
        private readonly Func<Task> _command;
        public AsyncCommand(Func<Task> command)
        {
            _command = command;
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
        public override Task ExecuteAsync(object parameter)
        {
            return _command();
        }
    }
}
