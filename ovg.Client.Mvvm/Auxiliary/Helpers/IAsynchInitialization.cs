﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.Auxiliary.Helpers
{
    /// <summary>
    /// Marks a type as requiring asynchronous initialization and provides the result of that initialization.
    /// For use when you need an async method in a constructor. We will still have to return an uninitialized instance, but at least
    /// we'll expose a property (below) to contain the results
    /// From https://blog.stephencleary.com/2013/01/async-oop-2-constructors.html
    /// </summary>
    public interface IAsyncInitialization
    {
        /// <summary>
        /// The result of the asynchronous initialization of this instance.
        /// </summary>
        Task Initialization { get; }
    }
}
