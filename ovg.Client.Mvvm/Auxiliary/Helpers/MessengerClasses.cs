﻿using GalaSoft.MvvmLight.Messaging;
using ovg.Client.Mvvm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ovg.Client.Mvvm.Auxiliary.Helpers
{
    public class NavigationMessage
    {
        public string TabName { get; set; }
        public string Action { get; set; }
        public int WorkUnitId { get; set; }
    }
    public class ConnectionStatus
    {
        public string StatusType { get; set; }
        public string Status { get; set; }
        public long result { get; set; }
    }
}
