﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using static ovg.DL.Models.WorkUnitInfo;

namespace ovg.Client.Mvvm.Auxiliary.Converters
{
    [ValueConversion(typeof(MonitorStatus), typeof(Colors))]
    public class EnumToColorConverter : IValueConverter
    {

        // convert enum to color for UI
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                //sanity check
                if (!(value is MonitorStatus))
                    return Brushes.Transparent;
                MonitorStatus mv = (MonitorStatus)value;

                // Convert
                switch (mv)
                {
                    case MonitorStatus.Red:
                        return Brushes.OrangeRed;
                    case MonitorStatus.Yellow:
                        return Brushes.Yellow;
                    case MonitorStatus.Green:
                        return Brushes.Green;
                    case MonitorStatus.None:
                        return Brushes.Transparent;
                    default:
                        return Brushes.Transparent;
                }
            }
            catch (Exception)
            {
                return Brushes.Transparent;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                Brushes col = (Brushes)value;
                if (col.Equals(Brushes.OrangeRed))
                    return MonitorStatus.Red;
                if (col.Equals(Brushes.Yellow))
                    return MonitorStatus.Yellow;
                if (col.Equals(Brushes.Green))
                    return MonitorStatus.Green;
                else
                    return MonitorStatus.None;
            }
            catch (Exception)
            {
            }
            return MonitorStatus.None;
        }
    }

}
