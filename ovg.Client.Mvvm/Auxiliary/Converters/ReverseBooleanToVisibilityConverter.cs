﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ovg.Client.Mvvm.Auxiliary.Converters
{
    public class ReverseBooleanToVisibilityConverter : IValueConverter
    {
        private readonly BooleanToVisibilityConverter _converter = new BooleanToVisibilityConverter();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                var b = (bool)value;

                if (b)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            catch (Exception)
            {
                return Visibility.Collapsed;
            }
        }
 
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                var result = _converter.ConvertBack(value, targetType, parameter, culture) as bool?;
                return result == true ? false : true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
