﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Models
{
    public class DistrictDTO
    {
        public string DistrictDesc { get; set; }
        public string DistrictIuupsidlist { get; set; }
        public int? Order { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
