﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Models
{
    public class FieldToolInfo
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? LastCheckin { get; set; }
        public string Status { get; set; }
        public int SecurityGroupId { get; set; }
        public string SecurityGroupDesc { get; set; }
    }
}
