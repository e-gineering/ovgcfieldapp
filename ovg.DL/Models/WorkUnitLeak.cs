﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Models
{

    public class Rootobject
    {
        public string type { get; set; }
        public object[] fileAttachmentsToAdd { get; set; }
        public string[] fileAttachmentsCached { get; set; }
        public Workunit workUnit { get; set; }
        public Form[] forms { get; set; }
        public bool deleteThisWorkUnit { get; set; }
        public bool autocheckinThisWorkUnit { get; set; }
    }

    public class Workunit
    {
        public string type { get; set; }
        public int WorkUnitID { get; set; }
        public object ParentWorkUnitID { get; set; }
        public string DistrictID { get; set; }
        public string WorkUnitType { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public int AssignedToUser { get; set; }
        public DateTime AssignedToUserOnDate { get; set; }
        public int CheckedOutBy { get; set; }
        public DateTime CheckedOutOnDate { get; set; }
        public string CheckedOutID { get; set; }
        public object ClosedBy { get; set; }
        public object ClosedByDate { get; set; }
        public object ApprovedClosedBy { get; set; }
        public object ApprovedClosedByDate { get; set; }
        public string Notes { get; set; }
        public DateTime LastUpdatedOnDate { get; set; }
        public int LastUpdatedBy { get; set; }
        public string LastUpdateAction { get; set; }
        public string MachineName { get; set; }
        public object IUPPSTicketNo { get; set; }
        public string CreatedByUserName { get; set; }
        public string AssignedToUserUserName { get; set; }
        public object ClosedByUserName { get; set; }
        public object ApprovedClosedByUserName { get; set; }
        public string CheckedOutByUserName { get; set; }
        public string LeakDataAddress { get; set; }
        public string LeakDataCityStateZipID { get; set; }
        public object LocateJobSiteAddress { get; set; }
        public object LocateJobSiteCity { get; set; }
        public object LeakMonitor { get; set; }
        public string LeakClass { get; set; }
        public object InitialLeakClass { get; set; }
        public object LeakEvaluationDate { get; set; }
        public object LeakRepairDate { get; set; }
        public object LeakCause { get; set; }
        public object LeakRepairVerificationDate { get; set; }
        public object LocateIsEmergencyTicket { get; set; }
        public object locateTicketDate { get; set; }
        public object locateType { get; set; }
        public object formsListDisplay { get; set; }
        public string localPackageFile { get; set; }
        public string address { get; set; }
        public string city { get; set; }
    }

    public class Form
    {
        public string type { get; set; }
        public Leak2detectedby[] leak2DetectedBy { get; set; }
        public Leak2detectedin[] leak2DetectedIn { get; set; }
        public int LeakDataID { get; set; }
        public string EstimatedSource { get; set; }
        public string SurfaceCover { get; set; }
        public string Pressure { get; set; }
        public string PipeType { get; set; }
        public string PipeSize { get; set; }
        public object VentedCGIPercentGas { get; set; }
        public object VentedCGIPercentLEL { get; set; }
        public object VentedCGIPPM { get; set; }
        public string LeakSurvey { get; set; }
        public string Town { get; set; }
        public string Address { get; set; }
        public string MapSectionNo { get; set; }
        public string Remarks { get; set; }
        public string LeakClass { get; set; }
        public string InitialLeakClass { get; set; }
        public object EstNoOfLeaks { get; set; }
        public string MeterSetOVGCNo { get; set; }
        public string MeterSetLocation { get; set; }
        public string State { get; set; }
        public bool PublicReportedViaPhone { get; set; }
        public bool PublicReportedViaCounter { get; set; }
        public bool PublicReportedGasOdorStrong { get; set; }
        public bool PublicReportedGasOdorFaint { get; set; }
        public string PublicReportedRemarks { get; set; }
        public string PublicReportedRecievedFrom { get; set; }
        public string PublicReportedPhone { get; set; }
        public bool PublicReportedLocationInside { get; set; }
        public bool PublicReportedLocationOutside { get; set; }
        public DateTime RecievedOn { get; set; }
        public DateTime ArrivedSceneOn { get; set; }
        public DateTime DispatchedOn { get; set; }
        public DateTime CompletedOn { get; set; }
        public bool FederalProperty { get; set; }
        public bool OdorantSmellTest { get; set; }
        public string OdorantSmellTestResult { get; set; }
        public string ResponsibleParty { get; set; }
        public bool Monitor { get; set; }
        public DateTime LeakEvaluationDate { get; set; }
        public DateTime LeakDetectionDate { get; set; }
        public string CityStateZipID { get; set; }
        public int WorkUnitID { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? LockedOnDate { get; set; }
        public string LockedSig { get; set; }
        public string formAcronym { get; set; }
        public bool delete { get; set; }
        public Leak2pipeconditionexternal[] leak2PipeConditionExternal { get; set; }
        public Leak2pipeconditioninternal[] leak2PipeConditionInternal { get; set; }
        public int LeakRepairID { get; set; }
        public string LeakConfirmation { get; set; }
        public string LeakCause { get; set; }
        public string Component { get; set; }
        public string Environment { get; set; }
        public string PipeData { get; set; }
        public string SoilNearPipe { get; set; }
        public string PressureUnits { get; set; }
        public string CoatingCondition { get; set; }
        public string DetailOfRepair { get; set; }
        public string DetailOfComponentFailure { get; set; }
        public string MRTNum { get; set; }
        public string Location { get; set; }
        public string TypeCoating { get; set; }
        public int Depth { get; set; }
        public int PressureAmount { get; set; }
        public bool CathodicProtection { get; set; }
        public float PipeToSoil { get; set; }
        public string PipeToSoilPolarity { get; set; }
        public string PipeToSoilCurrentType { get; set; }
        public string DecadeInstalled { get; set; }
        public DateTime RepairDate { get; set; }
        public Leakverification2detectedby[] leakVerification2DetectedBy { get; set; }
        public int LeakRepairVerificationID { get; set; }
        public string Notes { get; set; }
        public DateTime RepairVerificationDate { get; set; }
        public int LeakFinalReviewID { get; set; }
        public string Reportable { get; set; }
        public bool DIMPRelated { get; set; }
        public string Designator { get; set; }
        public int MechanicalFittingLeakID { get; set; }
        public string Fitting { get; set; }
        public string FittingType { get; set; }
        public string FittingMaterial { get; set; }
        public string YearInstalled { get; set; }
        public string YearManufactured { get; set; }
        public string Manufacturer { get; set; }
        public string PartModelNumber { get; set; }
        public string LotNumber { get; set; }
        public string OtherAttributes { get; set; }
        public string FirstPipeSize { get; set; }
        public string FirstPipeMaterial { get; set; }
        public string FirstPipeUnit { get; set; }
        public string SecondPipeSize { get; set; }
        public string SecondPipeMaterial { get; set; }
        public string SecondPipeUnit { get; set; }
        public object FittingLeakLocation { get; set; }
        public DateTime DateOfFailure { get; set; }
        public string MechanicalFittingLeakLocation1 { get; set; }
        public string MechanicalFittingLeakLocation2 { get; set; }
        public string MechanicalFittingLeakLocation3 { get; set; }
        public string MechanicalFittingLeakCause { get; set; }
        public bool ThermalExpansionOrContraction { get; set; }
        public bool ExcavationDamageAtTimeOfLeakDiscovery { get; set; }
        public bool ExcavationDamagePreviousOfLeakDiscovery { get; set; }
        public bool LeakDueToMaterialDefect { get; set; }
        public bool LeakDueToDesignDefect { get; set; }
        public string MechanicalFittingLeakOccurred { get; set; }
        public int PipeAndCoatingInspectionID { get; set; }
        public string MapSec { get; set; }
        public string PipeInspectionType { get; set; }
        public bool isAnodeOnExposedPipe { get; set; }
        public string AnodeOnExposedPipeExplain { get; set; }
        public object isService { get; set; }
        public bool isServicePressure10psigOrMore { get; set; }
        public bool isFlowLimitiorInstalled { get; set; }
        public string FlowLimiterExplain { get; set; }
        public string SLTestPressure1 { get; set; }
        public string SLTestDuration1 { get; set; }
        public string SLTotalPipeFtg1 { get; set; }
        public string SLTestPressure2 { get; set; }
        public string SLTestDuration2 { get; set; }
        public string SLTotalPipeFtg2 { get; set; }
        public string PD_WONumMain { get; set; }
        public string PD_WONumService { get; set; }
        public string PD_YearInstalledMain { get; set; }
        public string PD_YearInstalledService { get; set; }
        public string PD_SizeMain { get; set; }
        public string PD_SizeService { get; set; }
        public string PD_DepthMain { get; set; }
        public string PD_DepthService { get; set; }
        public string PD_PressureMain { get; set; }
        public string PDPressureService { get; set; }
        public bool PDisCathoodicProtectedMain { get; set; }
        public bool PDisCathoodicProtectedService { get; set; }
        public string PDCorrosionMeaterReadingMain { get; set; }
        public string PDCorrosionMeaterReadingService { get; set; }
        public string PCoatingMaterialService { get; set; }
        public string PCoatingMaterialMain { get; set; }
        public string PConditionExtMain { get; set; }
        public string PConditionExtService { get; set; }
        public string PConditionIntMain { get; set; }
        public string PConditionIntService { get; set; }
        public string CoatingConditionMain { get; set; }
        public string CoatingConditionService { get; set; }
        public string SoilNearPipeMain { get; set; }
        public string SoilNearPipeService { get; set; }
        public bool isEnteredOnMap { get; set; }
        public string EnteredOnMapSigned { get; set; }
        public DateTime EnteredOnMapSignedDate { get; set; }
        public object Signed { get; set; }
        public object SignedDate { get; set; }
        public string LocationOfExposedPipe { get; set; }
    }

    public class Leak2detectedby
    {
        public string type { get; set; }
        public object LeakDataID { get; set; }
        public string DetectedBy { get; set; }
        public object WorkUnitID { get; set; }
        public int VentedCGITestValue { get; set; }
    }

    public class Leak2detectedin
    {
        public string type { get; set; }
        public object LeakDataID { get; set; }
        public string DetectedIn { get; set; }
        public object WorkUnitID { get; set; }
    }

    public class Leak2pipeconditionexternal
    {
        public string type { get; set; }
        public object LeakRepairID { get; set; }
        public string PipeConditionExternal { get; set; }
        public object WorkUnitID { get; set; }
    }

    public class Leak2pipeconditioninternal
    {
        public string type { get; set; }
        public object LeakRepairID { get; set; }
        public string PipeConditionInternal { get; set; }
        public object WorkUnitID { get; set; }
    }

    public class Leakverification2detectedby
    {
        public string type { get; set; }
        public object LeakRepairVerificationID { get; set; }
        public string DetectedBy { get; set; }
        public object WorkUnitID { get; set; }
        public int VentedCGITestValue { get; set; }
    }


}
