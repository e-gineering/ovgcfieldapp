﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Models
{
    public class WorkUnitInfo
    {
        public enum MonitorStatus
        {
            None,
            Green,
            Yellow,
            Red
        }
        public int WorkUnitId { get; set; }
        public string Address { get; set; }
        public int? ApprovedClosedBy { get; set; }
        public string ApprovedClosedByUser { get; set; }
        public int? AssignedTo { get; set; }
        public string AssignedToUser { get; set; }
        public MonitorStatus IsEmergencyTicket { get; set; }
        public int? CheckedOutBy { get; set; }
        public string CheckedOutByUser { get; set; }
        public string CityStateZip { get; set; }
        public int? CityStateZipId { get; set; }
        public int? ClosedBy { get; set; }
        public string ClosedByUser { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedByUser { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string District { get; set; }
        public int DistrictId { get; set; }
        public int? LeakClassId { get; set; }
        public DateTime? LeakEvaluationDate { get; set; }
        public bool? LeakMonitor { get; set; }
        public DateTime? LeakRepairDate { get; set; }
        public string MachineName { get; set; }
        public MonitorStatus Monitor { get; set; }
        public DateTime? RepairVerificationDate { get; set; }
        public MonitorStatus Verify { get; set; }
        public int WorkUnitTypeId { get; set; }
        public string WorkUnitType { get; set; }
    }
}
