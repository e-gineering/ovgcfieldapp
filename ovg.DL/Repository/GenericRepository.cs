﻿using Microsoft.EntityFrameworkCore;
using ovg.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ovg.DL.Repository
{
    public class GenericRepository<T, TKey> : IGenericRepository<T, TKey> where T : class
    {
        private readonly ovgContext _context;
        protected DbSet<T> dbSet;

        public GenericRepository(ovgContext context)
        {
            _context = context;
            dbSet = OvgContext.Set<T>();
        }
        public ovgContext OvgContext
        {
            get { return _context; }
        }
        public T GetById(TKey id)
        {
            return OvgContext.Set<T>().Find(id);
        }
        public virtual IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public IEnumerable<T> GetList()
        {
            return OvgContext.Set<T>().ToList();
        }
        public IEnumerable<T> Where(Expression<Func<T, bool>> exp)
        {
            return OvgContext.Set<T>().Where(exp);
        }
        public void Insert(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            OvgContext.Set<T>().Add(entity);
            OvgContext.SaveChanges();
        }
        public void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            OvgContext.Set<T>().Attach(entity);
            OvgContext.Set<T>().Remove(entity);
            OvgContext.SaveChanges();
        }
        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            OvgContext.Set<T>().Attach(entity);
            OvgContext.Entry(entity).State = EntityState.Modified;
            OvgContext.SaveChanges();
        }
    }


}
