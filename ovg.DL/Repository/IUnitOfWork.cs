﻿using ovg.DAL.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Repository
{
    public interface IUnitOfWork
    {
        IGenericRepository<SecurityGroup, int> SecurityGroupRepository { get; }
        IGenericRepository<District, int> DistrictRepository { get; }
        IGenericRepository<LeakData, int> LeakDataRepository { get; }
        IGenericRepository<LeakRepair, int> LeakRepairRepository { get; }
        IGenericRepository<LeakRepairVerification, int> LeakRepairVerificationRepository { get; }
        IGenericRepository<Locate, int> LocateRepository { get; }
        IGenericRepository<User, int> UserRepository { get; }
        IGenericRepository<User2District, int> User2DistrictRepository { get; }
        IGenericRepository<WorkUnit, int> WorkUnitRepository { get; }
        IGenericRepository<WorkUnitType, int> WorkUnitTypeRepository { get; }
        IGenericRepository<CityStateZip, int> CityStateZipRepository { get; }
        void Save();
    }
}

