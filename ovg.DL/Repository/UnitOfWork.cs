﻿using ovg.DAL.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        // Database context
        private readonly ovgContext _context;

        // Property for each repository (returns a repository instance that has been instantiated using the same database context as the other repository instances)
        // (Each repository checks whether the repository already exists. If not, it's instantiated passing in the context instance so as a reault, all repositories
        // share the same context instance)
        private IGenericRepository<SecurityGroup, int> _securitygrouprepository { get; set; }
        private IGenericRepository<District, int> _districtrepository { get; set; }
        private IGenericRepository<LeakData, int> _leakdatarepository { get; set; }
        private IGenericRepository<LeakRepair, int> _leakrepairrepository { get; set; }
        private IGenericRepository<LeakRepairVerification, int> _leakrepairverificationrepository { get; set; }
        private IGenericRepository<Locate, int> _locaterepository { get; set; }
        private IGenericRepository<User, int> _userrepository { get; set; }
        private IGenericRepository<User2District, int> _user2districtrepository { get; set; }
        private IGenericRepository<WorkUnit, int> _workunitrepository { get; set; }
        private IGenericRepository<WorkUnitType, int> _workunittyperepository { get; set; }
        private IGenericRepository<CityStateZip, int> _citystateziprepository { get; set; }
        public UnitOfWork(ovgContext context)
        {
            _context = context;
        }

        public IGenericRepository<SecurityGroup, int> SecurityGroupRepository
        {
            get { return _securitygrouprepository = _securitygrouprepository ?? new GenericRepository<SecurityGroup, int>(_context); }
        }
        public IGenericRepository<CityStateZip, int> CityStateZipRepository
        {
            get { return _citystateziprepository = _citystateziprepository ?? new GenericRepository<CityStateZip, int>(_context); }
        }
        public IGenericRepository<District, int> DistrictRepository
        {
            get { return _districtrepository = _districtrepository ?? new GenericRepository<District, int>(_context); }
        }
        public IGenericRepository<LeakData, int> LeakDataRepository
        {
            get { return _leakdatarepository = _leakdatarepository ?? new GenericRepository<LeakData, int>(_context); }
        }
        public IGenericRepository<LeakRepair, int> LeakRepairRepository
        {
            get { return _leakrepairrepository = _leakrepairrepository ?? new GenericRepository<LeakRepair, int>(_context); }
        }
        public IGenericRepository<LeakRepairVerification, int> LeakRepairVerificationRepository
        {
            get { return _leakrepairverificationrepository = _leakrepairverificationrepository ?? new GenericRepository<LeakRepairVerification, int>(_context); }
        }
        public IGenericRepository<Locate, int> LocateRepository
        {
            get { return _locaterepository = _locaterepository ?? new GenericRepository<Locate, int>(_context); }
        }
        public IGenericRepository<User, int> UserRepository
        {
            get  {  return _userrepository = _userrepository ?? new GenericRepository<User, int>(_context); }
        }
        public IGenericRepository<User2District, int> User2DistrictRepository
        {
            get { return _user2districtrepository = _user2districtrepository ?? new GenericRepository<User2District, int>(_context); }
        }
        public IGenericRepository<WorkUnit, int> WorkUnitRepository
        {
            get { return _workunitrepository = _workunitrepository ?? new GenericRepository<WorkUnit, int>(_context); }
        }
        public IGenericRepository<WorkUnitType, int> WorkUnitTypeRepository
        {
            get { return _workunittyperepository = _workunittyperepository ?? new GenericRepository<WorkUnitType, int>(_context); }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
