﻿using Microsoft.EntityFrameworkCore;
using ovg.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ovg.DL.Repository
{
    public class GenericRepos<T> : IGenericRepos<T> where T : class
    {
        private readonly ovgContext _context;
        protected DbSet<T> dbSet;

        public GenericRepos(ovgContext context)
        {
            _context = context;
            dbSet = OvgContext.Set<T>();
        }
        public ovgContext OvgContext
        {
            get { return _context; }
        }
        public T GetById(object id)
        {
            return dbSet.Find(id);
        }
        public virtual IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public IEnumerable<T> Get()
        {
            return dbSet.ToList();
        }
        public IEnumerable<T> Where(Expression<Func<T, bool>> exp)
        {
            return dbSet.Where(exp);
        }
        public void Insert(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            dbSet.Add(entity);
            OvgContext.SaveChanges();  // could be that this should be call on the context after any other changes have been made
        }
        public void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            dbSet.Attach(entity);
            dbSet.Remove(entity);
            OvgContext.SaveChanges();  // could be that this should be call on the context after any other changes have been made
        }
        public void Delete(object id)
        {
            T entityToDelete = dbSet.Find(id);
            if (entityToDelete == null) throw new ArgumentNullException("entity");
            Delete(entityToDelete);
        }
        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            dbSet.Attach(entity);
            OvgContext.Entry(entity).State = EntityState.Modified;
            OvgContext.SaveChanges();  // could be that this should be call on the context after any other changes have been made
        }
    }


}
