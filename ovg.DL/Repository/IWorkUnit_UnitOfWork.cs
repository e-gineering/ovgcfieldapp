﻿using ovg.DAL.Models;
using ovg.DL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ovg.DL.Repository
{
    public interface IWorkUnit_UnitOfWork
    {
        IGenericRepos<SecurityGroup> SecurityGroupRepository { get; }
        IGenericRepos<District> DistrictRepository { get; }
        IGenericRepos<LeakData> LeakDataRepository { get; }
        IGenericRepos<LeakRepair> LeakRepairRepository { get; }
        IGenericRepos<LeakRepairVerification> LeakRepairVerificationRepository { get; }
        IGenericRepos<Locate> LocateRepository { get; }
        IGenericRepos<User> UserRepository { get; }
        IGenericRepos<User2District> User2DistrictRepository { get; }
        IGenericRepos<WorkUnit> WorkUnitRepository { get; }
        IGenericRepos<WorkUnitType> WorkUnitTypeRepository { get; }
        IGenericRepos<CityStateZip> CityStateZipRepository { get; }
        void Save();
    }
}

