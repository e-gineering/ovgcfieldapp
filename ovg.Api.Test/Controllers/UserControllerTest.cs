﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using ovg.Api.Controllers;
using ovg.DAL.Models;
using ovg.DL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using static ovg.Api.Test.dbContextBuilder;
using ovg.Api.Common;

namespace ovg.Api.Test.Controllers
{
    public class UserControllerTest : IDisposable
    {
        private readonly ovgContext _dbContext;
        private readonly UnitOfWork _unitofwork;
        private readonly UserController _usercontroller;

        public UserControllerTest()
        {
            dbContextBuilder dbcb = new dbContextBuilder(OVGObject.All);
            _dbContext = dbcb.GetContextWithData();
            _unitofwork = new UnitOfWork(_dbContext);
            _usercontroller = new UserController(_unitofwork);

            _usercontroller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = BuildUserClaimsPrincipal("sherri.johns@e-gineering.com", "46278") }
            };
        }
        public ClaimsPrincipal BuildUserClaimsPrincipal(string emailaddr, string zip)
        {
            return new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "1"),
                new Claim(ClaimTypes.Email, emailaddr),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.PostalCode, zip)
            }));
        }
        public void Dispose()
        {
            _dbContext.Dispose();
            _unitofwork.Dispose();
            _usercontroller.Dispose();
        }
        [Fact]
        public void UserController_InstantiationTest()
        {
            // assert the controller is not null
            Assert.NotNull(_usercontroller);
        }

        [Xunit.Theory]
        [InlineData(67)]
        [InlineData(116)]
        public void Can_Retrieve_User_By_ID(int id)
        {
            var useritem = _usercontroller.GetItem(id);
            Assert.NotNull(useritem);
        }

        [Xunit.Theory]
        [InlineData("sherri.johns@e-gineering.com")]
        [InlineData("kjones@ovgc.com")]
        public void Can_Validate_OVG_User_By_Email(string emailaddress)
        {
            _usercontroller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = BuildUserClaimsPrincipal(emailaddress, "46278") }
            };

            var useritem = _usercontroller.Validate(emailaddress);
            Assert.NotEmpty(useritem.UserName);
        }

        [Xunit.Fact]
        public void Can_Retrieve_User_List()
        {
            int count = 0;
            var userlist = _usercontroller.GetUserList();
            foreach (UserInfo u in userlist)
                count++;
            Assert.True(count > 0 );
        }
        [Xunit.Fact]
        public void GetUserList_Method_Is_Decorated_With_Authorize_Attribute()
        {
            var type = _usercontroller.GetType();
            var methodInfo = type.GetMethod("GetUserList", new Type[] { });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "No AuthorizeAttribute found on District() method");
        }
    //    public UserInfo GetOvgUser(IUnitOfWork unitofwork, int userid, string emailaddress)
        [Xunit.Theory]
        [InlineData(67)]
        [InlineData(116)]
        public void Can_Retrieve_OVG_User(int id)
        {
            CommonMethods comm = new Common.CommonMethods();
            UserInfo uinfo = comm.GetOvgUser(_unitofwork, id, string.Empty);
            Assert.NotNull(uinfo);
        }
    }
}