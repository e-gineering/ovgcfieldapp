﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ovg.Api.Controllers;
using ovg.DAL.Models;
using ovg.DL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Xunit;
using static ovg.Api.Test.dbContextBuilder;

// Note: Run dotnet test with switch /p:CollectCoverage=true
namespace ovg.Api.Test.Controllers
{
    public class WorkUnitControllerTest : IDisposable
    {
        private readonly ovgContext _dbContext;
        private readonly UnitOfWork _unitofwork;
        private readonly WorkUnitController _workunitcontroller;

        public WorkUnitControllerTest()
        {
            dbContextBuilder dbcb = new dbContextBuilder(OVGObject.WorkUnit);
            _dbContext = dbcb.GetContextWithData();
            _unitofwork = new UnitOfWork(_dbContext);

            // New up an instance of the WorkUnitController
            _workunitcontroller = new WorkUnitController(_unitofwork);

            // Define fake user so we can test authenticated methods
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "1"),
                new Claim(ClaimTypes.Email, "sherri.johns@e-gineering.com"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.PostalCode, "46278")
            }));

            _workunitcontroller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

        }
        public void Dispose()
        {
            _dbContext.Dispose();
            _unitofwork.Dispose();
            _workunitcontroller.Dispose();
        }

        [Fact]
        public void WorkUnitController_InstantiationTest()
        {
            // assert the controller is not null
            Assert.NotNull(_workunitcontroller);
        }

        [Xunit.Theory]
        [InlineData(1301)]
        public void Can_Retrieve_WorkUnit(int id)
        {
            var workitem = _workunitcontroller.Details(id);
            Assert.NotNull(workitem);
        }

        [Xunit.Fact]
        public void User_Can_Retrieve_WorkUnit_List_When_Authenticated()
        {
            int count = 0;
            var workunitlist = _workunitcontroller.GetWorkUnitList();
            foreach (WorkUnitInfo u in workunitlist)
                count++;
            Assert.True(count > 0);
        }
        [Xunit.Theory]
        [InlineData(1301)]
        public void Can_Retrieve_WorkUnit_Simple(int id)
        {
            var workitem = _workunitcontroller.GetItem(id);
            Assert.NotNull(workitem);
        }
        [Xunit.Theory]
        [InlineData(2240)]
        public void Can_Retrieve_WorkUnit_Assigned_User(int id)
        {
            var workunitlist = _workunitcontroller.GetWorkUnitList();
            var assigneduser = string.Empty;
            foreach (WorkUnitInfo u in workunitlist)
            {
                if (u.WorkUnitId == id)
                {
                    assigneduser = u.AssignedToUser;
                    break;
                }
            }

            Assert.True(assigneduser=="jsimpson");
        }

        [Xunit.Theory]
        [InlineData(51890)]
        public void Leak_Address_Is_Truncated_If_Too_Long(int id)
        {
            var workunitlist = _workunitcontroller.GetWorkUnitList();
            var leakdataaddress = string.Empty;
            foreach (WorkUnitInfo u in workunitlist)
            {
                if (u.WorkUnitId == id)
                {
                    leakdataaddress = u.Address;
                    break;
                }
            }

            Assert.True(leakdataaddress.Length <= 25);
        }
        [Xunit.Theory]
        [InlineData(1301)]
        public void Can_Retrieve_City_State_Zip_Text_From_Id(int id)
        {
            var workunitlist = _workunitcontroller.GetWorkUnitList();
            var citystatezip = string.Empty;
            foreach (WorkUnitInfo u in workunitlist)
            {
                if (u.WorkUnitId == id)
                {
                    citystatezip = u.CityStateZip;
                    break;
                }
            }

            Assert.NotEmpty(citystatezip);
        }
        [Xunit.Theory]
        [InlineData(1301)]
        public void Can_Retrieve_Leak_Repair_Verification_Date(int id)
        {
            var workunitlist = _workunitcontroller.GetWorkUnitList();
            var repairverification = DateTime.MinValue;
            foreach (WorkUnitInfo u in workunitlist)
            {
                if (u.WorkUnitId == id)
                {
                    repairverification = (DateTime)u.RepairVerificationDate;
                    break;
                }
            }

            Assert.NotEqual(repairverification, DateTime.MinValue);
        }

        [Xunit.Fact]
        public void GetWorkUnitList_Method_Is_Decorated_With_Authorize_Attribute()
        {
            var type = _workunitcontroller.GetType();
            var methodInfo = type.GetMethod("GetWorkUnitList", new Type[] { });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "No AuthorizeAttribute found on GetWorkUnitList() method");
        }
    }
}