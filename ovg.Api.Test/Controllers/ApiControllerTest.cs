﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ovg.Api.Controllers;
using System;
using System.Linq;
using System.Security.Claims;
using Xunit;

namespace ovg.Api.Test.Controllers
{
    public class ApiControllerTest : IDisposable
    {
        private readonly ApiController _api;

        public ApiControllerTest()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "1"),
                new Claim(ClaimTypes.Email, "info@e-gineering.com"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.PostalCode, "46278")
            }));
            _api = new ApiController();
            _api.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
        }
        public void Dispose()
        {
        }
        [Fact]
        public void ApiController_InstantiationTest()
        {
            // assert the controller is not null
            Assert.NotNull(_api);
        }

        [Fact]
        public void Public_Method_Requires_No_Auth()
        {
            var type = _api.GetType();
            var methodInfo = type.GetMethod("Public", new Type[] { });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.False(attributes.Any(), "No AuthorizeAttribute found on Public() method");

        }
        [Fact]
        public void Get_Public_Returns_Json()
        {
            var result = _api.Public() as JsonResult;
            Assert.NotNull(result);
        }
        [Fact]
        public void Get_Private_Returns_Json()
        {
            var result = _api.Private() as JsonResult;
            Assert.NotNull(result);
        }
        [Fact]
        public void Private_Method_Requires_Auth()
        {
            var type = _api.GetType();
            var methodInfo = type.GetMethod("Private", new Type[] { });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "AuthorizeAttribute found on Private() method");

        }
        [Fact]
        public void Scoped_Method_Requires_Auth()
        {
            var type = _api.GetType();
            var methodInfo = type.GetMethod("Scoped", new Type[] { });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "AuthorizeAttribute found on Scoped() method");

        }
        [Fact]
        public void Scoped_returns_Json()
        {
            var result = _api.Scoped() as JsonResult;
            Assert.NotNull(result);
        }
        [Fact]
        public void Claims_Returns_Json()
        {
            var result = _api.Claims() as JsonResult;
            Assert.NotNull(result);
        }
    }
}