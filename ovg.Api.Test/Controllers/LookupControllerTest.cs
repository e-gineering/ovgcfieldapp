﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ovg.Api.Controllers;
using ovg.DAL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using static ovg.Api.Test.dbContextBuilder;

namespace ovg.Api.Test.Controllers
{
    public class LookupControllerTest : IDisposable
    {
        private readonly ovgContext _dbContext;
        private readonly UnitOfWork _unitofwork;
        private readonly LookupController _lookup;

        public LookupControllerTest() 
        {
            dbContextBuilder dbcb = new dbContextBuilder(OVGObject.All);
            _dbContext = dbcb.GetContextWithData();
            _unitofwork = new UnitOfWork(_dbContext);
            _lookup = new LookupController(_unitofwork);
        }
        public void Dispose()
        {
            _dbContext.Dispose();
            _unitofwork.Dispose();
            _lookup.Dispose();
        }
        [Fact]
        public void LookupController_InstantiationTest()
        {
            // assert the controller is not null
            Assert.NotNull(_lookup);
        }

        [Xunit.Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(88)]
        public void Can_Retrieve_Single_District(int id)
        {

            switch (id)
            {
                case 0:
                    var district0 = _lookup.District(id);
                    Assert.Null(district0);
                    break;
                case 1:
                    var district = _lookup.District(id);
                    Assert.True(district.DistrictId == id);
                    break;
                default:
                    // Expecting id 88 is not there
                    var nodistrict = _lookup.District(id);
                    Assert.Null(nodistrict);
                    break;
            }
        }

        [Xunit.Fact]
        public void Can_Retrieve_District_List()
        {
            int count = 0;
            var districtlist = _lookup.District();
            foreach (District d in districtlist)
                count++;
            Assert.True(count > 0);
        }
        [Xunit.Fact]
        public void District_Method_Is_Decorated_With_Authorize_Attribute()
        {
            var type = _lookup.GetType();
            var methodInfo = type.GetMethod("District", new Type[] {  });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "No AuthorizeAttribute found on District() method");
        }
        [Xunit.Fact]
        public void Get_District_Method_Is_Decorated_With_Authorize_Attribute()
        {
            var type = _lookup.GetType();
            var methodInfo = type.GetMethod("District", new Type[] { typeof(int) });
            var attributes = methodInfo.GetCustomAttributes(typeof(AuthorizeAttribute), true);
            Assert.True(attributes.Any(), "No AuthorizeAttribute found on District(int id) method");
        }
    }
}
