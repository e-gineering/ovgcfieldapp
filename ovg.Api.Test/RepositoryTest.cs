﻿using ovg.Api.Controllers;
using ovg.DAL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using static ovg.Api.Test.dbContextBuilder;

namespace ovg.Api.Test
{
    public class RepositoryTest : IDisposable
    {
        private readonly ovgContext _dbContext;
        private readonly UnitOfWork _unitofwork;
        private readonly LookupController _lookup;

        public RepositoryTest()
        {
            dbContextBuilder dbcb = new dbContextBuilder(OVGObject.None);
            _dbContext = dbcb.GetContextWithData();
            _unitofwork = new UnitOfWork(_dbContext);
            _lookup = new LookupController(_unitofwork);
        }
        public void Dispose()
        {
            _dbContext.Dispose();
            _unitofwork.Dispose();
            _lookup.Dispose();
        }
        [Xunit.Theory]
        [InlineData(10)]
        public void Invalid_District_Repository_Returns_Null(int id)
        {
            var nodistrict = _lookup.District(id);
            Assert.Null(nodistrict);
        }


    }
}
