﻿using Microsoft.AspNetCore.Hosting;

namespace ovg.Api.Test
{
    public class Startup
    {
        public Startup(IHostingEnvironment env) 
        {
        HostingEnvironment = env;
        }

    public IHostingEnvironment HostingEnvironment { get; }
    }
}
