﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ovg.DAL.Models;
using System;

namespace ovg.Api.Test
{
    public class dbContextBuilder
    {
        public enum OVGObject
        {
            None,
            WorkUnit,
            District,
            User,
            All
        }
        private OVGObject _datatype;

    public dbContextBuilder(OVGObject contextDataType)
        {
            _datatype = contextDataType;
        }
        public ovgContext GetContextWithData()
        {
            var options = GetDbOptionsBuilder()
                .Options;


            var context = new ovgContext(options);
            if (_datatype == OVGObject.None)
                return context;

            context.District.Add(new District { DistrictId = 1, DistrictName = "Portland", DistrictIuupsidlist = "asd" });
            context.District.Add(new District { DistrictId = 2, DistrictName = "Winchester", DistrictIuupsidlist = "asd" });
            context.District.Add(new District { DistrictId = 4, DistrictName = "Connersville", DistrictIuupsidlist = "asd" });
            context.District.Add(new District { DistrictId = 5, DistrictName = "Tell City", DistrictIuupsidlist = "ID2396" });
            context.District.Add(new District { DistrictId = 9, DistrictName = "Sullivan", DistrictIuupsidlist = "ID6369" });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "compliance", SecurityGroupId = 1 });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "dispatch_operator", SecurityGroupId = 2 });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "global_manager", SecurityGroupId = 3 });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "manager", SecurityGroupId = 4 });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "read_only_user", SecurityGroupId = 5 });
            context.SecurityGroup.Add(new SecurityGroup { SecurityGroup1 = "service_tech", SecurityGroupId = 6 });
            context.WorkUnitType.Add(new WorkUnitType { WorkUnitType1 = "leak", WorkUnitTypeId = 1 });
            context.WorkUnitType.Add(new WorkUnitType { WorkUnitType1 = "locate", WorkUnitTypeId = 2 });
            context.WorkUnitType.Add(new WorkUnitType { WorkUnitType1 = "mjo", WorkUnitTypeId = 3 });
            context.WorkUnitType.Add(new WorkUnitType { WorkUnitType1 = "sljo", WorkUnitTypeId = 4 });
            context.WorkUnitType.Add(new WorkUnitType { WorkUnitType1 = "wo", WorkUnitTypeId = 5 });
            context.User.Add(new User { UserId = 10, UserName = "IUPPS", FirstName = "IUPPS", LastName = "IUPPS", Email = "NULL", SecurityGroupId = 3 });
            context.User.Add(new User { UserId = 28, UserName = "jgreen", FirstName = "Jeff", LastName = "Green", Email = "jgreen@ovgc.com", SecurityGroupId = 6 });
            context.User.Add(new User { UserId = 35, UserName = "kjones", FirstName = "Kevin", LastName = "Jones", Email = "kjones@ovgc.com", SecurityGroupId = 6 });
            context.User.Add(new User { UserId = 38, UserName = "bfields", Email = "bfields@ovgc.com", SecurityGroupId = 3 });
            context.User.Add(new User { UserId = 60, UserName = "jbuffenbarger", FirstName = "Jeremiah", LastName = "Buffenbarger", Email = "jbuffenbarger@ovgc.com", SecurityGroupId = 3 });
            context.User.Add(new User { UserId = 67, UserName = "theck", FirstName = "Todd", LastName = "Heck", Email = "theck@ovgc.com", SecurityGroupId = 6 });
            context.User.Add(new User { UserId = 70, UserName = "jsimpson", FirstName = "Jeff", LastName = "Simpson", Email = "jsimpson@ovgc.com", SecurityGroupId = 6 });
            context.User.Add(new User { UserId = 116, UserName = "jmonnett", Email = "jmonnett@ovgc.com", SecurityGroupId = 5 });
            context.User.Add(new User { UserId = 117, UserName = "sjohns", Email = "sherri.johns@e-gineering.com", SecurityGroupId = 3 });
            context.User.Add(new User { UserId = 118, UserName = "stech", Email = "sherri.johns@gmail.com", SecurityGroupId = 6 });
            context.User2District.Add(new User2District { UserId = 67, DistrictId = 5 });
            context.User2District.Add(new User2District { UserId = 116, DistrictId = 1 });
            context.User2District.Add(new User2District { UserId = 116, DistrictId = 2 });
            context.User2District.Add(new User2District { UserId = 116, DistrictId = 4 });
            context.User2District.Add(new User2District { UserId = 116, DistrictId = 5 });
            context.User2District.Add(new User2District { UserId = 116, DistrictId = 9 });
            context.User2District.Add(new User2District { UserId = 117, DistrictId = 1 });
            context.User2District.Add(new User2District { UserId = 117, DistrictId = 2 });
            context.User2District.Add(new User2District { UserId = 117, DistrictId = 4 });
            context.User2District.Add(new User2District { UserId = 117, DistrictId = 5 });
            context.User2District.Add(new User2District { UserId = 117, DistrictId = 9 });
            context.User2District.Add(new User2District { UserId = 118, DistrictId = 1 });
            context.User2District.Add(new User2District { UserId = 35, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Batesville, IN 47006", CityStateZipId = 1, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Bicknell, IN 47512", CityStateZipId = 2, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Brookville, IN 47012", CityStateZipId = 3, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Brownsville, IN 47325", CityStateZipId = 4, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Cannelton, IN 47520", CityStateZipId = 5, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Carlisle, IN 47838", CityStateZipId = 6, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Connersville, IN 47331", CityStateZipId = 7, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Dugger, IN 47848", CityStateZipId = 8, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Edwardsport, IN 47528", CityStateZipId = 9, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Evanston, IN 47531", CityStateZipId = 10, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Farmersburg, IN 47850", CityStateZipId = 11, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Ferdinand, IN 47532", CityStateZipId = 12, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Fountain City, IN 47341", CityStateZipId = 13, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Guilford, IN 47022", CityStateZipId = 14, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Hazelton, IN 47640", CityStateZipId = 15, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Huntingburg, IN 47542", CityStateZipId = 16, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Hymera, IN 47855", CityStateZipId = 17, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Jasper, IN 47546", CityStateZipId = 18, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Laurel, IN 47024", CityStateZipId = 19, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Lawrenceburg, IN 47025", CityStateZipId = 20, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Leopold, IN 47551", CityStateZipId = 21, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Lewis, IN 47858", CityStateZipId = 22, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Liberty, IN 47353", CityStateZipId = 23, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Linton, IN 47441", CityStateZipId = 24, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Lynn, IN 47355", CityStateZipId = 25, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Metamora, IN 47030", CityStateZipId = 26, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Milan, IN 47031", CityStateZipId = 27, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Monroe City, IN 47557", CityStateZipId = 28, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Oaktown, IN 47561", CityStateZipId = 29, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Pennville, IN 47369", CityStateZipId = 30, DistrictId = 1 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Petersburg, IN 47567", CityStateZipId = 31, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Pimento, IN 47866", CityStateZipId = 32, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Portland, IN 47371", CityStateZipId = 33, DistrictId = 1 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Richmond, IN 47374", CityStateZipId = 34, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Ridgeville,  IN 47380", CityStateZipId = 35, DistrictId = 1 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Ridgeville, IN 47380", CityStateZipId = 36, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Riley, IN 47871", CityStateZipId = 37, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Saratoga, IN 47382", CityStateZipId = 38, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Shelburn, IN 47879", CityStateZipId = 39, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "St Anthony, IN 47575", CityStateZipId = 40, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "St Meinrad, IN 47577", CityStateZipId = 41, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Sullivan, IN 47882", CityStateZipId = 42, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Sunman, IN 47041", CityStateZipId = 43, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Tell City, IN 47586", CityStateZipId = 44, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Terre Haute, IN 47802", CityStateZipId = 45, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Troy, IN 47588", CityStateZipId = 46, DistrictId = 5 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Union City, IN 47390", CityStateZipId = 47, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Union City, OH 45390", CityStateZipId = 48, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Vincennes, IN 47591", CityStateZipId = 49, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "West Harrison, IN 47060", CityStateZipId = 50, DistrictId = 4 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Wheatland, IN 47597", CityStateZipId = 51, DistrictId = 9 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Williamsburg, IN 47393", CityStateZipId = 52, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Winchester, IN 47394", CityStateZipId = 53, DistrictId = 2 });
            context.CityStateZip.Add(new CityStateZip { CityStateZipName = "Winslow, IN 47598", CityStateZipId = 54, DistrictId = 9 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 79425, CreatedBy = 117, CreatedOnDate = DateTime.Parse("2018-01-02"), MachineName = "DESKTOP - B0AH78I", });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 79423, CreatedBy = 117, CreatedOnDate = DateTime.Parse("2018-01-02"), MachineName = "DESKTOP - B0AH78I", });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 51890, CreatedBy = 24, CreatedOnDate = DateTime.Parse("2017-06-20"), ClosedBy = 102, ClosedByDate = DateTime.Parse("2017-07-03"), ApprovedClosedBy = 60, ApprovedClosedByDate = DateTime.Parse("2017-07-10"), });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 3201, CreatedBy = 10, CreatedOnDate = DateTime.Parse("2016-12-10"), });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2474, CreatedBy = 67, CreatedOnDate = DateTime.Parse("2016-11-14"), ClosedBy = 67, ClosedByDate = DateTime.Parse("2017-03-29"), ApprovedClosedBy = 60, ApprovedClosedByDate = DateTime.Parse("2017-03-30"), WorkUnitTypeId = 1, DistrictId = 5 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2455, CreatedBy = 46, CreatedOnDate = DateTime.Parse("2016-11-11"), WorkUnitTypeId = 1, DistrictId = 4 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2284, CreatedBy = 10, CreatedOnDate = DateTime.Parse("2016-11-07"), WorkUnitTypeId = 2, DistrictId = 5 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2257, CreatedBy = 10, CreatedOnDate = DateTime.Parse("2016-11-05"), WorkUnitTypeId = 2, DistrictId = 5 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2240, CreatedBy = 10, CreatedOnDate = DateTime.Parse("2016-11-04"), AssignedToUser = 70, WorkUnitTypeId = 2, DistrictId = 5 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 2230, CreatedBy = 75, CreatedOnDate = DateTime.Parse("2016-11-04"), ClosedBy = 93, ClosedByDate = DateTime.Parse("2016-11-04"), ApprovedClosedBy = 60, ApprovedClosedByDate = DateTime.Parse("2016-11-17"), WorkUnitTypeId = 1, DistrictId = 9 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 1356, CreatedBy = 45, CreatedOnDate = DateTime.Parse("2016-08-23"), WorkUnitTypeId = 1, DistrictId = 4 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 1335, CreatedBy = 35, CreatedOnDate = DateTime.Parse("2016-08-09"), ClosedBy = 35, ClosedByDate = DateTime.Parse("2016-08-23"), ApprovedClosedBy = 38, ApprovedClosedByDate = DateTime.Parse("2016-09-09"), WorkUnitTypeId = 1, DistrictId = 2 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 1304, CreatedBy = 28, CreatedOnDate = DateTime.Parse("2016-07-11"), ClosedBy = 28, ClosedByDate = DateTime.Parse("2016-09-06"), ApprovedClosedBy = 38, ApprovedClosedByDate = DateTime.Parse("2016-09-09"), WorkUnitTypeId = 1, DistrictId = 2 });
            context.WorkUnit.Add(new WorkUnit { WorkUnitId = 1301, CreatedBy = 35, CreatedOnDate = DateTime.Parse("2016-07-11"), ClosedBy = 35, ClosedByDate = DateTime.Parse("2016-09-09"), ApprovedClosedBy = 60, ApprovedClosedByDate = DateTime.Parse("2016-09-09"), WorkUnitTypeId = 1, DistrictId = 2 });
            context.LeakData.Add(new LeakData { LeakDataId = 82, WorkUnitId = 1356, Town = "Sunman", Address = "Walters Rd ", CompletedOn = DateTime.Parse("2016-08-23"), Monitor = true, LeakEvaluationDate = DateTime.Parse("2017-05-19"), CityStateZipId = 43, LeakClassId = 3 });
            context.LeakData.Add(new LeakData { LeakDataId = 76643, WorkUnitId = 79425, Address = "111 Test Drive", Monitor = false, CityStateZipId = 33, LeakClassId = 3 });
            context.LeakData.Add(new LeakData { LeakDataId = 76641, WorkUnitId = 79423, Address = "111 Penn Street", Monitor = false, CityStateZipId = 30, LeakClassId = 1 });
            context.LeakData.Add(new LeakData { LeakDataId = 61, WorkUnitId = 1335, Town = "union city", Address = "615 n state line", CompletedOn = DateTime.Parse("2016-08-08"), CityStateZipId = 47, LeakClassId = 1 });
            context.LeakData.Add(new LeakData { LeakDataId = 49108, WorkUnitId = 51890, Address = "NE CORNER OF N MAIN ST & E 5TH V201-0165 EMERGENCY VALVE", Monitor = true, LeakEvaluationDate = DateTime.Parse("2017-06-20"), CityStateZipId = 53, LeakClassId = 3 });
            context.LeakData.Add(new LeakData { LeakDataId = 410, WorkUnitId = 2474, Address = "5866 Sugar Maple Rd5866 Sugar Maple Rd.", CompletedOn = DateTime.Parse("2016-11-14"), Monitor = false, CityStateZipId = 5, LeakClassId = 1 });
            context.LeakData.Add(new LeakData { LeakDataId = 407, WorkUnitId = 2455, Town = "Guilford ", Address = "guilford town border station", CompletedOn = DateTime.Parse("2016-08-23"), Monitor = true, LeakEvaluationDate = DateTime.Parse("2017-05-17"), CityStateZipId = 14, LeakClassId = 3 });
            context.LeakData.Add(new LeakData { LeakDataId = 30, WorkUnitId = 1304, Town = "Ridgeville", Address = "623 N. Portland St.", CompletedOn = DateTime.Parse("2016-05-06"), CityStateZipId = 36, LeakClassId = 1 });
            context.LeakData.Add(new LeakData { LeakDataId = 27, WorkUnitId = 1301, Town = "lynn", Address = "2044 e 900 s", CompletedOn = DateTime.Parse("2016-07-06"), CityStateZipId = 25, LeakClassId = 1 });
            context.Locate.Add(new Locate { LocateId = 131, WorkUnitId = 1692, IuppsticketNo = "1610184533", IuppsticketDate = DateTime.Parse("2016-10-18"), JobSiteDate = DateTime.Parse("2016-10-20"), JobSiteCity = "SULLIVAN", IsEmergencyTicket = false, JobSiteAddress = " N CO RT 175 E" });
            context.Locate.Add(new Locate { LocateId = 619, WorkUnitId = 2240, IuppsticketNo = "1611042487", IuppsticketDate = DateTime.Parse("2016-11-04"), JobSiteDate = DateTime.Parse("2016-11-09"),JobSiteCity = "CANNELTON", JobSiteAddress = "508 DODGE ST" });
            context.Locate.Add(new Locate { LocateId = 650, WorkUnitId = 2284, IuppsticketNo = "1611072609", IuppsticketDate = DateTime.Parse("2016-11-07"), JobSiteDate = DateTime.Parse("2016-11-10"), JobSiteCity = "ST MEINRAD", JobSiteAddress = "18185 N SHORT JOHNNYTOWN RD" });
            context.Locate.Add(new Locate { LocateId = 1478, WorkUnitId = 3201, IuppsticketNo = "1612100091", JobSiteDate = DateTime.Parse("2016-12-10"), JobSiteCity = "FERDINAND", IsEmergencyTicket = true, JobSiteAddress = " E 14TH ST" });
            context.LeakRepair.Add(new LeakRepair { LeakRepairId = 25, WorkUnitId = 1301, DetailOfRepair = "replaced insulated union on meter set. to fix the leak.", });
            context.LeakRepair.Add(new LeakRepair { LeakRepairId = 28, WorkUnitId = 1304, DetailOfRepair = "", });
            context.LeakRepair.Add(new LeakRepair { LeakRepairId = 405, WorkUnitId = 2474, });
            context.LeakRepair.Add(new LeakRepair { LeakRepairId = 59, WorkUnitId = 1335, DetailOfRepair = "Crew replaced  transition riser and rebuilt meter set", RepairDate = DateTime.Parse("2016-08-08") });
            context.LeakRepairVerification.Add(new LeakRepairVerification { LeakRepairVerificationId = 27, WorkUnitId = 1301, Notes = "no other problems found at this time. ", LeakConfirmation = "No Leak Found", RepairVerificationDate = DateTime.Parse("2016-07-06"), LockedBy = 35, LockedOnDate = DateTime.Parse("2016-09-09"), LockedSig = "Kevin Jones (kjones)", });
            context.LeakRepairVerification.Add(new LeakRepairVerification { LeakRepairVerificationId = 30, WorkUnitId = 1304, Notes = "Crew to run new service", LeakConfirmation = "No Leak Found", LockedBy = 28, LockedOnDate = DateTime.Parse("2016-07-11"), LockedSig = "Jeff Green (jgreen)", });

            context.SaveChanges();

            return context;
        }

        protected static DbContextOptionsBuilder<ovgContext> GetDbOptionsBuilder()
        {

            // The key to keeping the databases unique and not shared is generating a unique db name for each.
            string dbName = Guid.NewGuid().ToString();

            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<ovgContext>();
            builder.UseInMemoryDatabase(dbName)
                   .UseInternalServiceProvider(serviceProvider)
                   .EnableSensitiveDataLogging(true);

            return builder;
        }

    }
}
