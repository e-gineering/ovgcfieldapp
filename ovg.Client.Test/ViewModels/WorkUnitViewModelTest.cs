﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.Models;
using ovg.Client.Mvvm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ovg.Client.Test.ViewModels
{
    [TestClass]
    class WorkUnitViewModelTest : WorkUnitViewModel
    {
        public WorkUnitViewModelTest(IDataService dataService) : base(dataService) { }

        [TestMethod]
        public void WorkUnitViewModel_InstantiationTest()
        {
            // assert a property of the viewmodel is not null
            Assert.IsNotNull(this.Title);
        }
    }
}
