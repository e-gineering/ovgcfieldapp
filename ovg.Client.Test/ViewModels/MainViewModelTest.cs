﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.Design;
using ovg.Client.Mvvm.ViewModels;

namespace ovg.Client.Test.ViewModels
{
    [TestClass]
    public class MainViewModelTest
    {
        [TestMethod]
        public void MainViewModel_InstantiationTest()
        {
            // Instantiate the viewModel we want to test
            var viewmodel = new MainViewModel(new DesignDataService());

            // assert a property of the viewmodel is not null
            Assert.IsNotNull(viewmodel.UserEnvironmentInfo.MachineName);
        }
    }
}
