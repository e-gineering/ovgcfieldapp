﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.ViewModels;
using GalaSoft.MvvmLight.Ioc;
using CommonServiceLocator;
using ovg.Client.Mvvm.Models;
using ovg.Client.Mvvm.Design;

namespace ovg.Client.Test.ViewModels
{
    [TestClass]
    class ManageUserViewModelTest : WorkUnitListViewModel
    {
        public ManageUserViewModelTest(IDataService dataService) : base(dataService) { }
     
        [TestMethod]
        public void ManageUserViewModel_InstantiationTest()
        {
            // assert a property of the viewmodel is not null
            Assert.IsNotNull(this.Title);
        }
    }
}
