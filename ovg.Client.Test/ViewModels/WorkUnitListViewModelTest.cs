﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.ViewModels;
using GalaSoft.MvvmLight.Ioc;
using CommonServiceLocator;
using ovg.Client.Mvvm.Models;
using ovg.Client.Mvvm.Design;

namespace ovg.Client.Test.ViewModels
{
    [TestClass]
    class WorkUnitListViewModelTest : WorkUnitListViewModel
    {
        public WorkUnitListViewModelTest(IDataService dataService) : base(dataService) { }
     
        [TestMethod]
        public void WorkUnitListViewModel_InstantiationTest()
        {
            // assert a property of the viewmodel is not null
            Assert.IsNotNull(this.Title);
        }
    }
}
