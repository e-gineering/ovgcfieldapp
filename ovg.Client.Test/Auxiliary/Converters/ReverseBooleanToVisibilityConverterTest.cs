﻿using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.Auxiliary.Converters;

namespace ovg.Client.Test.Auxiliary.Converters
{
    [TestClass]
    public class ReverseBooleanToVisibilityConverterTest
    {
        [TestMethod]
        public void Converter_IfTrueNotVisible()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (Visibility)converter.Convert(true, null, null, null);
            Assert.AreEqual(Visibility.Collapsed, result);
        }

        [TestMethod]
        public void Converter_IfFalseThenVisible()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (Visibility)converter.Convert(false, null, null, null);
            Assert.AreEqual(Visibility.Visible, result);
        }
        [TestMethod]
        public void Converter_IfBadInputThenNotVisible()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (Visibility)converter.Convert(new object(), null, null, null);
            Assert.AreEqual(Visibility.Collapsed, result);
        }

        [TestMethod]
        public void ConvertBack_IfVisibleThenFalse()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (bool)converter.ConvertBack(Visibility.Visible, null, null, null);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void ConvertBack_IfCollapsedThenTrue()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (bool)converter.ConvertBack(Visibility.Collapsed, null, null, null);
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void ConvertBack_IfBadInputThenFalse()
        {
            var converter = new ReverseBooleanToVisibilityConverter();
            var result = (bool)converter.ConvertBack(Visibility.Hidden, null, null, null);
            Assert.AreEqual(true, result);
        }

    }
}
