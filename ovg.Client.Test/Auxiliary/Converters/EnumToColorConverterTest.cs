﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ovg.Client.Mvvm.Auxiliary.Converters;
using static ovg.DL.Models.WorkUnitInfo;

namespace ovg.Client.Test.Auxiliary.Converters
{
    [TestClass]
    public class EnumToColorConverterTest
    {
        [TestMethod]
        public void Converter_IfRedThenColorRed()
        {
            var converter = new EnumToColorConverter();
            var result = converter.Convert(MonitorStatus.Red, null, null, null);
            Assert.AreEqual(Brushes.OrangeRed, result);
        }
        [TestMethod]
        public void Converter_IfYellowThenColorYellow()
        {
            var converter = new EnumToColorConverter();
            var result = converter.Convert(MonitorStatus.Yellow, null, null, null);
            Assert.AreEqual(Brushes.Yellow, result);
        }
        [TestMethod]
        public void Converter_IfGreenThenColorGreen()
        {
            var converter = new EnumToColorConverter();
            var result = converter.Convert(MonitorStatus.Green, null, null, null);
            Assert.AreEqual(Brushes.Green, result);
        }
        [TestMethod]
        public void Converter_IfNoEnumThenNoColor()
        {
            var converter = new EnumToColorConverter();
            var result = converter.Convert(MonitorStatus.None, null, null, null);
            Assert.AreEqual(Brushes.Transparent, result);
        }

        [TestMethod]
        public void Converter_IfBadInputThenNoColor()
        {
            var converter = new EnumToColorConverter();
            var result = converter.Convert(new object(), null, null, null);
            Assert.AreEqual(Brushes.Transparent, result);
        }
    }
}
