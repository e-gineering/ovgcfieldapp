﻿using ovg.DAL.Models;
using ovg.DL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ovg.Api.Common
{
    public class CommonMethods
    {
        public UserInfo GetOvgUser(IUnitOfWork unitofwork, int userid, string emailaddress)
        {
            UserInfo rtnObject = new UserInfo();

            try
            {
                var reposSecurity = unitofwork.SecurityGroupRepository.GetList();
                var reposUser2District = unitofwork.User2DistrictRepository.GetList();
                var reposDistrict = unitofwork.DistrictRepository.GetList();

                var us = (from u in unitofwork.UserRepository.GetList()
                          select new
                          {
                              userinfo = u,
                              SecurityGroupDesc = (from g in reposSecurity
                                                   where g.SecurityGroupId == u.SecurityGroupId
                                                   select g.SecurityGroup1).FirstOrDefault(),
                              Dists = (from d in reposUser2District
                                       where d.UserId == u.UserId
                                       select d.District)

                          }).FirstOrDefault(u => (userid > 0 && u.userinfo.UserId == userid) 
                          || !string.IsNullOrEmpty(emailaddress) && u.userinfo.Email == emailaddress);

     rtnObject = new UserInfo()
                {
                    Email = us.userinfo.Email,
                    FirstName = us.userinfo.FirstName,
                    LastCheckin = us.userinfo.LastCheckin,
                    LastName = us.userinfo.LastName,
                    SecurityGroupId = us.userinfo.SecurityGroupId,
                    UserId = us.userinfo.UserId,
                    UserName = us.userinfo.UserName
                };

                if (us.SecurityGroupDesc != null)
                    rtnObject.SecurityGroupDesc = us.SecurityGroupDesc;

                List<DistrictDTO> dlist = new List<DistrictDTO>();
                if (us.Dists != null)
                {
                    foreach (District d in us.Dists)
                        dlist.Add(new DistrictDTO
                        {
                            DistrictId = d.DistrictId,
                            DistrictDesc = d.DistrictName,
                            DistrictName = d.DistrictName,
                            DistrictIuupsidlist = d.DistrictIuupsidlist
                        });
                }
                rtnObject.UserDistricts = dlist.ToList();

                // Status
                if (rtnObject.LastCheckin != null)
                {
                    TimeSpan timespan = DateTime.UtcNow.Subtract(rtnObject.LastCheckin.Value);
                    if (timespan.Minutes < 5)
                        rtnObject.Status = "Online";
                    else
                        rtnObject.Status = "Offline";
                }
                else
                    rtnObject.Status = "Unknown";
            }
            catch (Exception)
            {
                return null;

            }
            return rtnObject;
        }
    }
}
