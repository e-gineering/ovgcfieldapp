﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ovg.Api.Common;
using ovg.DL.Models;
using ovg.DL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ovg.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        readonly IUnitOfWork _unitofwork;
        public UserController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        // Retrieve the entire list, plus related data
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserInfo>), 200)]
        [Authorize]
        public IEnumerable<UserInfo> GetUserList()
        {
            List<UserInfo> ulist = new List<UserInfo>();
            var reposSecurityGroup = _unitofwork.SecurityGroupRepository.GetList();

         //   var users = _unitofwork.UserRepository.Get(includeProperties: "User2District");
            var users = (from u in _unitofwork.UserRepository.GetList()
                             select new
                             {
                                 User = u,
                                 SecurityGroupDesc = (from g in reposSecurityGroup
                                                  where g.SecurityGroupId == u.SecurityGroupId
                                                  select g.SecurityGroup1).FirstOrDefault()

                             }).OrderBy(us=>us.User.LastName).ThenBy(us => us.User.FirstName).ToList();

            foreach (var item in users)
            {
                UserInfo ui = new UserInfo()
                {
                    UserId = item.User.UserId,
                    UserName = item.User.UserName,
                    LastName = item.User.LastName,
                    FirstName = item.User.FirstName,                   
                    Email = item.User.Email,
                    LastCheckin = item.User.LastCheckin,
                    SecurityGroupDesc = item.SecurityGroupDesc
                };
                if (item.User.LastCheckin == null)
                    ui.Status = "Unknown";
                else if (DateTime.UtcNow.Subtract(item.User.LastCheckin.Value).Minutes < 5)
                    ui.Status = "Online";
                else
                    ui.Status = "Offline";
                ulist.Add(ui);
            }

            return ulist;
        }
        [Route("{id:int}")]
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(UserInfo), 200)]
        public UserInfo GetItem(int id)
        {
            // Get list of districts that the user is in
            CommonMethods comm = new Common.CommonMethods();
            return comm.GetOvgUser(_unitofwork, id, string.Empty);
        }

        [Authorize]
        [Route("validate/{email}")]
        [HttpGet]
        [ProducesResponseType(typeof(UserInfo), 200)]
        public UserInfo Validate(string emailaddress)
        {
            // Get list of districts that the user is in
            var claims = HttpContext.User.Claims;
            foreach (Claim c in claims)
            {
                if (c.Type.Contains("emailaddress"))
                    emailaddress = c.Value;
            }
            ovg.Api.Common.CommonMethods comm = new Common.CommonMethods();
            var userinfo = comm.GetOvgUser(_unitofwork, 0, emailaddress);
            if (userinfo != null)
            {
                var curuser = _unitofwork.UserRepository.GetById(userinfo.UserId);
                curuser.LastCheckin = DateTime.Now;
                try
                {
                    _unitofwork.UserRepository.Update(curuser);
                    userinfo.LastCheckin = curuser.LastCheckin;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return userinfo;
        }
    }
}
