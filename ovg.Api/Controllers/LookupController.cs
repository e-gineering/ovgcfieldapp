﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ovg.DAL.Models;
using ovg.DL.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ovg.Api.Controllers
{
    [Route("api/[controller]")]
    public class LookupController : Controller
    {
        readonly IUnitOfWork _unitofwork;
        public LookupController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        // Retrieve the entire list, plus related data
        [HttpGet]
        [Authorize]
        [Route("district")]
        public IEnumerable<District> District()
        {
            try
            {
                return _unitofwork.DistrictRepository.GetList().ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpGet]
        [Authorize]
        [Route("district/{id}")]
        public District District([FromRoute(Name = "id")] int districtId)
        {
            if (districtId > 0)
            {
                try
                {
                    var dist = _unitofwork.DistrictRepository.GetById(districtId);
                    return dist;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }
    }
}
