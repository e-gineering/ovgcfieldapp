﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ovg.DAL.Models;
using ovg.DL.Models;
using ovg.DL.Repository;
using static ovg.DL.Models.WorkUnitInfo;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ovg.Api.Controllers
{
    [Route("api/workunit")]
    public class WorkUnitController : Controller
    {
        readonly IUnitOfWork _unitofwork;

        public WorkUnitController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        // Retrieve the entire list, plus related data
        [Route("")]
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<WorkUnitInfo>), 200)]
        public IEnumerable<WorkUnitInfo> GetWorkUnitList()
        {
            // Get list of districts that the user is in
            var emailaddress = string.Empty;

            var claims = HttpContext.User.Claims;
            foreach (var c in claims)
            {
                if (c.Type.Contains("emailaddress"))
                    emailaddress = c.Value;
            }
            var comm = new Common.CommonMethods();
            var ui = comm.GetOvgUser(_unitofwork, 0, emailaddress);
            var userdistricts = new List<int>();
            foreach (DistrictDTO d in ui.UserDistricts)
                userdistricts.Add(d.DistrictId);

            var wlist = new List<WorkUnitInfo>();
            var loggedin = HttpContext.User.Identity.Name;

            try
            {
                var reposDistrict = _unitofwork.DistrictRepository.GetList();
                var reposLocate = _unitofwork.LocateRepository.GetList();
                var reposLeakData = _unitofwork.LeakDataRepository.GetList();
                var reposLeakRepair = _unitofwork.LeakRepairRepository.GetList();
                var reposUsers = _unitofwork.UserRepository.GetList();
                var reposWorkUnitType = _unitofwork.WorkUnitTypeRepository.GetList();
                var reposLeakRepairVerify = _unitofwork.LeakRepairVerificationRepository.GetList();
                var reposCityStateZip = _unitofwork.CityStateZipRepository.GetList();
                var workunits = (from w in _unitofwork.WorkUnitRepository
                                 where userdistricts.Contains(w.DistrictId)
                                 //  var workunits = (from w in _unitofwork.WorkUnitRepository.GetList()
                                 select new
                                 {
                                     WorkUnit = w,
                                     CreatedByUser = (from u in reposUsers
                                                      where u.UserId == w.CreatedBy
                                                      select u.UserName).FirstOrDefault(),
                                     CheckedOutByUser = (from u in reposUsers
                                                         where u.UserId == w.CheckedOutBy
                                                         select u.UserName).FirstOrDefault(),
                                     WorkUnitType = (from wu in reposWorkUnitType
                                                     where wu.WorkUnitTypeId == w.WorkUnitTypeId
                                                     select wu.WorkUnitType1).FirstOrDefault(),
                                     District = (from d in reposDistrict
                                                 where d.DistrictId == w.DistrictId
                                                 select d.DistrictDesc).FirstOrDefault(),
                                     LeakData = (from ld in reposLeakData
                                                 where ld.WorkUnitId == w.WorkUnitId
                                                 select ld).FirstOrDefault(),
                                     LocateData = (from loc in reposLocate
                                                   where loc.WorkUnitId == w.WorkUnitId
                                                   select loc).FirstOrDefault(),
                                     LeakRepairData = (from lr in reposLeakRepair
                                                       where lr.WorkUnitId == w.WorkUnitId
                                                       select lr).FirstOrDefault(),
                                     LeakRepairVerifyData = (from lrv in reposLeakRepairVerify
                                                             where lrv.WorkUnitId == w.WorkUnitId
                                                             select lrv).FirstOrDefault()
                                 }).OrderByDescending(x => x.WorkUnit.WorkUnitId)
                                    .ToList();

                // Create a searchable list for City/State/Zip
                List<CityStateZip> cszList = new List<CityStateZip>();
                foreach (var item in reposCityStateZip)
                    cszList.Add(item);
                // Create a searchable list for Users
                List<User> userList = new List<User>();
                foreach (var item in reposUsers)
                    userList.Add(item);

                foreach (var item in workunits)
                {
                    WorkUnitInfo w = new WorkUnitInfo()
                    {
                        ApprovedClosedBy = item.WorkUnit.ApprovedClosedBy,
                        AssignedTo = item.WorkUnit.AssignedToUser,
                        CheckedOutBy = item.WorkUnit.CheckedOutBy,
                        ClosedBy = item.WorkUnit.ClosedBy,
                        CreatedBy = item.WorkUnit.CreatedBy,
                        CreatedByUser = item.CreatedByUser,
                        CreatedOnDate = item.WorkUnit.CreatedOnDate,
                        District = item.District,
                        DistrictId = item.WorkUnit.DistrictId,
                        MachineName = item.WorkUnit.MachineName,
                        WorkUnitId = item.WorkUnit.WorkUnitId,
                        WorkUnitType = item.WorkUnitType
                    };

                    // Users
                    if (w.ApprovedClosedBy > 0)
                    {
                        int idx = userList.FindIndex(u => u.UserId == w.ApprovedClosedBy);
                        if (idx >= 0)
                            w.ApprovedClosedByUser = userList[idx].UserName;
                    }
                    if (w.AssignedTo > 0)
                    {
                        int idx = userList.FindIndex(u => u.UserId == w.AssignedTo);
                        if (idx >= 0)
                            w.AssignedToUser = userList[idx].UserName;
                    }
                    if (w.CheckedOutBy > 0)
                    {
                        int idx = userList.FindIndex(u => u.UserId == w.CheckedOutBy);
                        if (idx >= 0)
                            w.CheckedOutByUser = userList[idx].UserName;
                    }
                    if (w.ClosedBy > 0)
                    {
                        int idx = userList.FindIndex(u => u.UserId == w.ClosedBy);
                        if (idx >= 0)
                            w.ClosedByUser = userList[idx].UserName;
                    }
                    // Leak Data 
                    if (item.LeakData != null)
                    {
                        w.WorkUnitType = "leak";
                        if (item.LeakData.Address != null && !string.IsNullOrEmpty(item.LeakData.Address))
                        {
                            w.Address = item.LeakData.Address.Replace(Environment.NewLine, " ");
                            if (w.Address.Length > 25)
                                w.Address = w.Address.Substring(0, 25) + "..";
                        }
                        w.CityStateZipId = item.LeakData.CityStateZipId;
                        w.LeakClassId = item.LeakData.LeakClassId;
                        w.LeakEvaluationDate = item.LeakData.LeakEvaluationDate;
                        w.LeakMonitor = item.LeakData.Monitor;
                        int idx = cszList.FindIndex(c => c.CityStateZipId == item.LeakData.CityStateZipId);
                        if (idx >= 0)
                            w.CityStateZip = cszList[idx].CityStateZipName;
                        w.IsEmergencyTicket = GetMonitorStatus(w, "Emergency", item.LocateData, item.LeakData);
                        w.Monitor = GetMonitorStatus(w, "Monitor", item.LocateData, item.LeakData);
                        w.Verify = GetMonitorStatus(w, "Verify", item.LocateData, item.LeakData);
                    }

                    if (item.LocateData != null)
                    {
                        if (item.LocateData.JobSiteAddress != null && !string.IsNullOrEmpty(item.LocateData.JobSiteAddress))
                        {
                            w.Address = item.LocateData.JobSiteAddress.Replace(Environment.NewLine, " ");
                            if (w.Address.Length > 25)
                                w.Address = w.Address.Substring(0, 25) + "..";
                        }
                        w.CityStateZip = item.LocateData.JobSiteCity;
                        w.WorkUnitType = "locate";
                        w.IsEmergencyTicket = GetMonitorStatus(w, "Emergency", item.LocateData, item.LeakData);
                    }

                    if (item.LeakRepairVerifyData != null)
                        w.RepairVerificationDate = item.LeakRepairVerifyData.RepairVerificationDate;

                    if (item.LeakRepairData != null)
                        w.LeakRepairDate = item.LeakRepairData.RepairDate;

                    wlist.Add(w);
                }
            }
            catch (Exception)
            {
                wlist.Add(new WorkUnitInfo());
            }

            return wlist;
        } 

        public MonitorStatus GetMonitorStatus(WorkUnitInfo w, string monitortype, Locate locatedata, LeakData leakdata)
        {
            // No monitoring of closed / approved items
            if (w.ApprovedClosedBy > 0 || w.ClosedBy > 0)
                return MonitorStatus.None;

            switch (monitortype)
            {
                case "Emergency":
                    if (w.WorkUnitType == "locate" && locatedata.IsEmergencyTicket == true)
                        return MonitorStatus.Red;
                    if (w.WorkUnitType == "leak" && w.LeakClassId == 1)
                        return MonitorStatus.Red;
                    break;

                case "Monitor":
                    if (w.WorkUnitType == "leak" && w.LeakClassId != null && w.LeakMonitor != null)
                    {
                        if (w.LeakEvaluationDate == null)
                            return MonitorStatus.Red;
                        else
                        {
                            DateTime evalDate = (DateTime)w.LeakEvaluationDate;
                            switch (w.LeakClassId)
                            {
                                case 3:
                                    if (DateTime.Now > evalDate.AddDays(360))
                                        return MonitorStatus.Red;
                                    else if (DateTime.Now > evalDate.AddDays(330))
                                        return MonitorStatus.Yellow;
                                    else
                                        return MonitorStatus.Green;

                                case 2:
                                    if (DateTime.Now > evalDate.AddDays(175))
                                        return MonitorStatus.Red;
                                    else if (DateTime.Now > evalDate.AddDays(145))
                                        return MonitorStatus.Yellow;
                                    else
                                        return MonitorStatus.Green;

                                case 1:
                                    if (DateTime.Now > evalDate.AddDays(25))
                                        return MonitorStatus.Red;
                                    else if (DateTime.Now > evalDate.AddDays(5))
                                        return MonitorStatus.Yellow;
                                    else
                                        return MonitorStatus.Green;

                                default:
                                    break;
                            }

                        }
                    }
                    else
                        return MonitorStatus.None;
                    break;

                case "Verify":
                    if (w.WorkUnitType == "leak")
                    {
                        if (w.LeakRepairDate == null || w.RepairVerificationDate == null)
                            return MonitorStatus.None;
                        else
                        {

                            if (DateTime.Now <= w.LeakRepairDate.Value.AddDays(25))
                                return MonitorStatus.Yellow;
                            else
                                return MonitorStatus.Red;
                        }
                    }
                    else
                        return MonitorStatus.None;
            }

            return MonitorStatus.None;
        }

        [Route("{id:int}")]
        [HttpGet]
        [ProducesResponseType(typeof(WorkUnit), 200)]
        public WorkUnit GetItem(int id)
        {
            var workunit = _unitofwork.WorkUnitRepository.GetById(id);
            return workunit;
        }
        [Route("{id:int}/Details")]
        [HttpGet]
        [ProducesResponseType(typeof(WorkUnit), 200)]
        public WorkUnit Details(int id)
        {
            var reposDistrict = _unitofwork.DistrictRepository.GetList();
            var reposLocate = _unitofwork.LocateRepository.GetList();
            var reposLeakData = _unitofwork.LeakDataRepository.GetList();
            var reposLeakRepair = _unitofwork.LeakRepairRepository.GetList();
            var reposUsers = _unitofwork.UserRepository.GetList();
            var reposWorkUnitType = _unitofwork.WorkUnitTypeRepository.GetList();
            var workunit = (from w in _unitofwork.WorkUnitRepository.GetList()
                             select new
                             {
                                 WorkUnit = w,
                                 CreatedByUser = (from u in reposUsers
                                                  where u.UserId == w.CreatedBy
                                                  select u.UserName).FirstOrDefault(),
                                 CheckedOutByUser = (from u in reposUsers
                                                     where u.UserId == w.CheckedOutBy
                                                     select u.UserName).FirstOrDefault(),
                                 IsEmergencyTicket = (from lo in reposLocate
                                                      where lo.WorkUnitId == w.WorkUnitId
                                                      select lo.IsEmergencyTicket).FirstOrDefault(),
                                 LeakClassId = (from l in reposLeakData
                                                where l.WorkUnitId == w.WorkUnitId
                                                select l.LeakClassId).FirstOrDefault(),
                                 Address = (from l in reposLeakData
                                            where l.WorkUnitId == w.WorkUnitId
                                            select l.Address).FirstOrDefault(),
                                 WorkUnitType = (from wu in reposWorkUnitType
                                                 where wu.WorkUnitTypeId == w.WorkUnitTypeId
                                                 select wu.WorkUnitType1).FirstOrDefault(),
                                 District = (from d in reposDistrict
                                             where d.DistrictId == w.DistrictId
                                             select d.DistrictDesc).FirstOrDefault()
                             })
                             .FirstOrDefault(w => w.WorkUnit.WorkUnitId == id);
            if (workunit != null)
            {
                WorkUnit wku = new WorkUnit
                {
                    WorkUnitId = workunit.WorkUnit.WorkUnitId
                };
                return wku;
            }
            else
            {
                return new WorkUnit();
            }
        }
    }
}
